# README #


# Plugin Information #
Contributors: KYB Productions, LLC

Donate link: http://kybproductions.net/donate

Tags: calendar, events

Requires at least: 3.0.1

Tested up to: 3.4

Stable tag: 4.3

License: GPLv2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html


# Description #
KYB Productions offers this simple event calendar manager for your WordPress site. Very simple integration and each calendar can be custom themed to your liking as well as many other features. This Event Calendar plugin displays a full calendar on the page with clickable highlighted events, the current month’s event listings and full view of the event information.  It also supports a mini calender to add as a side bar widget.

![Calendar Sample 1](http://www.kybproductions.net/wp-content/uploads/2013/12/cal_promo1.jpg)      
![Calendar Sample 2](http://www.kybproductions.net/wp-content/uploads/2013/12/cal_promo2.jpg)


# Installation #

1. Upload `kybcalendar` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Create a new form with form settings and form fields
3. Place `<?php do_action('[kybcalendar cal_id=1]'); ?>` in your templates or '[kybcalendar cal_id=1]' on your page or posts

# Changelog #

= 1.0 =

= 2.0 =

# Upgrade Notice #

= 1.0 =

= 0.5 =

# Features #

1.  Monthly view of events in full calendar format
2.  Possible to add multiple events on the same day
3.  Events can be set up to repeat on a weekly, monthly or yearly basis
4.  Can search events by keywords or by specific date
5.  Can set up different calendar themes with specified colors, fonts, borders, backgrounds and more…
6.  Ability to preview theme changes
7.  Ability to copy and paste shortcode on page or post to view specified calendar theme
8.  List of current month’s events can also be displayed
9.  Ability to share event information on social media sites
10. Ability to add event to outlook and google calendars