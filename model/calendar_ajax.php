<?php
$basedir = $_SERVER['DOCUMENT_ROOT'];
require_once("$basedir/wp-config.php");
$plugin_dir = WP_PLUGIN_DIR . "/kybcalendar";
require_once("$plugin_dir/model/shared.php"); // shared classes
require_once("$plugin_dir/model/smarty.php"); // smarty engine
require_once("$plugin_dir/controller/controller.php"); // controller classes
require_once("$plugin_dir/model/model.php"); // shared classes
define('KYBCAL_TABLE', $wpdb->prefix);
$cls = new calendarclass();
$process = $cls->checkRequest('process');

ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('log_errors',TRUE);
ini_set("error_log", WP_PLUGIN_DIR . "/kyb-calendar/error_log.log");
ini_set('display_errors',FALSE);

switch ($process) {
	case "viewcal":
		$prm = $cls->checkRequest('prm'); 
		$calID = $cls->checkRequest('calID');
		if(isset($prm) && $prm > 0){
			$m= $prm;
			$r=(int)date('m',mktime(0,0,0,$m,1,$y));
		} else {
			$m= date("m");
			$r=date("m");
		}
		$cls->checkRequest('y') != "" ? $y = $cls->checkRequest('y') : $y= date("Y");     // Finds today's or selected year
		$no_of_days = date('t',mktime(0,0,0,$m,1,$y)); // This is to calculate number of days in a month
		$mn=date('M',mktime(0,0,0,$m,1,$y)); // Month is calculated to display at the top of the calendar
		$yn=date('Y',mktime(0,0,0,$m,1,$y)); // Year is calculated to display at the top of the calendar
		$j= date('w',mktime(0,0,0,$m,1,$y)); // This will calculate the week day of the first day of the month
		$calDays = $cls->BeginTable('100%', "0", "0", "0","400");
		$calDays .= $cls->calDays($no_of_days, $r, $yn, $j, $calID);
		$calDays .= $cls->EndTable();

		echo $calDays;
	break;
	case "viewevents":		
		$eventList = "";
		$lookup = $cls->checkRequest('dt');
		$calID = $cls->checkRequest('calID');
		$calEvents = $cls->viewEvents ('', $lookup, $calID );
		if (is_array($calEvents)) {
			$html = $calEvents['HTML'];
			$template =  WP_PLUGIN_DIR . "/kybcalendar/view/html/$html";
			$eventList = $cls->showPage ($template, $calEvents, true);
		}		
		echo $eventList;
	break;
	case "changecal":
		$calID = $cls->checkRequest('calID');
		$calendarcontent = $cls->viewCalendar($calID);
		$calTemplate = KYBCAL_ABSPATH . "/view/html/" . $calendarcontent['HTML'];
		$calendarview = $cls->showPage ($calTemplate, $calendarcontent, true);
		echo $calendarview;
	break;
	case "sidecal":
		$content = $cls->viewSidebarCal();
		echo $content;
	break;
	case "printschedule":
		if (checkPermission()) {
			require(WP_PLUGIN_DIR . '/kybshares/fpdf16/fpdf.php');
			require(KYBCAL_ABSPATH . '/model/print_pdf.php');
			$pdf=new PDF();
			require_once(WP_PLUGIN_DIR . "/kybstore/lib/shared.php"); // store shared classes
			require_once(WP_PLUGIN_DIR . "/kybstore/lib/smarty.php"); // store smarty classes
			require_once(WP_PLUGIN_DIR . "/kybstore/controller/controller.php"); // controller classes
			require_once(WP_PLUGIN_DIR . "/kybstore/model/store_cart.php"); // cart classes
			$storecls = new cartprocesses;
			$schedule = "";
			$calID = 2;
			$startDate = date("Y-m-d", strtotime($cls->checkRequest('startdate')));
			$endDate = date("Y-m-d", strtotime($cls->checkRequest('enddate')));
			$startDate != $endDate ? $runDates = $cls->ReformatDatedb($startDate) . " - " . $cls->ReformatDatedb($endDate) : $runDates = $cls->ReformatDatedb($startDate);
			$filename = "River_Trip_Schedule_" . $runDates;
			$filename - str_replace("/", "-", $runDates);
			$filename - str_replace(" ", "_", $runDates);

			//======== Set Up PDF ========//
			$pdf->AliasNbPages();
			$pdf->AddPage('L');
			$pdf->Image("$basedir/wp-content/themes/trinityriver/images/logo.jpg",123,8,50);
			$pdf->SetFont('Arial','IB',12);
			$pdf->Cell(0,20,'',0,1,'C');
			$pdf->Cell(0,5,'River Trip Schedule',0,1,'C');
			$pdf->SetFont('Arial','B',12);
			$pdf->Cell(0,5,$runDates,0,1,'C');
			$pdf->Ln(10);

			$time_array = array();
			$sql = "SELECT *, MAKETIME(start_hour  + IF(start_hour<12, 12, 0), start_minute, 0) AS start_order from " . KYBCAL_TABLE . "kyb_calendar where dt between %s and %s and calendar_id = %d order by dt, start_day, start_order";
			$result = $wpdb->get_results($wpdb->prepare($sql, $startDate, $endDate, $calID));
			if (count($result) != 0) {
				foreach($result as $row) {
					strlen($row->start_minute) < 2 ? $startMinute = "0" . $row->start_minute : $startMinute = $row->start_minute;
					strlen($row->start_hour) < 2 ? $startHour = "0" . $row->start_hour : $startHour = $row->start_hour;
					$booking_time = $startHour . ":" . $startMinute . " " . $row->start_day;					
					$startCheck = date("gA", strtotime($booking_time));
					$time_array[$booking_time] = $startCheck;
					$time_array[$booking_time] = $row->start_time;
				}
			}
			
			$time_array = array_unique($time_array);			

			if (count($time_array) != 0) {
			//	foreach ($result as $row) {
				foreach ($time_array as $key => $value) {
					//$reservation_time = $row->reservation_time;
					//$split_time = explode("-",$reservation_time);
					$start_time = date("g:i A", strtotime($key));
					//$end_time = date("h:i:s A", strtotime($key));
					//$startCheck = date("gA", strtotime($split_time[0]));

					//Check to make sure there are paid listings to print
					$sqlCheck = "Select orders.order_id, " . KYBCAL_TABLE . "kyb_calendar.start_time, " . KYBCAL_TABLE . "kyb_calendar.start_hour, " . KYBCAL_TABLE . "kyb_calendar.start_minute, " . KYBCAL_TABLE . "kyb_calendar.start_day FROM orders INNER JOIN " . KYBCAL_TABLE . "kyb_calendar ON orders.order_id = " . KYBCAL_TABLE . "kyb_calendar.order_num where " . KYBCAL_TABLE . "kyb_calendar.dt between '$startDate' and '$endDate' and " . KYBCAL_TABLE . "kyb_calendar.start_time = '$value' and orders.order_paid = 1;";
					$rsCheck = $wpdb->get_results($sqlCheck);
					
					if (count($rsCheck) != 0) {
						$pdf->SetFont('Arial','B',14);
						$pdf->Ln(5);
						$pdf->Cell(40,5,$start_time,0,0,'L');
						$pdf->Ln(10);
						$pdf->SetFont('Arial','B',11);
						$pdf->Cell(50,5,'River Runs',1,0,'C');
						$pdf->Cell(50,5,'Customer Name',1,0,'C');
						$pdf->Cell(40,5,'Boat Types(s)',1,0,'C');
						$pdf->Cell(30,5,'# Ordered',1,0,'C');
						$pdf->Cell(40,5,'Sale Total',1,0,'C');
						$pdf->Cell(65,5,'Comments',1,0,'C');
						$pdf->Ln(5);
					}
					$sqlItems = "Select product_orders.*, " . KYBCAL_TABLE . "kyb_calendar.start_time, " . KYBCAL_TABLE . "kyb_calendar.start_hour, " . KYBCAL_TABLE . "kyb_calendar.start_minute, " . KYBCAL_TABLE . "kyb_calendar.start_day FROM product_orders INNER JOIN " . KYBCAL_TABLE . "kyb_calendar ON product_orders.order_num = " . KYBCAL_TABLE . "kyb_calendar.order_num where " . KYBCAL_TABLE . "kyb_calendar.dt between '$startDate' and '$endDate' and " . KYBCAL_TABLE . "kyb_calendar.start_time = '$value';";
					$rsItems = $wpdb->get_results($sqlItems);
					$pdf->SetFont('Arial','',11);
					$pdf->SetWidths(array(50, 50, 40, 30, 40, 65));
					srand(microtime()*1000000);
					if (count($rsItems) != 0) {
						foreach ($rsItems as $ord) {
							$product_id = $ord->product_id;
							$order_number = $ord->order_num;
							$orderPaid = $wpdb->get_var($wpdb->prepare("Select order_paid from orders where order_id = %d", $order_number));
							$customOrder = $wpdb->get_var($wpdb->prepare("Select custom_order from orders where order_id = %d", $order_number));
							$totalsale = $wpdb->get_var($wpdb->prepare("Select totalsale from orders where order_id = %d", $order_number));
							$totalsale = "$" . $totalsale;
							$auth_num = $wpdb->get_var($wpdb->prepare("Select authorization_code from orders where order_id = %d", $order_number));
							$auth_num = str_replace("- Custom Order", "", $auth_num);
							$auth_num = str_replace("Custom Order", "", $auth_num);
							$comments = str_replace("\'", "'", $ord->order_comments);
							$user_id = $ord->user_id;
							$boat_type = "";
							$boats_ordered = "";
							$customerInfo = $storecls->getUserInfo($user_id);
							$customerName = $customerInfo->fullname;
							$customerEmail = $customerInfo->email;
							$customerPhone = "\nPH: " . $customerInfo->fullphone;
							$productName = $wpdb->get_var($wpdb->prepare("Select product_name from products where product_id = %d", $product_id));
							$attributeInfo = $storecls->get_product_attributes($order_number, $product_id);		
							$payType = "";
							if (is_array($attributeInfo->boatName)) {
								foreach ($attributeInfo->boatName as $k => $v) {
									$boat_type .= $v . "\n";
									if ($customOrder) {
										if ($auth_num == "") {
											$boats_ordered .= $attributeInfo->numOrdered[$k] . "\n";
											$payType = "\nReserved NP - Custom\n";
										} else {
											$boats_ordered .= $attributeInfo->numOrdered[$k] . "\n";
											$payType = "\nPaid - Custom\n";
										}
									} else {
										$boats_ordered .= $attributeInfo->numOrdered[$k] . "\n";
										$payType = "\nPaid \n";
										
									}
								}
							}

							$totalsale .= $payType;

							if (is_array($attributeInfo->boatSizeInfo)) {
								foreach ($attributeInfo->boatSizeInfo as $k => $v) {
									//echo $v[0] . "-" . $v[1] . "-" . $v[2] . "-" . $v[3] . "<br>";
									
								}
							}
							//Check form submissions for bookings
							$formID = $wpdb->get_row($wpdb->prepare("Select form_type_id from form_info where order_num = %d", $order_number));
							if ($formID != "") {
								$boats_ordered = $wpdb->get_var($wpdb->prepare("Select sum(quantity) as totalspecials from product_orders where order_num = %d", $order_number));
							}

							
							if ($orderPaid) {
								$pdf->Row(array($productName, $customerName . " " . $customerPhone, $boat_type, $boats_ordered, $totalsale, $comments));
							}
						}
					}
				}
			}
			$pdf->Output($filename);
		} else {
			echo "Unauthorized!";
		}
	break;
	case "printschedule2":
		if (checkPermission()) {
			require_once(WP_PLUGIN_DIR . "/kybstore/lib/shared.php"); // store shared classes
			require_once(WP_PLUGIN_DIR . "/kybstore/lib/smarty.php"); // store smarty classes
			require_once(WP_PLUGIN_DIR . "/kybstore/controller/controller.php"); // controller classes
			require_once(WP_PLUGIN_DIR . "/kybstore/model/store_cart.php"); // cart classes
			$storecls = new cartprocesses;
			$schedule = "";
			$calID = 2;
			$startDate = date("Y-m-d", strtotime($cls->checkRequest('startdate')));
			$endDate = date("Y-m-d", strtotime($cls->checkRequest('enddate')));
			$startDate != $endDate ? $runDates = $cls->ReformatDatedb($startDate) . " - " . $cls->ReformatDatedb($endDate) : $runDates = $cls->ReformatDatedb($startDate);
			$sql = "SELECT *, MAKETIME(start_hour  + IF(start_hour<12, 12, 0), start_minute, 0) AS start_order from " . KYBCAL_TABLE . "kyb_calendar where dt between %s and %s and calendar_id = %d order by dt, start_order";
			$result = $wpdb->get_results($wpdb->prepare($sql, $startDate, $endDate, $calID));
			if (count($result) != 0) {
				foreach($result as $row) {
					$totalEvents = 0;
					$boat_type = "";				
					$booking_time = $row->start_hour . ":" . $row->start_minute . " " . $row->start_day;
					$booking_time .= " - " . $row->end_hour . ":" . $row->end_minute . " " . $row->end_day;
					$order_number = $row->order_num;
					$sqlOrders = "Select * from product_orders where order_num = %d";
					$resultOrders = $wpdb->get_results($wpdb->prepare($sqlOrders, $order_number));
					if (count($resultOrders) != 0) {
						$boats = $cls->BeginTable('100%', "0", "0", "0", "");
						foreach ($resultOrders as $ord) {
							$product_id = $ord->product_id;
							$productName = $wpdb->get_var($wpdb->prepare("Select product_name from products where product_id = %d", $product_id));
							$attributeInfo = $storecls->get_product_attributes($order_number, $product_id);
							
							if (is_array($attributeInfo->boatName)) {
								foreach ($attributeInfo->boatName as $k => $v) {
									$boat_type .= $v . "<br>";
									$boatarray = array(
										$v , $attributeInfo->numOrdered[$k]
									);
									$boats .= $cls->buildRow2($boatarray, '', '', count($boatarray), '', '', '', '', '', '', 'top');
								}
							}
						}
						$boats .= $cls->EndTable();
					}
					
					$orderPaid = $wpdb->get_var($wpdb->prepare("Select order_paid from orders where order_id = %d", $order_number));
					$booking_date = date("m/d/Y", strtotime($row->dt));
					$scharray = array(
						$booking_date, $booking_time,  $productName, $boats, ''
					);
					if ($orderPaid) {
						$schedule .= $cls->buildRow2($scharray, '', '', count($scharray), '', '', '', '', '', 'border:solid 1px #dddddd;', 'top');
					}
				}
			}
			$content = array (
				'SCHEDULE' => $schedule,
				'RUNDATES' => $runDates
			);
			$template = WP_PLUGIN_DIR . "/kybcalendar/view/html/cal_printout.html";
			$printout = $cls->showPage ($template, $content, true);
			//echo $printout;

			header("Content-type: application/vnd.ms-word; name='word'");
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=Booking_Schedule.doc");
			header("Pragma: no-cache");
			header("Expires: 0");		
			echo $printout;
		} else {
			echo "Unauthorized!";
		}
		
	break;
}

function checkPermission() {
	global $current_user;
	if ($current_user->roles[0] == "administrator") { 
		return true;
	}
	return false;
}
?>