<html>
<head>
    <style>

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        a.arrownavigation:active {
            font-size: 14px;
            font-family: Tahoma, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            text-decoration : underline;
        }

        a.arrownavigation:hover {
            font-size: 14px;
            font-family: Tahoma, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            text-decoration : underline;
        }

        a.arrownavigation:link {
            font-size: 14px;
            font-family: Tahoma, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            text-decoration : underline;
        }

        a.arrownavigation:visited {
            font-size: 14px;
            font-family: Tahoma, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            text-decoration : underline;
        }

        .arrownavigation {
            font-size: 14px;
            font-family: Tahoma, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            text-decoration : underline;
        }

        #caltop {
            background: url(images/calpopupbg_top.gif) top left no-repeat;
            width: 403px;
            height: 41px;
        }

        .datetitle {
            color: #607593;
            font-size: 11px;
            font-family: Verdana, sans-serif;
            font-weight: bold;
            margin-left: 15px;
            margin-top: 5px;
        }

        #calmiddle {
            background: url(images/calpopupbg_middle.gif) top left repeat-y;
            width: 403px;
            color: #464646;
            font-size: 10px;
            font-family: Verdana, sans-serif;
        }

        #calbottom {
            background: url(images/calpopupbg_bottom.gif) top left no-repeat;
            width: 403px;
            height: 27px;
        }


    </style>
    <?

//Get required files
// Include library files
    $baseDir = $_SERVER['DOCUMENT_ROOT'];
    require_once("$baseDir/wp-config.php"); // configuration
    $templateURL = get_bloginfo('template_url');

//======== Functions ========//
    function month_name($m)
    {
        switch ($m) {
            case 1:
                return ("January");
            case 2:
                return ("February");
            case 3:
                return ("March");
            case 4:
                return ("April");
            case 5:
                return ("May"); // needs to be different than "May"
            case 6:
                return ("June");
            case 7:
                return ("July");
            case 8:
                return ("August");
            case 9:
                return ("September");
            case 10:
                return ("October");
            case 11:
                return ("November");
            case 12:
                return ("December");
        }
        return "unknown-month($m)";
    }

    ?>
    <link href="<?php echo $templateURL . "/styles/admin_stylesheet.css"; ?>" rel="stylesheet" type="text/css">
</head>
<body>
<?

if (isset($prm) and $prm > 0) {
    $m = bcadd($prm, $chm);
} else {
    $m = date("m");
}

$d = date("d");     // Finds today's date
$y = date("Y");     // Finds today's year

$no_of_days = date('t', mktime(0, 0, 0, $m, 1, $y)); // This is to calculate number of days in a month

$mn = date('M', mktime(0, 0, 0, $m, 1, $y)); // Month is calculated to display at the top of the calendar

$yn = date('Y', mktime(0, 0, 0, $m, 1, $y)); // Year is calculated to display at the top of the calendar

$j = date('w', mktime(0, 0, 0, $m, 1, $y)); // This will calculate the week day of the first day of the month

$datelookup = $_GET['requestdate'];

$datefield = array();
$datefield = explode('-', $datelookup);
$form['date'] = array('yy' => $datefield[0], 'mm' => $datefield[1], 'dd' => $datefield[2]);

if ($form['date']['mm'] == "01") {
    $monthtext = "January";
} elseif ($form['date']['mm'] == "02") {
    $monthtext = "February";
} elseif ($form['date']['mm'] == "03") {
    $monthtext = "March";
} elseif ($form['date']['mm'] == "04") {
    $monthtext = "April";
} elseif ($form['date']['mm'] == "05") {
    $monthtext = "May";
} elseif ($form['date']['mm'] == "06") {
    $monthtext = "June";
} elseif ($form['date']['mm'] == "07") {
    $monthtext = "July";
} elseif ($form['date']['mm'] == "08") {
    $monthtext = "August";
} elseif ($form['date']['mm'] == "09") {
    $monthtext = "September";
} elseif ($form['date']['mm'] == "10") {
    $monthtext = "October";
} elseif ($form['date']['mm'] == "11") {
    $monthtext = "November";
} elseif ($form['date']['mm'] == "12") {
    $monthtext = "December";
}

$displaydate = $monthtext . " " . $form['date']['dd'] . ", " . $form['date']['yy'];


for ($k = 1; $k <= $j; $k++) { // Adjustment of date starting
    $adj .= "<td>&nbsp;</td>";
}
$sql = "Select * from kyb_calendar where DT = '$datelookup' and status = 1 order by START_TIME";
$result = $wpdb->get_results($sql);



?>
<div class="center">
    <table style="padding: 0;width: 403px; border: 0;">
        <tr>
            <td id="caltop">
                <div class="datetitle"><br/><? echo $displaydate; ?></div>
            </td>
        </tr>
        <tr>
            <td id="calmiddle" style="vertical-align:top">
                <?php
                if (count($result) != 0) {
                    ?>
                    <table class="center">
                        <? foreach ($result as $row) {
                        $DT = $row->DT;
                        $startMinute = $row->START_MINUTE;
                        $endMinute = $row->END_MINUTE;
                        $startHour = $row->START_HOUR;
                        $startDay = $row->START_DAY;
                        $endHour = $row->END_HOUR;
                        $endDay = $row->END_DAY;
                        $description = $row->DESCRIPTION;

                        $datefield = array();
                        $datefield = explode('-', $DT);
                        $form['date'] = array('yy' => $datefield[0], 'mm' => $datefield[1], 'dd' => $datefield[2]);

                        if (strlen($startMinute) < 2) {
                            $start_minute = "0" . $startMinute;
                        } else {
                            $start_minute = $startMinute;
                        }

                        if (strlen($endMinute) < 2) {
                            $end_minute = "0" . $endMinute;
                        } else {
                            $end_minute = $endMinute;
                        }

                        $start_time = $startHour . ":" . $start_minute . " " . $startDay;
                        $end_time = $endHour . ":" . $end_minute . " " . $endDay;

                        $monthtext = month_name($form['date']['mm']);
                        ?>
                        <tr>
                            <td class="text">
                                <div style="margin-left:20px;font-size:12px;"><? echo $start_time; ?>
                                    - <? echo $end_time; ?></div>
                        <tr>
                            <td class="text">
                                <div style="margin-left:20px;font-size:12px;"><? echo $description; ?></div>
                            </td>
                        <tr>
                        <tr>
                            <td style="text-align:center;">
                                <hr style="color:#29245E; width:150px;"/>
                            </td>
                        </tr>
                        <? } ?>
                    </table>
                    <? } else { ?>
                    <table class="center">
                        <tr>
                            <td class="text">
                                <div style="text-align:center;">No plans have been set for this date.</div>
                            </td>
                        </tr>

                    </table>
                    <? } ?>
            </td>
        </tr>
        <tr>
            <td id="calbottom" style="text-align: center;vertical-align: top;">&nbsp;</td>
        </tr>
    </table>
</BODY>
</HTML>