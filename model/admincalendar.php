<html>
<head>

    <script type="text/javascript">
        function NewWindow2(mypage, myname, w, h, scroll, pos) {
            var LeftPosition = "";
            var TopPosition = "";
            if (pos == "random") {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }

            if (pos == "center") {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            } else if ((pos != "center" && pos != "random") || pos == null) {
                LeftPosition = 0;
                TopPosition = 20
            }
            var settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';

            window.open(mypage, myname, settings);
        }

    </script>

</head>
<body>

<?

// Include library files
$baseDir = $_SERVER['DOCUMENT_ROOT'];
require_once("$baseDir/wp-config.php"); // configuration

define ('KYBCAL_FOLDER', dirname(plugin_basename(__FILE__)));
define ('KYBCAL_BASENAME', plugin_basename(__FILE__) );
!defined('WP_CONTENT_URL') ? define('WP_CONTENT_URL', site_url('wp-content', $scheme)) : '';
!defined('WP_CONTENT_DIR') ? define( 'WP_CONTENT_DIR', str_replace('\\', '/', ABSPATH) . 'wp-content' ) : '';
define ('KYBCAL_ABSPATH', WP_CONTENT_DIR . '/plugins/' . KYBCAL_FOLDER);
define ('KYBCAL_URLPATH', WP_PLUGIN_URL . "/" . KYBCAL_FOLDER);
define('KYBCAL_AJAXPATH', str_replace($_SERVER['DOCUMENT_ROOT'], "", WP_PLUGIN_DIR) . "/" . KYBCAL_FOLDER);


$imgUrl = get_bloginfo('template_url');
$pluginDir = KYBCAL_ABSPATH; 
$parseURL = parse_url($imgUrl);
$templatePath = $parseURL['path'];
$urlPath = KYBCAL_URLPATH;
$element = $_GET['element'];
$chm = "";
$prm = "";

if (isset($_GET['prm'])) {
    $prm = $_GET['prm'];
}

if (isset($_GET['chm'])) {
    $chm = $_GET['chm'];
}

if (isset($prm) and $prm > 0) {
    $m = bcadd($prm,$chm);
    $r = (int)date('m', mktime(0, 0, 0, $m, 1, $y));

} else {
    $m = date("m");
    $r = date("m");
}

$d = date("d");     // Finds today's date
$y = date("Y");     // Finds today's year

$no_of_days = date('t', mktime(0, 0, 0, $m, 1, $y)); // This is to calculate number of days in a month

$mn = date('F', mktime(0, 0, 0, $m, 1, $y)); // Month is calculated to display at the top of the calendar

$yn = date('Y', mktime(0, 0, 0, $m, 1, $y)); // Year is calculated to display at the top of the calendar

$j = date('w', mktime(0, 0, 0, $m, 1, $y)); // This will calculate the week day of the first day of the month

for ($k = 1; $k <= $j; $k++) { // Adjustment of date starting
    $adj .= "<td>&nbsp;</td>";
}

/// Starting of top line showing name of the days of the week

echo " <table  style='padding:0;text-align:center;'><tr><td>";

$prevLink = "$pluginDir/model/admincalendar.php?prm=$m&chm=-1&element=$element";
$nextLink = "$pluginDir/model/admincalendar.php?prm=$m&chm=1&element=$element";


echo "<table style='padding:0;text-align:center;width:196px;height:209px;background-color:#FFFFFF;' ><tr><td style='text-align:center;background-color:#ffffff;width:30px;height:24px;'><a style='font-weight:bold; color:#FFFFFF; font-size:10px; font-family:Tahoma, sans-serif;' href=\"javascript:void(0);\" onclick=\"showCal('$prevLink','$element')\"><img src=\"$imgUrl/images/premontharrow.gif\" border=\"0\"/></a> </td><td colspan=5 style='text-align:center;background-color:#ffffff;height: 24px;' ><span style='font-size:12px; font-family:Verdana, sans-serif; font-weight:bold; color:#333333'>$mn $yn </span> </td><td style='text-align: center; background-color: #ffffff;width:30px;height:24px;'><a style='font-weight:bold; color:#FFFFFF; font-size:10px; font-family:Tahoma, sans-serif;' href=\"javascript:void(0);\" onclick=\"showCal('$nextLink','$element')\"><img src=\"$imgUrl/images/nextmontharrow.gif\" border=\"0\"/></a> </td></tr><tr>";

echo "<td class='days'>Su</td><td class='days'>Mo</td><td class='days'>Tu</td><td class='days'>We</td><td class='days'>Th</td><td class='days'>Fr</td><td class='days'>Sa</td></tr><tr>";

////// End of the top line showing name of the days of the week//////////

///////////////////////////////////////////////////////////
// Control error reporting (display every error)
// Not to be used for real and public Web sites!
///////////////////////////////////////////////////////////
//ini_set('display_errors', 1);
//error_reporting(E_ALL);


//////// Starting of the days//////////
$lookup =  "";
$selectday = "";
for ($i = 1; $i <= $no_of_days; $i++) {

    $sql = "SELECT * from kyb_calendar where month = $r and day = $i and year = $yn";
    $result = $wpdb->get_results($sql);


    // Check the result of the query
    if (count($result) != 0) {
        foreach ($result as $row) {
            $selectday = $row->DAY;
            $lookup = $row->DT;
			$status = $row->STATUS;
        }
        if (isset($_REQUEST['view'])) {
            $viewLink = "$urlPath/model/viewday.php?prm=$m&requestdate=$lookup";
        } else {
            $viewLink = "$urlPath/model/viewday.php?prm=$m&requestdate=$lookup";
        }

		$viewLink = "admin.php?page=kybcalendar/index.php&process=viewday&lookup=$lookup";
        if ($i == $selectday) {
			
            echo $adj . "<td valign=top><div id='daybgselected'><div class='calendartext'><b><a style=\"color:#f4161b;\" href=\"$viewLink\" >$selectday<b /></div></div>";
        } else {
            echo $adj . "<td valign=top><div id='daybg'><div class='calendartext'>$i</div></div>"; // This will display the date inside the calendar cell
        }
    } else {
        echo $adj . "<td valign=top><div id='daybg'><div class='calendartext'>$i</div></div>"; // This will display the date inside the calendar cell
    }


    echo " </font></td>";
    $adj = '';
    $j++;
    if ($j == 7) {
        echo "</tr><tr>";
        $j = 0;
    }

}
echo "</tr></table></td></tr></table>";
?>
</body>
</html>