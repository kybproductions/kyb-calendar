<?php
/*******************************************************************************
* CALENDAR CLASS								                               *
*                                                                              *
* Version: 1.0                                                                 *
* Date:    09-03-2012                                                          *
* Author:  Kimla Y. Beasley													   *
* Copyright 2012 KYB PRODUCTIONS											   *
*******************************************************************************/
ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('log_errors',TRUE);
ini_set("error_log", WP_PLUGIN_DIR . "/kybcalendar/error_log.log");
ini_set('display_errors',FALSE);
class calendarclass extends calendarsharedProcesses implements calendarfunctions {	

	var $imageFolder;
	var $table;
	var $themetable;
	var $configtable;
	var $calendarURL;
	var $urlInfo;

	public function __construct() {
		parent::__construct();
		$this->imageFolder = KYBCAL_URLPATH . "/images";
		$this->table = KYBCAL_TABLE . "kyb_calendar";
		$this->themetable = KYBCAL_TABLE . "kyb_calendar_themes";
		$this->configtable = KYBCAL_TABLE . "kybcalendar_config";
		$this->calendarURL = get_permalink(11); //Get from settings
		$currURL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$urlInfo = parse_url($currURL, PHP_URL_PATH);
		$this->urlInfo = explode("/", $urlInfo);
		
	}
   
   public function update_calendar($process) {
		global $wpdb;
		$post_array = array();
     		//======== Set variables ========//
		$post_array['description']['h'] = $_POST['content'];
		$fullDate = $this->ReformatDateform($this->checkRequest('event_date_full'));
        list( $eventYear, $eventMonth, $eventDay ) = explode("-", $fullDate, 3);
		
		strlen($eventMonth) < 2 ? $eventMonth = "0" . $eventMonth : '';
		strlen($eventDay) < 2 ? $eventDay = "0" . $eventDay : '';
		$post_array['year']['i'] = $eventYear;
		$post_array['month']['i'] = $eventMonth;
		$post_array['day']['i'] = $eventDay;
		$post_array['dt']['s'] = $eventYear . "-" . $eventMonth . "-" . $eventDay;

		strlen($_POST['start_minute']) < 2 ? $startMinute = "0" . $_POST['start_minute'] : $startMinute = $_POST['start_minute'];
		$starttime = $_POST['start_hour'] . ":" .  $startMinute;
		$_POST['start_day'] == "PM" ? $_POST['start_time'] = (($_POST['start_hour'] + 12) . ":" . $startMinute) : $_POST['start_time'] = $starttime;

		strlen($_POST['end_minute']) < 2 ? $endMinute = "0" . $_POST['end_minute'] : $endMinute = $_POST['end_minute'];
		$endtime = $_POST['end_hour'] . ":" .  $endMinute;
		$_POST['end_day'] == "PM" ? $_POST['end_time'] = (($_POST['end_hour'] + 12) . ":" . $endMinute) : $_POST['end_time'] = $endtime;

		$post_array['event_start']['s'] = $eventYear . "-" . $eventMonth . "-" . $eventDay . " " . $starttime;
		$post_array['event_end']['s'] = $eventYear . "-" . $eventMonth . "-" . $eventDay . " " . $endtime;
		
		$ID = $_POST['id'];
		
		$post_array['start_time']['s'] = $starttime;
		$post_array['start_hour']['s'] = $this->checkRequest('start_hour');
		$post_array['start_minute']['s'] = $startMinute;
		$post_array['start_day']['s'] = $this->checkRequest('start_day');

		$post_array['end_time']['s'] = $endtime;
		$post_array['end_hour']['s'] = $this->checkRequest('end_hour');
		$post_array['end_minute']['s'] = $endMinute;
		$post_array['end_day']['s'] = $this->checkRequest('end_day');

		$post_array['calendartime']['s'] = $this->checkRequest('start_time');
		$post_array['calendarendtime']['s'] = $this->checkRequest('end_time');
		$process == "add" ? $process_text = "added" : $process_text = "updated";

		$post_array['calendar_id']['i'] = $this->checkRequest('calendar_id');
		$post_array['title']['s'] = $this->checkRequest('title');
		$post_array['event_timezone']['s'] = $this->checkRequest('event_timezone');
		$post_array['event_address']['s'] = $this->checkRequest('event_address');
		$post_array['event_city']['s'] = $this->checkRequest('event_city');
		$post_array['event_state']['s'] = $this->checkRequest('event_state');
		$post_array['event_zip']['s'] = $this->checkRequest('event_zip');
		$post_array['featured_image']['s'] = $this->checkRequest('featured_image');
		$post_array['featured_txt']['s'] = $this->checkRequest('featured_txt');
		$post_array['event_rsvp']['i'] = $this->checkRequest('event_rsvp');
		$post_array['rsvp_link']['s'] = $this->checkRequest('rsvp_link');
		$post_array['status']['i'] = $this->checkRequest('status');
		$return = "Calendar event has been $process_text successfully";
		$processID = $this->process_post($process, $post_array, $this->table, $ID, $return, 'id');
		//Add to event blog category 
		$process == "add" ? $ID = $processID : '';		
		$calID = $this->checkRequest('calendar_id');
		$post_array['wp_postID']['i'] = $this->processToBlog($calID, $ID);
		$postID = $post_array['wp_postID']['i'];
		if ($postID != "") {
          $post_array['cal_link']['s'] = get_permalink($postID);
		}

		//Update post ID
		$wpdb->query($wpdb->prepare("Update " . $this->table . " set wp_postID = %d where id = %d", $post_array['wp_postID'][i], $ID));

		if ($_POST['tRepeat'] > 0) {
          $this->process_repeat($post_array, $ID);
		} 
       if ($this->checkRequest('customdates') != '' ) {
          $this->process_customdates($post_array, $ID);
		}
		$modThis = $_POST['form']['modThis'];

		$this->process_mode($post_array, $ID, $modThis, 'update');
   }

	

	public function processToBlog($calID, $ID) {
		global $wpdb;
		$current_user = wp_get_current_user();
		//Get category ID from calendar theme
		$post_catID = $wpdb->get_var($wpdb->prepare("Select cal_wp_categoryID from " . $this->themetable . " where cal_id = %d", $calID));
		$wpCats = array($post_catID);
		$post_ID = $wpdb->get_var($wpdb->prepare("Select wp_postID from " . $this->table . " where id = %d", $ID));
		$post_ID != "" ? $postInfo = get_post($post_ID) : $postInfo = array();

		if (count($postInfo) != 0) {
			$post_parent = $postInfo->post_parent;
			$userID = $postInfo->post_author; //keep as user who originated post if updating.
			$post_date = $postInfo->post_date; //keep original date if updating
			$post_date_gmt = $postInfo->post_date_gmt; //keep original date gmt if updating
		} else {
			$post_parent = 0;
			$userID = $current_user->ID; //Add new user if adding
			$post_date = date('Y-m-d H:i:s');
			$post_date_gmt = date('Y-m-d H:i:s');
		}

		$post_content = $_POST['content'];
		$post_content = preg_replace("@<script[^>]*>.+</script[^>]*>@i", "", $post_content); 
		$excerpt = $this->get_excerpt($post_content, 100);
		$excerpt = str_replace("\r"," ",$excerpt);
		$excerpt = str_replace("\n"," ",$excerpt);
		$excerpt .= "...";
		$post_hook = "[kybcalendar cal_id=$calID eventid=$ID]";
		$post_title = $this->checkRequest('title');
		$_POST['status'] == 0 ? $post_status = 'private' : $post_status = 'publish';
		$post_ID != "" ? $post_name = get_post_field('post_name', $post_ID) : $post_name = $this->generate_user_permalink($post_title); //leave slug as is if already a post as not to change links that may have been sent out already

		$eventpost = array(
		  'comment_status' => 'closed',
		  'ping_status'    => 'open',
		  'post_author'    => $userID,
		  'post_content'   => $post_hook,
		  'post_category'  => $wpCats,
		  'post_date'      => $post_date,
		  'post_date_gmt'  => $post_date_gmt,
		  'post_excerpt'   => $excerpt,
		  'post_name'      => $post_name,
		  'post_parent'    => $post_parent,
		  'post_status'    => $post_status,
		  'post_title'     => $post_title,
		  'post_type'      =>'post',
		);  

		if ($post_ID != "0" && $post_ID != "") {
			//======== Add ID to array for updating ========//
			$eventpost['ID'] = $post_ID;
			$postID = $post_ID;
			wp_insert_post( $eventpost);
		} else {
			//======== Insert as new post ========//
			$postID = wp_insert_post( $eventpost);
		}

		//$wpCats = array('Events');

		//======== update post categories ========//
		wp_set_post_terms( $postID, $wpCats);
		//======== End update post categories ========//
		return $postID;
		

	}
   
	public function process_repeat($post_array, $ID) {
		global $wpdb;
		$post_array['recurring']['i'] = 1;
		$post_array['recurring_type']['s'] = $this->checkRequest('selPeriod');
		$post_array['recurring_period']['i'] = $this->checkRequest('tRepeat');
		$post_array['recurring_startdate']['s'] = $post_array['event_start']['s'];
		// update added event or updated event
		$this->process_post('update', $post_array, $this->table, $ID, $return, 'id');
		$startDate = $post_array['event_start']['s'];
		$repeat = $this->checkRequest('tRepeat');
		$calID = $this->checkRequest('calendar_id');
		$fullDate = $this->ReformatDateform($this->checkRequest('event_date_full'));
		list( $rollYear, $rollMonth, $rollDay ) = explode("-", $fullDate, 3);

		$period = $this->checkRequest('selPeriod');
		$wknd = $this->checkRequest('wknd');

		//======== Gather date arrays ========//
		for($k=0;$k<=$repeat;$k++){
			$newDate = mktime(0,0,0,$rollMonth,$rollDay,$rollYear);
			$datePeriod = $datePeriod . "'" . date("Y-m-d", $newDate) . "',";
			$datelist[] = date("Y-m-d", $newDate);
			$monthlist[] = $rollMonth;
			$daylist[] = $rollDay;
			$yearlist[] = $rollYear;
			$totalDays = cal_days_in_month(CAL_GREGORIAN, $rollMonth, $rollYear);

			if($period == "Day")   $rollDay++;
			if($period == "BusDay")$rollDay++;
			if($period == "Week")  $rollDay = $rollDay + 7;
			if($period == "Month") $rollMonth++; 
			if($period == "Year")  $rollYear++;
			
			if ($rollDay > $totalDays) {
				$calTotal = $rollDay - $totalDays;
				$rollMonth++;
				$rollDay = $calTotal;
			}

			if($period == "Month") {
				$currday = getdate(mktime(0,0,0,$rollMonth,$rollDay,$rollYear));
				$weekday = getdate(mktime(0,0,0, $post_array['month'][i], $post_array['day'][i], $post_array['year'][i]));
				if ($currday['wday'] != $weekday['wday']) {
					//Go back to first day within that week.
					if ($currday['wday'] > $weekday['wday']) {
						$totalBack = $currday['wday'] - $weekday['wday'];
						$rollDay = $currday['mday'] - $totalBack;
					} else {
						$totalBack = $weekday['wday'] - $currday['wday'];
						$rollDay = $currday['mday'] + $totalBack;
					}		
				}					
			}
		}

		//======== Insert recurring into database ========//
		for($j=1;$j<$repeat+1;$j++){
			$day = $datelist[$j];
			$month = $monthlist[$j];
			$dayof = $daylist[$j];
			$year = $yearlist[$j];
			$addto = true;
			$post_array['dt']['s'] = $day;
			$post_array['month']['i'] = $month;
			$post_array['day']['i'] = $dayof;
			$post_array['year']['i'] = $year;
			$return = "";
			if($period == "BusDay" || $wknd == 0) {
				$weekends = array("Saturday", "Sunday");
				$landdate = "$year/$month/$dayof"; 
				$landday = date('l', strtotime($landdate));
				if (in_array($landday, $weekends)) {
					$addto = false;
				} 
			}
			if ($addto) {
				$post_array['wp_postID']['i'] = 0; 
				$addID = $this->process_post('add', $post_array, $this->table, $ID, $return, 'id');

				if ($addID != "") {
					$items = array();
					$items['wp_postID']['i'] = $this->processToBlog($calID, $addID);
					$postID = $items['wp_postID']['i'];
					
					if ($postID != "") {
						$items['cal_link']['s'] = get_permalink($postID);
					}

					//======== Update recurring with repeat information ======//
					
					$items['recurring']['i'] = 1;
					$items['recurring_type']['s'] = $this->checkRequest('selPeriod');
					$items['recurring_period']['i'] = $this->checkRequest('tRepeat');
					$items['recurring_startdate']['s'] =  $startDate;
					$this->process_post('update', $items, $this->table, $addID, $return, 'id');
				}
			}

			//======== End Update ========//
		}

   }

   public function process_customdates($post_array, $ID) {
        global $wpdb;
		 $calID = $this->checkRequest('calendar_id');
     	 $customDates = $_POST['customdates'];
		$startDate = $post_array['event_start']['s'];
		 if (count($customDates) != 0) {
			$post_array['recurring'][i] = 1;
			$post_array['recurring_type'][s] = $this->checkRequest('selPeriod');
			$post_array['recurring_period'][i] = $this->checkRequest('tRepeat');
			$post_array['recurring_startdate'][s] = $post_array['event_start'][s];
// update added event or updated event
          $this->process_post('update', $post_array, $this->table, $ID, $return, 'id');
			foreach ($customDates as $key => $value) {
					if ($value != "") {
				       list( $rollMonth, $rollDay, $rollYear ) = explode("/", $value, 3);
						$post_array['dt'][s] = $rollYear . "-" . $rollMonth . "-" . $rollDay ;
						$post_array['month'][i] = $rollMonth;
						$post_array['day'][i] = $rollDay;
						$post_array['year'][i] = $rollYear;
						$return = "";
                    $addID = $this->process_post('add', $post_array, $this->table, 0, $return, 'id');		
                 }
				    if ($addID != "") {
					     $items = array();
					     $items['wp_postID']['i'] = $this->processToBlog($calID, $addID);
					     $postID = $items['wp_postID']['i'];
					
					     if ($postID != "") {
						     $items['cal_link']['s'] = get_permalink($postID);
					     }

					//======== Update recurring with repeat information ======//
					
					     $items['recurring']['i'] = 1;
					     $items['recurring_type']['s'] = $this->checkRequest('selPeriod');
					     $items['recurring_period']['i'] = $this->checkRequest('tRepeat');
					     $items['recurring_startdate']['s'] =  $startDate;
					     $this->process_post('update', $items, $this->table, $addID, $return, 'id');
					}					
				
			  }
       }
   }

   public function process_mode($post_array, $ID, $modThis, $process) {
         global $wpdb;
		//====== Get recurring Start Date ========//
		$sqlStart = "Select recurring_startdate from " . $this->table . " where id = %d";
		$rec_date = $wpdb->get_var($wpdb->prepare($sqlStart, $ID));
		switch ($modThis) {
			case 2:				
				//Set start query
				$sqllist = "Select id, dt, wp_postID, recurring_startdate, day from " . $this->table . " where recurring_startdate = %s";
				$resultlist = $wpdb->get_results($wpdb->prepare($sqllist, $rec_date));
				$currentDate = $post_array['year']['i'] . "-" . $post_array['month']['i'] . "-" . $post_array['day']['i'];

				if (count($resultlist) != 0) {
					foreach ($resultlist as $row) {

						if (strtotime($row->dt) > strtotime($currentDate)) {
							$id = $row->id;
							$wp_postID = $row->wp_postID;
							$post_array['dt'][s] = $row->dt;
							$post_array['day'][s] = $row->day;
							$return = "";
							$this->process_post($process, $post_array, $this->table, $id, $return, 'id');		
							if ($process == 'delete' && $wp_postID != '') {
								wp_delete_post($wp_postID, true);
							}						
						}
					}
				}
			break;
			case 3:
				//Set start query
				$sqllist = "Select id, dt, wp_postID, recurring_startdate, day, month, year from " . $this->table . " where recurring_startdate = %s";
				$resultlist = $wpdb->get_results($wpdb->prepare($sqllist, $rec_date));

				$rollMonth =  $this->checkRequest('month');
				$rollDay = $this->checkRequest('day');
				$rollYear = $this->checkRequest('year');
				$totalDays = cal_days_in_month(CAL_GREGORIAN, $rollMonth, $rollYear);
				$cnt = 1;
				if (count($resultlist) != 0) {
					foreach ($resultlist as $row) {
						$id = $row->id;
						$wp_postID = $row->wp_postID;
						$year = $this->checkRequest('year');
						$month = $this->checkRequest('month');
						$day = $this->checkRequest('day');
						$rollMonth =  $row->month;
						$setdate = $year . "-" . $month . "-" . $row->day;
						$currday = $row->day;
						if ($rollDay != $currday) {
							$rollDay = bcadd(7,$rollDay);
							if ($rollDay > $totalDays) {
								$calTotal = $rollDay - $totalDays;
								$rollMonth = bcadd(1,$rollMonth);
								$rollDay = $calTotal;
							}
						}
						
						$setdate = $year . "-" . $rollMonth . "-" . $rollDay;
						$currday = $rollDay;
					
						$post_array['dt'][s] = $row->dt;
						$post_array['day'][i] = $row->day;	
						$post_array['month'][i] = $row->month;
						$return = "";
						$this->process_post($process, $post_array, $this->table, $id, $return, 'id');		
						if ($process == 'delete' && $wp_postID != '') {
                          wp_delete_post($wp_postID, true);
						}
					}
				}		
			break;
	       default:
            switch($process) {
               case "delete":
				  	$wp_postID = $wpdb->get_var($wpdb->prepare("Select wp_postID from " . $this->table . " where id = %d", $ID));
				$return = "Calendar event has been deleted successfully";
$postCount = $wpdb->get_var($wpdb->prepare("Select Count(id) as totalPosts from " . $this->table . " where wp_postID = %d", $ID));
			         $this->process_post($process, $post_array, $this->table, $ID, $return, 'id');	

	             if ($wp_postID != "" && $postCount == 1) {
			//======== Begin delete from blog if none are recurring in the calendar ========//			
			            wp_delete_post($wp_postID, true);
		            }
                 break;
               }
			break;
		}
   }

	

	public function get_theme_style($calID) {
		global $wpdb;
		
		$row = $this->get_calInfo($calID);
		$row->arrows_style == "bolditalic" ? $arrows_style = "font-weight:bold;font-style:italic;" : $arrows_style = $row->arrows_style == "italic" ? "font-style:" . $row->arrows_style . ";" : "font-weight:" . $row->arrows_style . ";";
		$row->monyr_style == "bolditalic" ? $monyr_style = "font-weight:bold;font-style:italic;" : $monyr_style = $row->monyr_style == "italic" ? "font-style:" . $row->monyr_style . ";" : "font-weight:" . $row->monyr_style . ";";
		$row->wkname_style == "bolditalic" ? $wkname_style = "font-weight:bold;font-style:italic;" : $wkname_style = $row->wkname_style == "italic" ? "font-style:" . $row->wkname_style . ";" : "font-weight:" . $row->wkname_style . ";";
		$row->wkday_style == "bolditalic" ? $wkday_style = "font-weight:bold;font-style:italic" : $wkday_style = $row->wkday_style == "italic" ? "font-style:" . $row->wkday_style : "font-weight:" . $row->wkday_style;
		$row->selwkday_style == "bolditalic" ? $selwkday_style = "font-weight:bold;font-style:italic" : $selwkday_style = $row->selwkday_style == "italic" ? "font-style:" . $row->selwkday_style : "font-weight:" . $row->selwkday_style;
		$row->event_link_style == "bolditalic" ? $eventlink_style = "font-weight:bold;font-style:italic" : $eventlink_style = $row->event_link_style == "italic" ? "font-style:" . $row->event_link_style : "font-weight:" . $row->event_link_style;
		$row->apply_style == "bolditalic" ? $apply_style = "font-weight:bold;font-style:italic" : $apply_style = $row->apply_style == "italic" ? "font-style:" . $row->apply_style : "font-weight:" . $row->apply_style;
		$row->direct_style == "bolditalic" ? $direct_style = "font-weight:bold;font-style:italic" : $direct_style = $row->direct_style == "italic" ? "font-style:" . $row->direct_style : "font-weight:" . $row->direct_style;
		$row->view_style == "bolditalic" ? $view_style = "font-weight:bold;font-style:italic" : $view_style = $row->view_style == "italic" ? "font-style:" . $row->view_style : "font-weight:" . $row->view_style;
		$row->current_style == "bolditalic" ? $current_style = "font-weight:bold;font-style:italic" : $current_style = $row->current_style == "italic" ? "font-style:" . $row->current_style : "font-weight:" . $row->current_style;
		
		
		$row->arrow_border != "" ? $arrows_style .= ";border:solid {$row->arrow_border} 1px" : '';
		$row->monyr_border != "" ? $monyr_style .= ";border:solid {$row->monyr_border} 1px" : '';
		$row->wkday_border != "" ? $wkday_style .= ";border:solid {$row->wkday_border} 1px" : '';
		$row->wkname_border != "" ? $wkname_style .= ";border:solid {$row->wkname_border} 1px" : '';
		$row->eventlink_border != "" ? $eventlink_style .= ";border:solid {$row->eventlink_border} 1px" : '';
		$row->selwkday_border != "" ? $selwkday_style .= ";border:solid {$row->selwkday_border} 1px" : '';
		$row->apply_border != "" ? $apply_style .= ";border:solid {$row->apply_border} 1px" : '';
		$row->direct_border != "" ? $direct_style .= ";border:solid {$row->direct_border} 1px;" : '';
		$row->view_border != "" ? $view_style .= ";border:solid {$row->view_border} 1px" : '';
		$row->current_border != "" ? $current_style .= ";border:solid {$row->current_border} 1px" : '';

		
		$row->wkday_image != "" ? $wkday_image = "background:url(" . $row->wkday_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $wkday_image = "";
		$row->selwkday_image != "" ? $selwkday_image = "background:url(" . $row->selwkday_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $selwkday_image = "";
		$row->arrow_image != "" ? $arrow_image = "background:url(" . $row->arrow_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $arrow_image = "";
		$row->monyr_image != "" ? $monyr_image = "background:url(" . $row->monyr_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $monyr_image = "";
		$row->wkname_image != "" ? $wkname_image = "background:url(" . $row->wkname_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $wkname_image = "";
		$row->event_link_image != "" ? $event_link_image = "background:url(" . $row->event_link_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $event_link_image = "";
		$row->apply_image != "" ? $apply_image = "background:url(" . $row->apply_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $apply_image = "";
		$row->direct_image != "" ? $direct_image = "background:url(" . $row->direct_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $direct_image = "";
		$row->view_image != "" ? $view_image = "background:url(" . $row->view_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $view_image = "";
		$row->current_image != "" ? $current_image = "background:url(" . $row->current_image . ") top left no-repeat;background-size:100% 100%;padding-left:10px;padding-top:10px;" : $current_image = "";

		$selected_fonts = array($row->arrows_font, $row->monyr_font, $row->wkday_font, $row->wkname_font, $row->selwkday_font, $row->event_link_font, $row->apply_font, $row->direct_font, $row->view_font, $row->current_font);

		//Determine imports
		$configInfo = $wpdb->get_row("Select * from " . $this->configtable);			
		$fontChoices = json_decode($configInfo->fonts, true);
		$importItems = array();
		$imports = "";
			
		foreach ($fontChoices as $key => $value) {
			foreach ($value as $k => $v) {
				//echo $k . "=" . $v . "<br>";
				if (in_array($k, $selected_fonts)) {
					foreach ($v as $kv => $iv) {
						if ($iv != "") {							
							$importItems[$k] = $iv;
						}
					}
				}
			}
		}

		if (count($importItems) != 0) {
			foreach ($importItems as $key => $value) {
				$imports .= "@import url($value);\n";
			}
		}
				
		$calstyletemplate = KYBCAL_ABSPATH . "/view/html/calendar_style.html";
		$calstylecontent = array (
			'arrowfont' => $row->arrows_font,
			'monyrfont' => $row->monyr_font,
			'wkdayfont' => $row->wkday_font,
			'wknamefont' => $row->wkname_font,
			'selwkdayfont' => $row->selwkday_font,
			'wkdaylinkfont' => $row->event_link_font,
			'applyfont' => $row->apply_font,
			'directfont' => $row->direct_font,
			'viewfont' => $row->view_font,
			'currentfont' => $row->current_font,
			'arrowfontsize' => $row->arrows_fontsize,
			'monyrfontsize' => $row->monyr_fontsize,
			'wkdayfontsize' => $row->wkday_fontsize,
			'wknamefontsize' => $row->wkname_fontsize,
			'selwkdayfontsize' => $row->selwkday_fontsize,
			'wkdaylinkfontsize' => $row->event_link_fontsize,
			'applyfontsize' => $row->apply_fontsize == "" ? "12px" : $row->apply_fontsize,
			'directfontsize' => $row->direct_fontsize,
			'viewfontsize' => $row->view_fontsize,
			'currentfontsize' => $row->current_fontsize,
			'arrowcolor' => $row->arrow_color,
			'monyrcolor' => $row->monyr_color,
			'wkdaycolor' => $row->wkday_color,
			'wknamecolor' => $row->wkname_color,
			'selwkdaycolor' => $row->selwkday_color,
			'wkdaylinkcolor' => $row->event_link_color,
			'applycolor' => $row->apply_color,
			'directcolor' => $row->direct_color,
			'viewcolor' => $row->view_color,
			'currentcolor' => $row->current_color,
			'arrowsstyle' => $arrows_style,
			'monyrstyle' => $monyr_style,
			'wkdaystyle' => $wkday_style,
			'wknamefontstyle' => $wkname_style,
			'selwkdaystyle' => $selwkday_style,
			'wkdaylinkstyle' => $eventlink_style,
			'applystyle' => $apply_style,
			'directstyle' => $direct_style,
			'viewstyle' => $view_style,
			'currentstyle' => $current_style,
			'arrowborder' => $row->arrow_border,
			'monyrborder' =>  $row->monyr_border,
			'wkdayborder' => $row->wkday_border,
			'wknameborder' => $row->wkname_border,
			'selwkdayborder' => $row->selwkday_border,
			'wkdaylinkborder' => $row->eventlink_border,
			'applyborder' => $row->apply_border,
			'directborder' => $row->direct_border,
			'viewborder' => $row->view_border,
			'currentborder' => $row->current_border,
			'arrowbgcolor' => $row->arrow_bgcolor,
			'monyrbgcolor' =>  $row->monyr_bgcolor,
			'wkdaybgcolor' => $row->wkday_bgcolor,
			'wknamebgcolor' => $row->wkname_bgcolor,
			'selwkdaybgcolor' => $row->selwkday_bgcolor,
			'wkdaylinkbgcolor' => $row->eventlink_bgcolor,
			'applybgcolor' => $row->apply_bgcolor == "" ? "#5c5c5c" : $row->apply_bgcolor,
			'directbgcolor' => $row->direct_bgcolor,
			'viewbgcolor' => $row->view_bgcolor,
			'currentbgcolor' => $row->current_bgcolor,
			'arrowimage' => $arrow_image,
			'monyrimage' => $monyr_image,
			'wkdayimage' => $wkday_image,
			'wknameimage' => $wkname_image,
			'selwkdayimage' => $selwkday_image,
			'wkdaylinkimage' => $event_link_image,
			'applyimage' => $apply_image,
			'directimage' => $direct_image,
			'viewimage' => $view_image,
			'currentimage' => $current_image,	
			'imports' => $imports
		);
		$calStyle = $this->showPage ($calstyletemplate, $calstylecontent, true);
		return $calStyle;
	}
	public function get_calheaders($beginDay) {
		$headerdays = "";
		/*Tuesday = 6
		   Wednesday = 5
		   Thursday = 4
		   Friday = 3
		   Saturday = 2
		   Sunday = 1
		   Monday = 0*/
		
		switch ($beginDay) {
			case 0:
				$days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
			break;
			case 2:
				$days = array('Sat', 'Sun', 'Mon', 'Tue', 'Thu', 'Fri', 'Sat');
			break;
			case 3:
				$days = array('Fri', 'Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu');
			break;
			case 4:
				$days = array('Thu', 'Fri', 'Sat', 'Sun', 'Mon', 'Tue', 'Wed');
			break;
			case 5:
				$days = array('Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Mon', 'Tue');
			break;
			case 6:
				$days = array('Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Mon');
			break;
			default:
				$days = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
			break;
		}

		foreach($days as $k => $v) {
          $headerdays .= sprintf( __('<td align="center" class="col1 wkdayname">%s</td>', 'aisd'), $v);
		}
		
		return $headerdays;
	}

	public function viewCalendar($calID, $index) {
		$row = $this->get_calInfo($calID);		
		//Intialize variables
		$ajaxURL = KYBCAL_AJAXPATH . "/model/calendar_ajax.php";
		$calStyle = $this->get_theme_style($calID);
		$btn = "Add New Event";
		$process = "updatesubmit";
		$prm = $this->checkRequest('prm');
		$chm = $this->checkRequest('chm'); 
		$yr = $this->checkRequest('y');
		$this->checkRequest('view') != "" ? $currview = $this->checkRequest('view') : $currview = "Month";
		$currmo = date('m');
		$curryr = date('Y');
       $prevcuurday = $this->checkRequest('cday');
		$this->checkRequest("dy") != "" ? $currday = $this->checkRequest("dy") : $currday = "01";
		if(isset($prm) && $prm != "" && prm >= 0){
			if ($this->checkRequest('view') == "Week") {
				$dt = strtotime($this->checkRequest('wk'));
				$chm == "-1" ? $datestart = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last sunday', $dt)) : $datestart = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('first sunday', $dt));
				$this->checkRequest('wk') != "" ? $m = date("m",strtotime($datestart)) : $m = $prm;
			} else if ($this->checkRequest('view') == "Day") {
              		$currno_of_days = date('t',mktime(0,0,0,$prm, 1,$yr)); //This is to calculate number of days in a month



              if ($chm == '1' && $currday == $currno_of_days) {
                  $m = bcadd($prm,$chm);
              } else if ($chm == '-1' && $prevcuurday == '1') {
                   $m = bcadd($prm,$chm);
                    $currday = date('t',mktime(0,0,0,$m, 1,$yr));
                    
              } else {
             
				$m = $prm;
               }


			} else {
				$m = bcadd($prm,$chm);
			}
			$r=(int)date('m',mktime(0,0,0,$m,1,$y));
		} else {
			$m= date("m");
			$r=date("m");
		}
		
		$imageFolder = parse_url($this->imageFolder);
		$beginDay = $row->start_wday;
		$d= date("d");     // Finds today's date
		isset($yr) && $yr != "" ? $y = $yr : $y= date("Y");     // Finds today's year
		$no_of_days = date('t',mktime(0,0,0,$m,1,$y)); // This is to calculate number of days in a month
		$mn=date('M',mktime(0,0,0,$m,1,$y)); // Month is calculated to display at the top of the calendar
		$yn=date('Y',mktime(0,0,0,$m,1,$y)); // Year is calculated to display at the top of the calendar
		$j= date('w',mktime(0,0,0,$m,$beginDay,$y)); // This will calculate the week day of the first day of the month		
		$headerdays = $this->get_calheaders($beginDay);		
		$monthoptions =  $this->monthoptions($r, $m, $yn);
		$monthName = $this->month_name($m);
		$singlemonthoptions = $this->singlemonthoptions($r, $m, $yn);
		$yearoptions = $this->yearoptions($y);
		$sqlThemes = "Select * from " . $this->themetable;
		$themeoptions = $this->build_dboptions($sqlThemes, 'cal_title', 'cal_id', $calID, true);
		$calDays = $this->calViewDays($no_of_days, $r, $yn, $j, $row->wp_pageID, $calID, true, $index);
		$calScript = $this->calScript($no_of_days, $r, $yn);
		$month = $this->month_name($m) . " " . $y;
		$url = get_permalink($row->wp_pageID);
		$eventListings = $this->getListings($m, $y, $no_of_days, $calID);
		$minical = $this->viewSidebarCal(false, 'minical');
		
		switch ($currview) {
			case "List":
				$mthselected = "";
				$yrselected = "";
				$dayselected = "";
				$wkselected = "";
				$listselected = "btn_selected";
				$yrviewclass = "closed";
				$mthviewclass = "closed";
				$wkviewclass = "closed";
				$dayviewclass = "closed";
				$row->show_list = 1;
				$calview = $eventListings;
			break;
			case "Year":				
				$mthselected = "";
				$yrselected = "btn_selected";
				$dayselected = "";
				$wkselected = "";
				$listselected = "";
				$yrviewclass = "open";
				$mthviewclass = "closed";
				$wkviewclass = "closed";
				$dayviewclass = "closed";
				$month = $y;
				$calview = $this->getCalView($calID, $y, 'year', $url, $currview, $arrow_bgcolor, $row->wp_pageID);
			break;
			case "Month":
				$mthselected = "btn_selected";
				$yrselected = "border_right_none";
				$dayselected = "";
				$listselected = "";
				$wkselected = "";
				$calviewtemplate = KYBCAL_ABSPATH . "/view/html/cal_month_view.html";
				$calviewcontent = array (
					'MONTHNAME' => $monthName,
					'YEAR' => $y,
					'CALENDAR' => $calDays,
					'arrowbgcolor' => $arrow_bgcolor,
				);
				$calview = $this->showPage ($calviewtemplate, $calviewcontent, true);
			break;
			case "Week":
             $daterange = $this->get_daterange($m, $chm, $this->checkRequest('nav'), $this->checkRequest('wk'), $yr);
             $datestart = $daterange[0];
             $dateend = $daterange[1];
             $month = "Week of " . $this->ReformatDatedb($datestart);
				$mthselected = "border_right_none";
				$yrselected = "";
				$dayselected = "";
				$listselected = "";
				$wkselected = "btn_selected";
				$yrviewclass = "closed";
				$mthviewclass = "closed";
				$wkviewclass = "open";
				$dayviewclass = "closed";
				//$calview = "Week view here";
				$calview = $this->getCalView($calID, $y, 'week', $url, $currview, $arrow_bgcolor, $row->wp_pageID);
			break;
			case "Day":
              $dayName = date("l M d, Y", strtotime ($yr . '-' . $m . '-' . $currday));
              $month = "$dayName";
				$mthselected = "";
				$yrselected = "";
				$listselected = "";
				$dayselected = "btn_selected";
				$wkselected = "border_right_none";
				$yrviewclass = "closed";
				$mthviewclass = "closed";
				$wkviewclass = "closed";
				$dayviewclass = "open";
				$calview = $this->getCalView($calID, $y, 'day', $url, $currview, $arrow_bgcolor, $row->wp_pageID);
			break;
			default:
				$mthselected = "btn_selected";
				$yrselected = "";
				$dayselected = "";
				$wkselected = "";
				$yrviewclass = "closed";
				$mthviewclass = "open";
				$wkviewclass = "closed";
				$dayviewclass = "closed";
				$calviewtemplate = KYBCAL_ABSPATH . "/view/html/cal_month_view.html";
				$calviewcontent = array (
					'MONTHNAME' => $monthName,
					'YEAR' => $y,
					'CALENDAR' => $calDays,
					'arrowbgcolor' => $arrow_bgcolor,
				);
				$calview = $this->showPage ($calviewtemplate, $calviewcontent, true);
			break;
		}

		 
		//======== Return Data Information ========//
		$row->show_list == 1 ? $template= "calendar_view_wlist.html": $template = "calendar_view_index.html";
		$row->show_nextprev == 1 ? $shownextprev = "open" : $shownextprev = "closed";
		$row->show_views == 1 ? $showviews = "open" : $showviews = "closed";
		$row->show_myapply == 1 ? $showapply = "open" : $showapply = "closed";
		$index ? $template = "calendar_index.html" : '';

		//Calendar buttons //
		$calbtntemplate = KYBCAL_ABSPATH . "/view/html/calendar_buttons.html";
		$calbtncontent = array (
			'AJAXURL' => $ajaxURL,
			'CALID' => $calID,
			'MONTH' => $m,
			'YEAR' => $y,
			'SHOWMTHYR' => $showapply,
			'SINGLEMONTHOPTIONS' => $singlemonthoptions,
			'YEAROPTIONS' => $yearoptions,
			'CURRVIEW' => $currview,
			'THISMONTH' => $currmo,
			'THISYEAR' => $curryr,
			'SHOWNEXTPREV' => $shownextprev,
			'SHOWVIEWS' => $showviews,
			'CURRDAY' => $currday,
			'yrselected' => $yrselected,
			'mthselected' => $mthselected,
			'wkselected' => $wkselected,
			'dayselected' => $dayselected,
			'listselected' => $listselected,
		);
		$calbuttons = $this->showPage ($calbtntemplate, $calbtncontent, true);
		
		$content = array (
			'AJAXURL' => $ajaxURL,
			'MONTH' => $m,
			'YEAR' => $y,
			'CALSTYLE' => $calStyle,
			'CALID' => $calID,
			'CURRDAY' => $currday,
			'CURRVIEW' => $currview,
			'CALBUTTONS' => $calbuttons,
			'ADMINLINK' => admin_url(),
			'CALTITLE' => stripslashes($row->calTitle),
			'CALSCRIPT' => $calScript,				
			'SINGLEMONTHOPTIONS' => $singlemonthoptions,
			'YEAROPTIONS' => $yearoptions,
			'ADMINURL' => $this->adminURL,
			'URL' => $url,
			'PROCESS' => $process,
			'BUTTON' => $btn,
			'IMAGEPATH' => $imageFolder['path'],
			'MONTHOPTIONS' => $monthoptions,			
			'THEMEOPTIONS' => $themeoptions,			
			'CURRMONTH' => $month,			
			'MONTHNAME' => $monthName,			
			'CALENDAR' => $calDays,
			'EVENTLISTINGS' => $eventListings,		
			'MINICAL' => $minical,
			'arrowbgcolor' => $arrow_bgcolor,
			'arrowcolor' => $arrow_color,
			'arrowfont' => $arrows_font,
			'arrows_style' => $arrows_style,
			'monyrbgcolor' => $monyr_bgcolor,
			'wknamebgcolor' => $wkname_bgcolor,
			'wknamecolor' => $wkname_color,
			'wknamefont' => $wkname_font,
			'wknamefontstyle' => $wkname_style,
			'monyrcolor' =>$monyr_color,
			'monyrfont' => $monyr_font,
			'monyrstyle' => $monyr_style,
			'monyrbgcolor' => $monyr_bgcolor,
			'wkdaylinkcolor' => $event_link_color,
			'wkdaycolor' => $wkday_color,
			'wkdayfont' => $wkday_font,
			'wkdaystyle' => $wkday_style,
			'wkdaybgcolor' => $wkday_bgcolor,
			'wkday_image' => $wkday_image,
			'selwkdaycolor' => $selwkday_color,
			'selwkdayfont' => $selwkday_font,
			'selwkdaystyle' => $selwkday_style,
			'selwkdaybgcolor' => $selwkday_bgcolor,
			'selwkday_image' => $selwkday_image,			
			'CALHEADERS' => $headerdays,
			'CALVIEW' => $calview,			
			'HTML' => $template
		);
		return $content;
	}

	public function getCalView($calID, $yr, $viewtype, $url, $currview, $arrow_bgcolor, $wp_pageID) {
		global $wpdb;
		$selectView = "";
		$titleview = "";
		$no_of_days = 7;
		$ajaxURL = KYBCAL_AJAXPATH . "/model/calendar_ajax.php";
		switch ($viewtype) {
			case "year":				
				$calView = $this->BeginTable('100%', "0", "2", "2", "", "", 'background-color:#FFFFFF;');
				$calView .= $this->BeginTR('', '', '', '', '', '');
				$cnt = 1;
				$currDay = date('Y-m-d');
				for ($i=1; $i<=12;$i++) {					
					$sql = "SELECT Count(id) as totalevents from " . $this->table . " where month = %d and year = %d and status = %d and calendar_id = %d";
					$eventCnt = $wpdb->get_var($wpdb->prepare($sql, $i, $yr, 1, $calID));
					$calView .= $this->BeginTD('25%', '', '', '', '', 'wkday cursor', '', '', 'middle', "calendar.view.calNav('$ajaxURL', '$i','0', '$yr', 'month', $currDay, '$calID', false);");
					$calView .= "<div class='mthviewmth'>" . $this->month_name($i) . "</div>";
					$eventCnt != 0 ? $eventCnt == 1 ? $calView .= "<div class='caltext eventnum'><a class='caltext' href='$url?prm=$i&chm=0&y=$yr&view=Month'>$eventCnt event</a></div>" : $calView .= "<div class='caltext eventnum'><a class='caltext' href='$url?prm=$i&chm=0&y=$yr&view=Month'>$eventCnt events</a></div>" : '';
					$calView .= $this->EndTD();
					if ($cnt == 4) {
						$calView .= $this->EndTR();
						$calView .= $this->BeginTR('', '', '', '', '', '');
						$cnt = 0;
					}
					$cnt++;
				}
				$calView .= $this->EndTR();
				$calView .= $this->EndTable();
				$calheader = $yr;
				$titleview = "display:none;";
			break;
			case "week":				
				$m = $this->checkRequest('prm');
				$chm = $this->checkRequest('chm');

             $daterange = $this->get_daterange($m, $chm, $this->checkRequest('nav'), $this->checkRequest('wk'), $yr);
             $datestart = $daterange[0];
             $dateend = $daterange[1];
				$datetime1 = new DateTime($datestart);
				$datetime2 = new DateTime($dateend);
				if (function_exists('date_diff')) {
					$interval = date_diff($datetime1, $datetime2);
					$no_of_days = $interval->days;
				} else {
					//$interval = $datetime1->diff($datetime2);
					$interval = abs(strtotime($dateend) - strtotime($datestart));
					

					$years = floor($interval / (365*60*60*24));
					$months = floor(($interval - $years * 365*60*60*24) / (30*60*60*24));
					$no_of_days = floor(($interval - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
					
				}
				

				$r=(int)date('m',$dt);
				$yn=date('Y',$dt); // Year is calculated to display at the top of the calendar
				$j= date('d', strtotime($datestart)); // This will calculate the week day of the first day of the month
								
				$calView .= $this->BeginTR('', '', '', '', '', '');
				$calView .= $this->calViewDays(7, $r, $yn, $j, $wp_pageID, $calID, false, false) ;
				$calView .= $this->EndTR();
				
				$calView .= "<tr><td colspan='7'><input type='hidden' name='currweek' id='currweek' value='$datestart'></td></tr>";
				$calheader = "Week of " . $this->ReformatDatedb($datestart);
			break;
			case "day":				
				$m = $this->checkRequest('prm');
				$chm = $this->checkRequest('chm');
				$this->checkRequest('dy') != "" ? $d = $this->checkRequest('dy') : $d  = "01";
             	$currno_of_days = date('t',mktime(0,0,0,$m, 1,$yr));
              $prevcuurday = $this->checkRequest('cday');
		       $this->checkRequest("dy") != "" ? $currday = $this->checkRequest("dy") : $currday = "01";

              if ($chm == '1' && $currday == $currno_of_days) {
                  $m = bcadd($m,$chm);
              } else if ($chm == '-1' && $prevcuurday == '1') {
                   $m = bcadd($m,$chm);
                    $d = date('t',mktime(0,0,0,$m, 1,$yr));
                    
              }
				strlen($d) < 2 ? $d = "0" . $d : '';
				$datestr  = "$yr-$m-$d";
				$calheader = "Full Day View<br>  " . $this->ReformatDatedb($datestr);
				$calContent = $this->viewEvents ('', $datestr, $calID, true);
				$calContent['EVENTLIST'] = "<tr><td colspan='7' style='padding-top:10px;'>" . $calContent['EVENTLIST'] . "</td></tr>";
				
				is_array($calContent) ? $html = $calContent['HTML'] : $html = '';
				
				if ($html != "") {
					$template =  KYBCAL_ABSPATH . "/view/html/$html";	
					$calView = "<input type='hidden' name='currday' id='currday' value='$d'>";
					$calView .= $this->showPage ($template, $calContent, true); 
				}
			break;
		}
		$calviewtemplate = KYBCAL_ABSPATH . "/view/html/cal_select_view.html";
		$calviewcontent = array (
			'URL' => $url,
			'MONTHNAME' => $monthName,
			'CURRVIEW' => $currview,
			'CALHEADER' => $calheader,
			'CALVIEW' => $calView,
			'TITLEVIEW' => $titleview
		);
		$selectView = $this->showPage ($calviewtemplate, $calviewcontent, true);
		return $selectView;
	}

	public function get_daterange($m, $chm, $nav, $wk, $yr) {
		    $nav == 'today' ? $chm = "-1" : '';
			 strlen($m) < 2 ? $m = "0" . $m : '';
			 $wk != "" ? $datestr = $wk : $datestr  = $yr . "-" . $m . "-01";
				$j= date('d', strtotime($datestr));
				$dt = strtotime($datestr);
				$chm == "-1" ? $datestart = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last sunday', $dt)) : $datestart = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('first sunday', $dt));
				$dateend = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next saturday', $dt));

         return array($datestart, $dateend);
    }

	public function getListings($m, $y, $no_of_days, $calID) {
		global $wpdb;
		$themeURL = $this->templateURL;
		$calURL = $this->calendarURL;
		$startDate = "$y-$m-01";
		$endDate = "$y-$m-$no_of_days";
		$sql = "Select * from " . $this->table . " where dt >= %s and dt <= %s and calendar_id = %d and status = 1 order by dt";
		$result = $wpdb->get_results($wpdb->prepare($sql, $startDate, $endDate, $calID));
		$listings = "<div>";
		
		if (count($result) != 0) {
			foreach ($result as $row) {
				$eventID = $row->id;
				$eventDate = $this->ReformatDatedb($row->dt);
				list( $eventYear, $eventMonth, $eventDay ) = explode("-", $row->dt, 3);
				
				$eventTitle = stripslashes($row->title);
              $rsvp_link = stripslashes($row->rsvp_link);
				$wp_postID = $row->wp_postID;
				$eventLink = get_permalink($wp_postID);
				$eventDesc = apply_filters('the_content', stripslashes($row->description));
				$eventDesc = str_replace("<p>", "", $eventDesc);
				$eventDesc = str_replace("</p>", "<br><br>", $eventDesc);
				$eventDesc = strip_tags($eventDesc, "<b></b><strong></strong><a></a><i></i><br>");
				$row->featured_txt != "" ? $excerpt = $row->featured_txt : $excerpt = $this->get_excerpt(stripslashes($row->description), 30);
				$listInfo .= "<font color=\"464646\"><b>$eventDate</b></font><b><br>";
				//$listInfo .= "<a class=\"style1 callink\" href=\"$eventLink\">$eventTitle</a></b>";
				$listInfo .= "<font color=\"7b988f\">$eventTitle</font>";
				$listInfo .= "<div style='width:240px;overflow:hidden;padding-top:0px;line-height:18px;color:#464646;' class='location'>$eventDesc</div>";
				
				$listarray = array(
					$listInfo
					);
				//$listings .= $this->buildRow2($listarray, '', '', count($listarray), '', '', '', '', '', "background: $bgcolor;vertical-align:top",'');
				//$listings .= "<tr><td>&nbsp;</td></tr>";
				/*$listings .= "<div class=\"row-fluid event_blockdate\">";
				$listings .= "<span class='span2'><div class='event_dateblock'><div class='datetxt'>$eventDay</div></div></span>";
				$listings .= "<span class='span8 event_title'>$eventTitle<div class='event_feat_txt'>$excerpt</div></span>";
				$listings .= "</div>";*/
				$event_address = "";
				$event_time = "";
				$event_dayname = date('D', strtotime($row->dt));
				$row->start_hour != 0 ? $event_time .= $row->start_hour . ":" . $row->start_minute . " " . $row->start_day : '';
				$row->end_hour != 0 ? $event_time .= " - " . $row->end_hour . ":" . $row->end_minute . " " . $row->end_day : '';
				$event_time != "" ? $event_time = "<span class='icon-clock fs1'></span>&nbsp;$event_time" : '';
				$row->event_address != "" ? $event_address .= $row->event_address : '';
				$row->event_city != "" ? $event_address .= " " . $row->event_city : '';
				$row->event_state != "" ? $event_address .= ", " . $row->event_state : '';
				$row->event_zip != "" ? $event_address .= " " . $row->event_zip : '';
				$event_address != "" ? $event_address = "<span class='icon-location fs1'></span>$event_address" : '';
$row->event_rsvp ? $event_rsvp = "<span class='icon-info fs1'><i><a href='$rsvp_link' target='_blank'>RSVP</a></i></span>" : $event_rsvp = "";

				//$event_address = $row->event_address . " " . $row->event_city . ", " . $row->event_state .  " " . $row->event_zip;
				$calblocktemplate = KYBCAL_ABSPATH . "/view/html/cal_eventblock.html";
				$calblockcontent = array (
					'EVENTDAY' => $eventDay,
					'EVENTTITLE' => $eventTitle,
					'EVENTDAYNAME' => $event_dayname,
					'EVENTTIME' => $event_time,
					'EVENTADDRESS' => $event_address,
                 'EVENTRSVP' => $event_rsvp,
					'EXCERPT' => $excerpt,
				);
				$listings .= $this->showPage ($calblocktemplate, $calblockcontent, true);
				

				$listInfo = "";
				$list_info = "";
				
				if ($bgcolor == "") {
					$bgcolor = "#f2f2f2";
				} else if ($bgcolor == "#f2f2f2") {
					$bgcolor = "";
				}
			}
			
		} else {
			$listings .= "<p>There are currently no events. Please check back with us at a later date.</p>";
		}
		$listings .= "</div>";
		return $listings;
	}

	public function calForm($calID, $action, $area) {
		global $wpdb;
		$adminURL = $this->adminURL;
		$wpadminURL = admin_url();
		$form = array();
		$dates = array();
		$rec_date = array();
		$datefield = array();
		$datePeriod = "";
		$datelist = array();
		$monthoptions = "";
		$daylist = array();
		$yearlist = array();	
		$title = "";
		$process = "addsubmit";
		$month = date("m");
		$dayof = 0;
		$year = date("Y");
		$starthour = 0;
		$startmin = 0;
		$endmin = 0;
		$startAMChecked = "";
		$endAMChecked = "";
		$startPMChecked = "";
		$endPMChecked = "";
		$status = 0;
		$featured = 0;
		$featuredTxt = "";
		$eventAddress = "";  //Get from settings
		$eventCity = ""; //Get from settings
		$eventState = ""; //Get from settings
		$eventZip = ""; //Get from settings
		$event_rsvp = 0;
		$target = 0;
		$themeID = 1;
		$addtoCal = 0;
		$trashnotice = "";
		$targetSelections = array(1 => "Calendar Entry Only", 2 => "Announcements and Calendar Entry", 3 => "Announcements Entry Only");
		$event_rsvp = 0;
		$rsvp_form = 0;
		$order_num = "";

		if (isset($calID) && $calID != 0 && $calID != "") {
			$sql = "Select * from ". $this->table . " where id = %d";
			$result = $wpdb->get_results($wpdb->prepare($sql, $calID));
			if (count($result) != 0) {	
				$process = "updatesubmit";
				$addtoCal = 1;
				foreach ($result as $row) {
					$day = ($row->year) . "-" . ($row->month) . "-" . ($row->day);
					$eventDate = ($row->month) . "/" . ($row->day) . "/" . ($row->year);
					$month = (int)$row->month;
					$dayof = (int)$row->day;
					$year = (int)$row->year;
					$calContent = apply_filters('the_content', $row->description);
					$starthour = $row->start_hour;
					$startmin = $row->start_minute;
					$endhour = $row->end_hour;
					$endmin = $row->end_minute;
					$timezone = $row->event_timezone;
					$starttime = $starthour . ":" . $startmin . ":00" ;
					$endtime = $endhour . ":" . $endmin . ":00" ;
					$startap = $row->start_day;
					$endap = $row->end_day;
					$title = stripslashes($row->title);
					$target = $row->target;
					$featured = $row->featured;
					$featuredTxt = stripslashes($row->featured_txt);
					$featuredImage = $row->featured_image;
					$tooltip = $this->cleanString(stripslashes($row->tooltip));
					$eventAddress = stripslashes($row->event_address);
					$eventCity = stripslashes($row->event_city);
					$eventState = stripslashes($row->event_state);
					$eventZip = $row->event_zip;
					$status = $row->status;
					$recurring = $row->recurring;
					$rec_type = $row->recurring_type;
					$rec_period = $row->recurring_period;
					$recurring_startdate = $row->recurring_startdate;
					$event_rsvp = $row->event_rsvp;
					$url = $this->cleanString($row->cal_link);
					$themeID = $row->calendar_id;
					$event_rsvp = $row->event_rsvp;
					$rsvp_form = $row->rsvp_form;
					$rsvplink = $row->rsvp_link;
					$order_num = $row->order_num;
					

					//========= Format start date for reoccurrances =============
					$rec_date = explode('-', $row->recurring_startdate);
					$rec_startdate = $rec_date[1] . "/". $rec_date[2] . "/" . $rec_date[0];	
			
					//========= Initialize month strings =============
					$monthtext = $this->month_name($month);
				}
			}
		}

		//======== Options ========//
		$monthoptions = $this->buildDateOptions(12, 1, 'month', $month);
		$dayoptions = $this->buildDateOptions(31, 1, '', $dayof);
		$endyy = $year + 5;
		$yearoptions = $this->buildDateOptions($endyy, $year, '', $year);
		$starthouroptions = $this->buildDateOptions(12, 0, '', $starthour);
		$endhouroptions = $this->buildDateOptions(12, 0, '', $endhour);
		$startminoptions = $this->buildDateOptions(55, 0, 'minutes', $startmin);
		$endminoptions = $this->buildDateOptions(55, 0, 'minutes', $endmin);
		$statusoptions = $this->buildStatusOptions($status);
		$rsvpoptions = $this->buildOptions ($this->bitOptions, $event_rsvp, '', '', '', 0);
		$addCalOptions = $this->buildOptions ($this->bitOptions, $addtoCal, '', '', '', 0);		
		$targetoptions = $this->buildOptions ($targetSelections, $target, '', '', '', 0);
		$featuredoptions = $this->buildRadioOptions ($featured, $this->bitOptions, 'featured', true, false);
		$stateOptions = $this->fillStates($eventState);
		$sql = "Select * from " . KYBCAL_TABLE . "kyb_calendar_timezones";
		$tzOptions = $this->build_dboptions($sql, 'tz_name', 'tz_id', $timezone, true);
		$sqlThemes = "Select cal_id, cal_title from " . $this->themetable;
		$themeOptions = $this->build_dboptions($sqlThemes, 'cal_title', 'cal_id', $themeID, true);
		$rsvpOptions = $this->buildStatusOptions($event_rsvp);
		$checkResult = $wpdb->get_results("SHOW TABLES LIKE '" . KYBCAL_TABLE . "forms'");
		$formOptions = "";
		if (count($checkResult) != 0) {
			$sqlForms = "Select form_id, form_title from " . KYBCAL_TABLE . "forms where form_rsvp = 1";
			$formOptions = $this->build_dboptions($sqlForms, 'form_title', 'form_id', $rsvp_form, true);
		}
		$recurStyle = "style='display:block;'";
		//======== End Options ========//
$featuredImage != '' ? $viewimage = $this->_createImage($featuredImage, '0', '', '') : $viewimage = "";
		
		/*if ($order_num != "") {
			//======== Get Order Information ========//
			require_once(WP_PLUGIN_DIR . "/kybstore/lib/shared.php");
			require_once(WP_PLUGIN_DIR . "/kybstore/lib/smarty.php");
			require_once(WP_PLUGIN_DIR . "/kybstore/controller/controller.php");
			require_once(WP_PLUGIN_DIR . "/kybstore/model/store_cart.php");
			$cls = new cartprocesses;
			$sqlProd = "Select * from product_orders where order_num = %d";
			$rsProd = $wpdb->get_results($wpdb->prepare($sqlProd, $order_num));
			if (count($rsProd) != 0) {
				foreach ($rsProd as $p) {
					$product_id = $p->product_id;
					$result = $cls->get_product_info($product_id);
					$product_sizes = explode(",",$p->product_size);
					$reservation_time = $p->reservation_time;
					$product_type_cost = $p->product_cost;
					$additionalOptionInfo = $cls->getAddedOptions($product_sizes, $reservation_time, $product_type_cost, $size_quantity, $product_id);
					$hourOptions = $additionalOptionInfo->hourOptions;
					$boatOptions = $additionalOptionInfo->boatOptions;
					$addlOptions = $additionalOptionInfo->addlOptions;
					$sizeOptions = $additionalOptionInfo->sizeOptions;
					$totalOptions = $additionalOptionInfo->totalOptions;
					$hrs = $additionalOptionInfo->hrs;
					isset($_SESSION['size_quantity'][$product_id]) ? $boatOptions = $cls->getBoatOptions($p->product_id, $reservation_time, $reservation_date, $product_sizes, $product_type_cost, $size_quantity, $order_comments) : $boatOptions = "<p>Select your reservation time and date above to view availability.</p>";
					$boatOptions = $cls->getBoatOptions($row->product_id, $reservation_time, $reservation_date, $product_sizes, $product_type_cost, $size_quantity, $order_comments);
				}
			}
		}*/

		if ($startap == "AM") {
			$startAMChecked = "checked";
		}

		if ($endap == "AM") {
			$endAMChecked = "checked";
		}

		if ($startap == "PM") {
			$startPMChecked = "checked";
		}

		if ($endap == "PM") {
			$endPMChecked = "checked";
		}

		//======== Set recurring options ========//
		$recurringOptions = $this->BeginTable('500', '1', '2', '2');
		if ($recurring == 1) { 
			switch($rec_type) {
				case 'BusDay':
					$occurtype = "business days";
				break;
				case 'Day':
					$occurtype = "daily";
				break;
				case 'Week':
					$occurtype = "weekly";
				break;
				case 'Month':
					$occurtype = "monthly";
				break;
				case 'Year':
					$occurtype = "yearly";
				break;
			}
			//Gather how many times recurring 
			$sqlcount = "Select COUNT(id) as total_recurrance from " . $this->table . " where recurring_startdate = %s";
			$resultcount = $wpdb->get_var($wpdb->prepare($sqlcount, $recurring_startdate));

			$rec_period = $resultcount;
			$rec_num = $rec_period;
			if ($rec_period == 0) { 
				$reccontent = array("<p>To make this event recurring select  how often you want to repeat it and for how long. To add current event to existing events click on the 'add to existing events' OR to replace all dates' events with current one, click on 'overwrite existing events' </p>");
				$recurringOptions .= $this->buildRow($reccontent, '', count($reccontent));
			} else {
				//======== Calculate recurring start date should the start date have been changed ========//
				$recur_start = $wpdb->get_var("Select dt from " . $this->table . " where recurring_startdate = '$recurring_startdate' order by dt limit 1");
				$recur_start = $this->ReformatDatedb($recur_start);
				$reccontent = array("<p class='dbMessage'>This is a recurring event that begins on $recur_start and reoccurs $occurtype for $rec_num $rec_type(s).</p><input type=\"hidden\" name=\"rec_date\" id=\"rec_date\" value=\"$rec_date\"/>");
				$recurringOptions .= $this->buildRow($reccontent, '', count($reccontent));
			}
		} else { 
			$reccontent = array("<p>To make this event recurring select  how often you want to repeat it and for how long. </p>");
			$recurringOptions .= $this->buildRow($reccontent, '', count($reccontent));
		} 
		$recurringOptions .= $this->EndTable();
		//======== End Set Recurring Options ========//

		//======== Start Repeat Options ========//
		$options = array('NA'=>'--','Day'=>'Day', 'Week'=>'Week', 'Month'=>'Month', 'Year'=>'Year');
		$optList = "";
		foreach ($options as $key => $value) { 
			if ($recurring == 1) {
				$optionselected = (($rec_type == $key)?' selected="selected"':'');
				//dissallow view of recurring options
				$recurStyle = "style='display:none;'";
			}
			$optList .= "<option value=\"$key\" $optionselected>$value</option>";
		}

		if ($recurring == 0) {
			$rec_period = "";
		

		$recurtemplate = KYBCAL_ABSPATH . "/view/html/cal_recur.html";
		$recurcontent = array(
			'recurstyle' => $recurStyle,
			'periodoptions' => $optList,
			'reperiod' => $rec_period,
			'customdates' => ''
		);
		$repeatOptions = $this->showPage($recurtemplate, $recurcontent, true);
    $recurclass = 'open';
} else {
    $recurclass = 'closed';
}
		//======== End Repeat Options ========//

		//======== Set Up Description ========//
		if ($area != "post") {
			$settings = array( 'media_buttons' => true);
			$description = $this->showWPeditor3(stripslashes($calContent), "content", 'content', true, 1, false);
			//======== End Set Up Description ========//
		}

		//======== Set up Action Buttons ========//
		$deleteLink = $this->adminURL . "&process=delete&ID=$calID";
		switch ($action) {
			case "addnew":
				$buttons = '<input name="Submit" type="submit" class="button" id="Submit" value="Create" />&nbsp;';
				$buttons .= '<input name="Submit" type="button" class="button" id="Submit" value="Cancel" onclick="window.location=\'' . $adminURL . '&calID={CALID}\'"/>';
			break;
			case "update":
				$buttons = '<input type="hidden" name="id" value="' . $calID . '" />';
				
				if ($recurring == 1) {
					$buttons .= '<b>Select modification type:</b><br><input type="radio" name="form[modThis]" id="mod" value="1" class="form1" />&nbsp;This occurance Only&nbsp;&nbsp;<input type="radio" name="form[modThis]" id="mod" value="2" class="form1" />&nbsp;All occurances from this date forward&nbsp;&nbsp;<input type="radio" name="form[modThis]" id="mod" value="3" class="form1">&nbsp;All occurances<br/>';
					$buttons .= '<input name="Submit" type="submit" class="button" id="Submit" value="Modify" />';
					$buttons .= '&nbsp;<input name="Submit" type="button" class="button" id="Submit" value="Delete" onclick="calendar.view.checkOK(\'' . $deleteLink . '\')" />';
					$buttons .= '&nbsp;<input name="Submit" type="button" class="button" id="Submit" value="Cancel" onclick="window.location=\'' . $adminURL . '&calID=' . $themeID . '\'"/>';
				} else {
					$buttons .= '<input name="Submit" type="submit" class="button" id="Submit" value="Modify" />';
					$buttons .= '&nbsp;<input name="Submit" type="button" class="button" id="Submit" value="Delete" onclick="calendar.view.checkDelete(\'' . $deleteLink . '\')" />';
					$buttons .= '&nbsp;<input name="Submit" type="button" class="button" id="Submit" value="Cancel" onclick="window.location=\'' . $adminURL . '&calID=' . $themeID . '\'"/>';
				}				
			break;
			default:
				$buttons = '<input name="Submit" type="button" class="button" id="Submit" value="Delete" onclick="calendar.view.checkOK(\'CalForm\', \'delete\', \'Delete\')"/>';
				$buttons .= '<input type="hidden" name="Submit" id="delete" value="">';
				if ($recurring == 1) {
					$buttons .= '<input type="radio" name="form[delThis]" id="form[delThis]" value="1" class="form1" />This occurance Only&nbsp;&nbsp;<input type="radio" name="form[delThis]" id="form[delThis]" value="2" class="form1" />All occurances from this date forward&nbsp;&nbsp;<input type="radio" name="form[delThis]" id="form[delThis]" value="3" class="form1" />All occurances<br/>';
				}
				$buttons .= '<input name="Submit" type="button" class="button" id="Submit" value="Cancel" onclick="window.location=\'' . $adminURL . '&calID=' . $themeID. '\'"/><input type="hidden" name="form[rec_startdate]" id="form[rec_startdate]" value="<? echo $recurring_startdate; ?>" />';
			break;        
	   } // end if action is modify 
	   //======== End Set Up Action Buttons ========//

		
		//======== Return Data Information ========//
		$area == 'post' ? $template = "calendar_postform.html" : $template = "calendar_form.html";
		$content = array (
			'ADMINURL' => $this->adminURL,
			'ID' => "$calID",
			'PROCESS' => $process,
			'TITLE' => $title,
			'EVENTDATE' => $eventDate,
			'MONTHOPTIONS' => $monthoptions,
			'DAYOPTIONS' => $dayoptions,
			'YEAROPTIONS' => $yearoptions,
			'REPEATOPTIONS' => "",
			'CALID' => "$themeID",
			'STARTHOUROPTIONS' => $starthouroptions,
			'ENDHOUROPTIONS' => $endhouroptions,
			'STARTMINUTEOPTIONS' => $startminoptions,
			'ENDMINUTEOPTIONS' => $endminoptions,
			'STARTAMCHECKED' => $startAMChecked,
			'ENDAMCHECKED' => $endAMChecked,
			'STARTPMCHECKED' => $startPMChecked,
			'ENDPMCHECKED' => $endPMChecked,
			'RECURRINGOPTIONS' => $recurringOptions,
          'RECURCLASS' => $recurclass,
			'REPEATOPTIONS' => $repeatOptions,
			'TARGETOPTIONS' => $targetoptions,
			'FEATUREDOPTIONS' => $featuredoptions,
			'LOGOIMAGE' => $featuredImage,
          'VIEWIMAGE' => $viewimage,
			'ADDTOCALOPTIONS' => $addCalOptions,
			'FEATUREDRTEXT' => $featuredTxt,
			'ADDRESS' => $eventAddress,
			'CITY' => $eventCity, 
			'STATEOPTIONS' => $stateOptions,
			'ZIP' => $eventZip,
			'STATUSOPTIONS' => $statusoptions,
			'TRASHNOTICE' => $trashnotice,
			'RSVPOPTIONS' => $rsvpoptions,
			'RSVPFORMS' => $formOptions,
			'RSVPLINK' => $rsvplink,
			'EVENTDESCRIPTION' => $description,
			'ACTIONBUTTONS' => $buttons,
			'TZOPTIONS' => $tzOptions,
			'CALENDARTHEMES' => $themeOptions,
			'HTML' => $template
		);
		return $content;
	}

	public function monthoptions($r, $m, $yn) {
		$selmonth = $r ;
		$locprm = $m - 1;
		$selyear = $yn;
		$options = "";

		for ($s=0; $s<=12; $s++) { 
			$monthName = $this->month_name($selmonth);
			$options .= "<option value=\"$locprm-$selyear\">$monthName $selyear</option>";
			$selmonth++;
			$locprm = $locprm + 1;
			if ($selmonth > 12) {
				$selyear = $selyear + 1;
				$selmonth = 1;
			}
		}
		return $options;
	}	

	public function calDays($no_of_days, $r, $yn, $j, $calID) {
		global $wpdb;
		$adj = "";
		$calInfo = "";
		$adminURL = KYBCAL_ADMINURL;
		$calURL = KYBCAL_URLPATH;
		$currDay = date("d");
		for ($k = 1; $k <= $j; $k++) { // Adjustment of date starting
			 $adj .= "<td class='emptywkday'>&nbsp;</td>";
		}
		$calID == "" ? $calID = 1 : '';
		//======== Starting of the days ========
		for($i=1;$i<=$no_of_days;$i++){		
			$sql = "SELECT DISTINCT day, month, year, calendar_id from " . $this->table . " where month = %d and day = %d and year = %d and calendar_id = %d";
			$result = $wpdb->get_results($wpdb->prepare($sql, $r, $i, $yn, $calID));
			// Check the result of the query 
			if (count($result) != 0) { 
				foreach ($result as $row) {
					$selectday = $row->day;
					strlen($row->day) < 2 ? $chkDay = "0" . $row->day : $chkDay = $row->day;
					$id = $row->calendar_id;
					$m = $row->month;
					$d = $row->day;
					if (strlen($row->month) == 1) {
						$m = "0" . $row->month;
					}

					if (strlen($row->day) == 1) {
						$d = "0" . $row->day;
					}
					$formaldate = addslashes($row->year) . "-" . addslashes($m) . "-" . addslashes($d);

					//======== Get information on that day if any ========
					//$sql2 = "SELECT * from " . $this->table . " where month = $r and day = $i and year = $yn  and calender_id = $calID order by start_hour";
					$sql2 = "SELECT *, MAKETIME(start_hour  + IF(start_hour<12, 12, 0), start_minute, 0) AS start_order from " . $this->table . " where month = %d and day = %d and year = %d and calendar_id = %d order by start_day,start_order";
					$result2 = $wpdb->get_results($wpdb->prepare($sql2, $r, $i, $yn, $calID));
					foreach ($result2 as $info) {
						$infoID = $info->id;
						$lookup = $info->dt;
						$wp_id = $info->wp_postID;
						/*$order_number = $info->order_num;
						$orderInfo = $wpdb->get_row($wpdb->prepare("Select * from orders where order_id = %d", $order_number));
						$orderPaid = $orderInfo->order_paid;
						$firstname = $orderInfo->billing_firstname;
						$lastname = $orderInfo->billing_lastname;*/
						strlen($info->start_minute) == 1 ? $startMinute = "0" . $info->start_minute : $startMinute = $info->start_minute;
						$event_time = "<i>" . $info->start_hour . ":" . $startMinute . " " . $info->start_day . "</i>";
						$status = $info->status;
						$calID == 2 && $orderPaid ? $status = 1 : '';
						$calID == 2 ? $wpdb->query($wpdb->prepare("Update " . $this->table . " set status = %d where id = %d", $status, $infoID)) : '';
						
						$status == 0 ? $post_status = "(inactive)" : '';

						if ($currDay == $chkDay) {
							$color = "#ffffff;";
							$status == 1 ? $inactive = "" : $inactive = "$post_status";
						} else if ($status) {
							$color = "#0077cc;";
							$inactive = "";
						} else {
							$color = "#aaaaaa;";
							$inactive = "$post_status";
						}

						$event_year = $info->year;
						$event_day = $info->day;
						strlen($info->start_minute) == 1 ? $startMinute = "0" . $info->start_minute : $startMinute = $info->start_minute;
						strlen($info->end_minute) == 1 ? $endMinute = "0" . $info->end_minute : $endMinute = $info->end_minute;
						$event_time = "<i>" . $info->start_hour . ":" . $startMinute . " " . $info->start_day . "</i>";
						$event_time .= " - <i>" . $info->end_hour . ":" . $endMinute . " " . $info->end_day . "</i>";
						

						//$information .= "<div style=\"padding-bottom: 5px;float:left;width:80%;\"><a class=\"adminLink\" style=\"color:$color;font-size:12px;\" href=\"$adminURL&process=update&ID=$infoID&lookup=$lookup\">" . $info->title . " $inactive</a></div><div style=\"float:left;width:10%;\"><input type=\"image\" src=\"$calURL/images/icon_outlook.png\" alt=\"Add Event To Outlook\" title=\"Add Event To Outlook\" style=\"border:0px;\" onclick=\"javascript:window.location='$calURL/model/outlook.php?eventID=$infoID&wpID=$wp_id'\"></div><div style=\"clear:both;\"></div>"; 

						$information .= "<div style=\"padding-bottom: 5px;float:left;width:80%;\"><a class=\"adminLink\" style=\"color:$color;font-size:12px;\" href=\"$adminURL&process=update&ID=$infoID&lookup=$lookup\">" . stripslashes($info->title) . " $inactive</a></div><div style=\"float:left;width:10%;\"></div><div style=\"clear:both;\"></div>"; 
					}

		
					if ($currDay == $chkDay) {
						$calInfo .= $adj."<td valign='top' class=\"currday\"><a href='$adminURL&process=viewday&lookup=$formaldate' style='color:#ffffff;font-weight:bold;'>$i</a>";
						if ($information != "") {
							$calInfo .= "<div class=\"caltext\" style=\"color:#ffffff;\">$information</div>";
						}
					} else if ($i == $selectday) {
						$calInfo .= $adj."<td valign='top' class=\"selwkday\"><a href='$adminURL&process=viewday&lookup=$formaldate' style='color:#b04040;'>$i</a><div class=\"caltext\">$information</div>";
					} else {
						//======== Display the date inside the calendar cell ========
						$calInfo .= $adj."<td valign='top' class=\"wkday\" ><span class=\"wkdaynum\">$i</span><br>"; 
					}

					$information = "";
				}
				
			} else {
				//======== Display the date inside the calendar cell ========
				strlen($i < 2) ? $calDay = "0" . $i : $calDay = $i; 
				if ($currDay == $calDay) {
					$calInfo .= $adj."<td valign=\"top\" class=\"currday\" ><span class=\"wkdaynum\">$i</span><br>"; 
				} else {				
					$calInfo .= $adj."<td valign=\"top\" class=\"wkday\" ><span class=\"wkdaynum\">$i</span><br>"; 
				}
			}

			$calInfo .= " </td>";
			$adj='';
			$j ++;
			if($j==7){
				$calInfo .= "</tr><tr>";
				$j=0;
			}			
		}
		return $calInfo;
	}

	public function calSidebarDays($no_of_days, $r, $yn, $j) {
		global $wpdb;
		$adj = "";
		$adminURL = KYBCAL_ADMINURL;
		$currDay = date("d");
		for ($k = 1; $k <= $j; $k++) { // Adjustment of date starting
			 $adj .= "<td>&nbsp;</td>";
		}
		//======== Starting of the days ========
		for($i=1;$i<=$no_of_days;$i++){		
			$sql = "SELECT DISTINCT day, month, year from " . $this->table . " where month = $r and day = $i and year = $yn";
			$result = $wpdb->get_results($sql);
			// Check the result of the query 
			if (count($result) != 0) { 
				foreach ($result as $row) {
					$selectday = $row->day;
					strlen($row->day) < 2 ? $chkDay = "0" . $row->day : $chkDay = $row->day;
					$id = $row->id;
					$m = $row->month;
					$d = $row->day;
					if (strlen($row->month) == 1) {
						$m = "0" . $row->month;
					}

					if (strlen($row->day) == 1) {
						$d = "0" . $row->day;
					}
					$formaldate = addslashes($row->year) . "-" . addslashes($m) . "-" . addslashes($d);

					//======== Get information on that day if any ========
					$sql2 = "SELECT * from " . $this->table . " where month = $r and day = $i and year = $yn order by start_hour";
					$result2 = $wpdb->get_results($sql2);
					foreach ($result2 as $info) {
						$infoID = $info->id;
						$themeID = $info->calendar_id;
						$lookup = $info->dt;
						strlen($info->start_minute) == 1 ? $startMinute = "0" . $info->start_minute : $startMinute = $info->start_minute;
						$event_time = "<i>" . $info->start_hour . ":" . $startMinute . " " . $info->start_day . "</i>";
						$status = $info->status;
					}

					if (isset($_REQUEST['view'])) {
						$viewLink = "$templatePath/model/viewday.php?prm=$m&requestdate=$lookup";
					} else {
						$viewLink = "$templatePath/model/viewday.php?prm=$m&requestdate=$lookup";
					}

					if ($i == $selectday) {
						$themeWP = $wpdb->get_var($wpdb->prepare("Select cal_wp_pageID from " . $this->themetable . " where cal_id = %d", $themeID));
						$calPage = get_permalink($themeWP); 
						$calInfo .= $adj."<td valign=top><div id='daybgselected'><div class='calendartext miniselwkday'><b><a class=\"calendartext\" href=\"$calPage/?process=viewdaylist&lookup=$lookup\">$selectday<b /></div></div>";
					} else {
						$calInfo .= $adj."<td valign=top><div id='daybg'><div class='calendartext'>$i</div></div>"; // This will display the date inside the calendar cell
					}
				}
			} else {
				//======== Display the date inside the calendar cell ========
				$calInfo .= $adj."<td valign=top><div id='daybg'><div class='calendartext'>$i</div></div>"; // This will display the date inside the calendar cell
			}

			$calInfo .= " </font></td>";
			$adj='';
			$j ++;
			if($j==7){
				$calInfo .= "</tr><tr>";
				$j=0;
			}			
		}
		return $calInfo;
	}
	
	public function calViewDays($no_of_days, $r, $yn, $j, $wp_pageID, $calID, $showadj, $index) {		
		global $wpdb;
		$adj = "";
		$calInfo = "";
		$eventListings = "";
		$currDay = date('Y-m-d');
		!$showadj ? $startnum = (int)$j : $startnum = 1;
		$showInfo = true;
		$showadjnum = false;
		$adminURL = $this->adminURL;
		$ajaxURL = KYBCAL_AJAXPATH;
		$url = get_permalink($wp_pageID);
		$themeURL = $this->templateURL;
		
		$show_list = $wpdb->get_var($wpdb->prepare("Select show_list from " . $this->themetable . " where cal_id = %d", $calID));
		
		if (!$showadj) {
			if (bcsub($startnum,6) <= 0) {
				$startdiff = bcsub($startnum,6);
				$showadj = true;
				$showadjnum = true;
				$j = bcsub($no_of_days,$startnum);
				$startnum = 1;				
			} else {
				$startnum = bcsub($startnum, 6);
			}

			$daysdiff = bcsub($no_of_days, $startnum);
			$daystoadd = bcsub($no_of_days, $daysdiff);
			$no_of_days = bcadd($no_of_days, $daystoadd-1);
			$showadj ? $no_of_days = bcsub($no_of_days,$j) : '';
		}
		if ($showadj) {
			for ($k = 1; $k <= $j; $k++) { // Adjustment of date starting
				$adj .= "<td width='10%' class='emptywkday'>&nbsp;</td>";
			}
		}
		
		//======== Starting of the days ========
		for($i=$startnum;$i<=$no_of_days;$i++){		
			$sql = "SELECT DISTINCT day, month, year from " . $this->table . " where month = %d and day = %d and year = %d and status = %d and calendar_id = %d";
			$result = $wpdb->get_results($wpdb->prepare($sql, $r, $i, $yn, 1, $calID));
			// Check the result of the query 
			$calDay = $yn . "-" . $r . "-" . $i;
			
			if (count($result) != 0) { 
				foreach ($result as $row) {
					$selectday = $row->day;
					$id = $row->calendar_id;
					$m = $row->month;
					$d = $row->day;
					
					$eventListings = "";
					if (strlen($row->month) == 1) {
						$m = "0" . $row->month;
					}
					if (strlen($row->day) == 1) {
						$d = "0" . $row->day;
					}
					$formaldate = addslashes($row->year) . "-" . addslashes($m) . "-" . addslashes($d);
					//======== Get information on that day if any ========
					$sql2 = "SELECT *, MAKETIME(start_hour  + IF(start_hour<12, 12, 0), start_minute, 0) AS start_order from " . $this->table . " where month = %d and day = %d and year = %d and status = %d and calendar_id = %d order by start_day,start_order";
					$result2 = $wpdb->get_results($wpdb->prepare($sql2, $r, $i, $yn, 1, $calID));
					$cnt = 1;
					foreach ($result2 as $info) {
						$infoID = $info->id;
						
						if ($show_list) {
							//$showInfo = false;
						}
						

						if ($showInfo) {
							$m = $info->month;
							$event_year = $info->year;
							$event_day = $info->day;
							$event_title = $info->title;
							$show_title = substr($event_title, 0, 100) . "...";
							$themeWP = $wpdb->get_var($wpdb->prepare("Select cal_wp_pageID from " . $this->themetable . " where cal_id = %d", $calID));
							$calPage = get_permalink($themeWP);
							$editLink = KYBCAL_ADMINURL . "&process=update&ID=$infoID";
							$index ? $calWPLink = "javascript:calendar.view.gotoLink('$editLink');" : $calWPLink = get_permalink($info->wp_postID);
							
							strlen($info->start_minute) == 1 ? $startMinute = "0" . $info->start_minute : $startMinute = $info->start_minute;
							strlen($info->end_minute) == 1 ? $endMinute = "0" . $info->end_minute : $endMinute = $info->end_minute;
							$event_time = "<i>" . $info->start_hour . ":" . $startMinute . " " . $info->start_day . "</i>";
							$event_time .= " - <i>" . $info->end_hour . ":" . $endMinute . " " . $info->end_day . "</i>";
							//$calinfo = $this->_hyperlink ("{$calPage}?viewevent={$infoID}", $event_time . "<br>" . $info->title, '', 'wkdaylink', '', '') ;
							
							//if ($show_list) {
								$calinfo = $this->_hyperlink ("$calWPLink", "<div class='eventnum'>$show_title</div>", '', 'wkdaylink', '', '') ;
								$information .= $this->_addDiv($calinfo, '', "padding-bottom: 10px;", '', '');
							/* {else {
								$information = "&nbsp;";
							}*/
							//$eventListings .= "<div class='caltext' style='font-size:11px;font-weight:bold;'>$event_time</div><a href=\"$calWPLink\" title=\"\" rel=\"author\" class=\"authlink\" style=\"font-size: 11px; float:none;line-height:12px;\">$event_title</a><br><br>";
							$eventListings .= '<p>' . $event_time . '<br><a href=&quot;' . $calWPLink . '&quot;>' . $event_title. '</a></p><hr/>';
							$cnt++;
							/*if ($cnt >= 3) {
								$information .= "<div align='center'><img src='$themeURL/images/arrowdown.png' onclick=\"javascript:window.location='$calPage?prm=$m&chm=1&y=$event_year&view=Day&dy=$event_day'\" border='0' class='cursors' alt='View More' title='view more'></div>";
								break;
							}*/
						}
						

						//Set back to true in case false was set above.
						$showInfo = true;
						
					}
					
					
					if ($i == $selectday && $showadj) {		
						//echo __LINE__ . "<br>";
						$dayLink = "<a href='$calPage/?process=viewdaylist&lookup=$formaldate' class='selwkdaylink' style='font-weight:normal;text-decoration:none;'>$i</a>";
						$gridtemplate = KYBCAL_ABSPATH . "/view/html/cal_gridbox.html";
						$gridcontent = array(
							'THEMEURL' => $this->templateURL,
							'DAYLINK' => $dayLink,
							'VIEWDAYLINK' => "$calPage/?prm=$m&chm=0&y=$event_year&view=Day&dy=$event_day",
							'INFORMATION' => $information,
							'EVENTLISTINGS' => $eventListings
						);
						$calbubble = $this->showPage($gridtemplate, $gridcontent, true);
						//$eventListings = "";

						$themeWP = $wpdb->get_var($wpdb->prepare("Select cal_wp_pageID from " . $this->themetable . " where cal_id = %d", $themeID));
						$calPage = get_permalink($themeWP);
						if (!$show_list) {
							$calInfo .= $adj."<td width='10%' valign=top class=\"selwkday\"><span class=\"selwkdaynum\">$i</span></div><div class=\"caltext\">$information</div>";
						} else {
							//$calInfo .= $adj."<td width='10%' valign=top class=\"selwkday\" onclick=\"$.facebox('$eventListings')\"><div style=\"font-size:0.750em;\">$i</div><div class=\"caltext\">$information</div>";
							$calInfo .= $adj."<td width='10%' valign=top class=\"selwkday\"><span class=\"selwkdaynum\">$i</span></div><div class=\"caltext\">$information</div>";
						}
					} else if ($startnum == $selectday && !$showadj) {
						//echo __LINE__ . "<br>";
						/*$themeWP = $wpdb->get_var($wpdb->prepare("Select cal_wp_pageID from kyb_calendar_themes where cal_id = %d", $themeID));
						$calPage = get_permalink($themeWP);
						$calInfo .= $adj."<td width='10%' valign=top class=\"selwkday\"><a href='$calPage/?process=viewdaylist&lookup=$formaldate' class='selwkdaylink'>$startnum</a><div class=\"caltext\">$information</div>";*/

						$dayLink = "<a href='$calPage/?process=viewdaylist&lookup=$formaldate' class='selwkdaylink' style='font-weight:normal;text-decoration:none;'>$startnum</a>";
						$gridtemplate = KYBCAL_ABSPATH . "/view/html/cal_gridbox.html";
						$gridcontent = array(
							'THEMEURL' => $this->templateURL,
							'DAYLINK' => $dayLink,
							'VIEWDAYLINK' => "$calPage/?prm=$m&chm=0&y=$event_year&view=Day&dy=$event_day",
							'INFORMATION' => $information,
							'EVENTLISTINGS' => $eventListings
						);
						$calbubble = $this->showPage($gridtemplate, $gridcontent, true);
						$eventListings = "";

						$themeWP = $wpdb->get_var($wpdb->prepare("Select cal_wp_pageID from " . $this->themetable . " where cal_id = %d", $themeID));
						$calPage = get_permalink($themeWP);
						//$calInfo .= $adj."<td valign=top class=\"selwkday\">$calbubble";
						$calInfo .= $adj."<td valign=top class=\"selwkday\"><span class=\"selwkdaynum\">$startnum</span><div class=\"caltext\">$information</div>";
					} else {
						//echo __LINE__ . "<br>";
						//======== Display the date inside the calendar cell ========						
						if (!$showadj) {							
							strtotime($currDay) == strtotime($calDay) ? $calClass = "currday" : $calClass = "wkday"; 
							$calInfo .= $adj."<td valign='top' class=\"$calClass\"><span class=\"wkdaynum\">$startnum</span><br>"; 
						} else {
							//strlen($i < 2) ? $calDay = "0" . $i : $calDay = $i; 
							strtotime($currDay) == strtotime($calDay) ? $calClass = "currday" : $calClass = "wkday"; 
							$calInfo .= $adj."<td valign='top' class=\"$calClass\"><span class=\"wkdaynum\">$i</span><br>"; 
						}
					}
					$information = "";
				}
			} else {
				//======== Display the date inside the calendar cell ========
				if (!$showadj) {
					strtotime($currDay) == strtotime($calDay) ? $calClass = "currday" : $calClass = "wkday"; 
					$calInfo .= $adj."<td valign='top' class=\"$calClass\"><span class=\"wkdaynum\">$startnum</span><br>"; 
				} else {
					strtotime($currDay) == strtotime($calDay) ? $calClass = "currday" : $calClass = "wkday"; 
					$calInfo .= $adj."<td valign='top' class=\"$calClass\"><span class=\"wkdaynum\">$i</span><br>"; 
				}
			}

			$calInfo .= " </font></td>";
			$adj='';
			$j ++;
			$startnum++;
			if($j==7 ){
				$calInfo .= "</tr><tr>";
				$j=0;
			}			
		}
		return $calInfo;
	}

	public function viewSidebarCal($showcal, $calcls) {
		$prm = $this->checkRequest('prm'); 
		$chm = $this->checkRequest('chm');
		$pluginURL = KYBCAL_URLPATH;
		$ajaxURL = KYBCAL_AJAXPATH;
		if(isset($prm) && $prm > 0){
			$m = bcadd($prm,$chm);
			$r=(int)date('m',mktime(0,0,0,$m,1,$y));
		} else {
			$m= date("m");
			$r=date("m");
		}

		$this->checkRequest('y') != "" ? $y = $this->checkRequest('y') : $y= date("Y");     // Finds today's or selected year
		$no_of_days = date('t',mktime(0,0,0,$m,1,$y)); // This is to calculate number of days in a month
		$mn=date('M',mktime(0,0,0,$m,1,$y)); // Month is calculated to display at the top of the calendar
		$yn=date('Y',mktime(0,0,0,$m,1,$y)); // Year is calculated to display at the top of the calendar
		$j= date('w',mktime(0,0,0,$m,1,$y)); // This will calculate the week day of the first day of the month
		$showcal ? $calDisplay = "display:block;" : $calDisplay = "display: none;";
		
		$calDays .= "<div id=\"kybcalload\" ></div>\n";
		$calDays .= "<div id=\"kybcalelem\" class=\"$calcls\"  style=\"min-width:206px;min-height:209px;$calDisplay\">\n";
		$calDays .= "<link href=\"$pluginURL/css/calendar_sb_styles.css\" rel=\"stylesheet\" type=\"text/css\">";
		$calDays .= "<style>#daybg {background:url($pluginURL/images/daybg.gif) top left no-repeat;}#daybgselected {	background:url($pluginURL/images/dayselectedbg.gif) top left no-repeat;}</style>";
		$calDays .= "<script src=\"$pluginURL/js/calendar_js.js\" type=\"text/javascript\"></script>";
		$calDays .= $this->BeginTable('100%', "0", "0", "0", "", "", 'padding:0;text-align:center;width:95%;min-height:209px;background-color:#FFFFFF;');
		$parseURL = parse_url(get_bloginfo('template_url'));
		$templatePath = $parseURL['path'];
		$prevLink = "$ajaxURL/model/calendar_ajax.php?process=sidecal&prm=$m&chm=-1";
		$nextLink = "$ajaxURL/model/calendar_ajax.php?process=sidecal&prm=$m&chm=1";
		
				
		$calDays .= "<tr><td class='arrowbg' style='text-align:center;width:10%;height:24px;'><a class='arrownav' href=\"javascript:void(0);\" onclick=\"loadkybCal('$prevLink','kybcalload','kybcalelem','$pluginURL')\">&laquo;</a> </td><td class='calmonthsel' colspan=5 style='text-align:center;height: 24px;' ><span>$mn $yn </span> </td><td class='arrowbg' style='text-align: center; width:30px;height:24px;'><a class='arrownav'  href=\"javascript:void(0);\" onclick=\"loadkybCal('$nextLink','kybcalload','kybcalelem','$pluginURL');\">&raquo;</a> </td></tr><tr>";
		$calDays .= "<td class='days'>Su</td><td class='days'>Mo</td><td class='days'>Tu</td><td class='days'>We</td><td class='days'>Th</td><td class='days'>Fr</td><td class='days'>Sa</td></tr><tr>";
		$calDays .= $this->calSidebarDays($no_of_days, $r, $yn, $j, $m);
		$calDays .= $this->EndTable();
		$calDays .= "</div>";

		return $calDays;
	}
		
	public function calScript($no_of_days, $r, $yn) {
		global $wpdb;
		$eventlist = array();
		$info = array();
		$selectday = "";
		$adminURL = $this->adminURL;
		$script = "";
		for($g=1;$g<=$no_of_days;$g++){
			$sql = "SELECT * from " . $this->table . " where month = $r and day = $g and year = $yn and status = 1 order by start_time";
			$result = $wpdb->get_results($sql);
			$LI = array();
			foreach ($result as $row) {
				$selectday = $row->day;
				$formaldate = addslashes($row->year) . "-" . addslashes($row->month) . "-" . addslashes($row->day);
				//======== Get information on that day if any ========				
				$id = $row->id;				
				$starthour = $row->start_hour;
				$startpart = $row->start_day;
				$title = $row->title; 
				$info[$g] .= "<a href=\"$adminURL&process=update&ID=$id&lookup=$formaldate\">$starthour$startpart&raquo;</a> - $title<br>";	
			}
			if ($g != $no_of_days) {
				if ($g == $selectday ) {
					$eventlist[$g] = "L$g: '$info[$g]', ";
				} else {
					$eventlist[$g] = "L$g: '', ";
				}
			} else {
				if ($g == $selectday ) {
					$eventlist[$g] = "L$g: '$info[$g]'";
				} else {
					$eventlist[$g] = "L$g: '' ";
				}
			}
			//======== reset info array ======//
			$info = array();		
		}

		$script .= "\n<script type='text/javascript'>\n";
		$script .= "dw_Tooltip.content_vars = {\n";
		for($v=1;$v<=$no_of_days;$v++){
			$script .= $eventlist[$v] . "\n";
		}
		$script .= "}\n";
		$script .= "</script>";

		return $script;
	}

	public function viewcalList($datelookup) {
		global $wpdb;
		$m= date("m");
		$d= date("d");     // Finds today's date
		$y= date("Y");     // Finds today's year

		$no_of_days = date('t',mktime(0,0,0,$m,1,$y)); // This is to calculate number of days in a month

		$mn=date('M',mktime(0,0,0,$m,1,$y)); // Month is calculated to display at the top of the calendar

		$yn=date('Y',mktime(0,0,0,$m,1,$y)); // Year is calculated to display at the top of the calendar

		$j= date('w',mktime(0,0,0,$m,1,$y)); // This will calculate the week day of the first day of the month

		$datefield = array();
		$datefield = explode('-', $datelookup);
		$form['date'] = array ('yy' => $datefield[0], 'mm' => $datefield[1], 'dd' => $datefield[2]);

		if ($form['date']['mm'] == "01") {
			$monthtext = "January";
		} elseif ($form['date']['mm'] == "02") {
			$monthtext = "February";
		} elseif ($form['date']['mm'] == "03") {
			$monthtext = "March";
		} elseif ($form['date']['mm'] == "04") {
			$monthtext = "April";
		} elseif ($form['date']['mm'] == "05") {
			$monthtext = "May";
		} elseif ($form['date']['mm'] == "06") {
			$monthtext = "June"; 
		} elseif ($form['date']['mm'] == "07") {
			$monthtext = "July";
		} elseif ($form['date']['mm'] == "08") {
			$monthtext = "August"; 
		} elseif ($form['date']['mm'] == "09") {
			$monthtext = "September";
		} elseif ($form['date']['mm'] == "10") {
			$monthtext = "October";
		} elseif ($form['date']['mm'] == "11") {
			$monthtext = "November";
		} elseif ($form['date']['mm'] == "12") {
			$monthtext = "December";
		}

		$listings = $this->BeginTable('98%','0');		
		$thInfo = array(
				'ID','Date','Title', 'Recurring Type','Status', 'Edit'
		);	
		$thheight = "35";
		$thwidth = array("50","100","400","100","75","75","75");
		$listings .= $this->buildHeaders($thInfo, $thheight, $thwidth, count($thInfo));
	
		$displaydate = $monthtext . " " . $form['date']['dd'] . ", " . $form['date']['yy'];
		$calendar_list = "";
		$sql = "Select * from " . $this->table . " where dt = '$datelookup' order by start_time";
		$result = $wpdb->get_results($sql);
		if (count($result) != 0) {
			$calendar_list .= "<table cellpadding=\"0\" cellspacing=\"0\" width=\"500\">";
			foreach ($result as $row) {
				$DT = $row->dt;
				$startMinute = $row->start_minute;
				$endMinute = $row->end_minute;
				$startHour = $row->start_hour;
				$startDay = $row->start_day;
				$endHour = $row->end_hour;
				$endDay = $row->end_day;
				$description = $row->description;
				$status = $row->status;
				$ID = $row->id;

			 	$datefield = array();
				$datefield = explode('-', $DT);
				$form['date'] = array ('yy' => $datefield[0], 'mm' => $datefield[1], 'dd' => $datefield[2]);

				if (strlen($startMinute) < 2) {
					$start_minute = "0" . $startMinute;
				} else {
					$start_minute = $startMinute;
				}

				if (strlen($endMinute) < 2) {
					$end_minute = "0" . $endMinute;
				} else {
					$end_minute = $endMinute;
				}

				!$status ? $activation = "Deactivated" : $activation = "Activated";

				$start_time = $startHour . ":" . $start_minute . " " . $startDay;
				$end_time = $endHour . ":" . $end_minute . " " . $endDay;

				$monthtext = $this->month_name($form['date']['mm']);
				$editLink = $this->adminURL . "&process=update&ID=$ID";
				$calcontent = array (
					$this->_addDiv("$start_time - $end_time <i>($activation)</i>", '', "margin-left:20px;font-size:12px;", "", ""), $this->_hyperlink($editLink, 'edit', '', '', '', '')
				);
				$calendar_list .= $this->buildRow2($calcontent, '', '', count($calcontent), '', '', '', 'text', '', '', '');

				$calcontent = array (
					$this->_addDiv($description, '', "margin-left:20px;font-size:12px;", "", "")
				);
				$calendar_list .= $this->buildRow2($calcontent, '', '', count($calcontent), '', '', '2', 'text', '', '', '');

				$calcontent = array (
					'<hr style="color:#29245E;">'
				);
				$calendar_list .= $this->buildRow2($calcontent, '', '', count($calcontent), '', '', '2', 'text', '', '', '');
			} 
			$calendar_list .= "</table>";
		} else { 
			$calendar_list .= $this->BeginTable('', '0', '0', '0', '','center');
			$calcontent = array (
				$this->_addDiv('No plans have been set for this date.', '', "text-align:center;", "", "")
			);
			$calendar_list .= $this->buildRow2($calcontent, '', '', count($calcontent), '', '', '', 'text', '', '', '');
			$calendar_list .= $this->EndTable();
		}		

		$template = "calendar_list.html";
		$content = array (
			'DISPLAYDATE' => $displaydate,
			'CALENDARLISTING' => $calendar_list,
			'HTML' => $template
		);
		return $content;
	}

	public function viewannouncements($datelookup) {
		$wpdb = $this->wpdb;
		$adminUrl = $this->adminURL;
		$shareImgFld = $this->imageFolder;
		//======== Get List ========//
		$limit = 20;
		$filterType = $this->checkRequest('filtertype');
		$pagenum = $this->checkRequest('pg');
		if (empty($pagenum) || $pagenum < 1 || !is_numeric($pagenum)) { 
			$pagenum = 1; 
		} 

		// begin variable check
		if ($this->checkRequest('ANNID') == "") { // begin a
			$ANNID = 0;
		} else {
			$ANNID = $this->checkRequest('lastview');
		} // end a

		if ($this->checkRequest('pageview') == 1) { // begin b
			$cnt = 1;
		} else {
			$cnt = $this->checkRequest('lastview') + 1;
		} // end b

		if (strpos($datelookup, "/")) {
			$datelookup = $this->ReformatDateform($datelookup);
		}

		//======== Determine Filter Type ========//
		$criteria = "";
		$search=false;
		$keyword = "";
		$orderby = "signature_date";
		$targetarea = "";
		$desc = true;
		switch ($filterType) {
			case "order_id":
				$criteria = "order_id";
				$search=true;
				$orderby = "order_id";
			break;
			case "order_date":
				$criteria = "signature_date";
				$search=true;
				$orderby = "signature_date";
			break;
			case "customername":
				$criteria = array("shipping_firstname", "shipping_lastname", "billing_firstname", "billing_lastname");
				$search=true;
				$orderby = "billing_lastname";
				$desc = false;
			break;
			case "search":
				$criteria = array("description", "title");
				$search=true;
				$keyword = $this->checkRequest('keywords');
				$orderby = "start_time";
			break;
		}
		//======== End Determine Filter Type ========//
		if ($datelookup == "" ) {
			$id = 2;			
			$info = $this->get_offset($page, '', $limit, $id, $search, $this->table, '*', '', $criteria, $keyword, $orderby, true, $desc);
			$list_type = "Events Listed by keyword(s) $keyword:";
		} else {
			$orderby = "start_time";
			$list_type = "Events Listed for $datelookup";
			$info = $this->get_offset($page, '', $limit, "'$datelookup'", $search, $this->table, '*', 'dt', $criteria, $keyword, $orderby, true, $desc);
		}

		$offset = $info[0];
		$pages = $info[1];
		$TotalRowCount = $info[2];
		$records = $info[3];

		$start = $offset + 1;
		$end = ($start + $limit) - 1;

		if ($end > $TotalRowCount) {
			$end = $TotalRowCount;
		}
		$pageturns = $this->showNextPrev ($offset, $pagenum, $pages, $limit, $TotalRowCount, $adminUrl);
		
		//======== Set up output ========//
		$listings = $this->BeginTable('98%','0');		
		$thInfo = array(
				'ID','Event Date','Event Title', 'Recurring Type','Status', 'Edit'
		);	
		$thheight = "35";
		$thwidth = array("5%","5%","10%","20%","20%","20%");
		$listings .= $this->buildHeaders($thInfo, $thheight, $thwidth, count($thInfo));
		$recur = 0;
		
		if (is_array($records) && count($records) != 0) {
			foreach($records as $v) {
				$cnt = $cnt + 1;
				$eventID = $v->id;
				$eventTitle = $v->title;
				$eventDate = $this->ReformatDatedb($v->dt);
				$v->status == 1 ? $status = "Activated" : $status = "Deactivated";
				switch ($v->target) {
					case 1:
						$targetarea = "Calendar Entry Only";
					break;

					case 2:
						$targetarea = "Announcements and Calendar Entry";
					break;

					case 3:
						$targetarea = "Announcements Entry Only";
					break;
				}


				
				switch($v->recurring_type) {
					case 'Day':
						$occurtype = "daily";
					break;

					case 'Week':
						$occurtype = "weekly";
					break;

					case 'Month':
						$occurtype = "monthly";
					break;

					case 'Year':
						$occurtype = "yearly";
					break;
				}
				$recurring_startdate = $v->recurring_startdate;
				$rec_period = $wpdb->get_var("Select COUNT(id) as total_recurrance from " . $this->table . " where recurring_startdate = '$recurring_startdate'");
				if ($v->recurring == 0) { 
					$recur_type = "No recurring dates";
				} else {
					$recur_type = "reoccurs $occurtype for $rec_period {$v->recurring_type}" .  "s";
				}

				//$editImage = $this->_createImage("$shareImgFld/images/ad-icon-edit.gif", '0', '', '');
				//$editLink = $this->_hyperlink ("$adminUrl&process=update&ID=$eventID", $editImage, '', '','');
				$editLink = $this->_doIcon("", "javascript:window.location = '$adminUrl&process=update&ID=$eventID'", "icon-pencil icon_gray fs5 hover", '');

				$rowcontent = array(
					$eventID, $this->_addDiv($eventDate, '', "text-align:center", "",''), $eventTitle, $recur_type,  $this->_addDiv($status, '', "text-align:center", "",'') , $this->_addDiv($editLink, '', "text-align:center", "",'')
				);
				$listings .= $this->buildRow2($rowcontent, '', '', count($rowcontent), '', '', '', '', '', '', 'top');	
			}
		} else {
			$colNum = count($thInfo);
			$listings .= "<tr><td colspan=\"$colNum\">There are currently no listings to view.</td></tr>";
		}
		$listings .= $this->EndTable();
		
		$template = "calendar_announcements.html";
		$content = array (
			'ADMINURL' => $this->adminURL,
			'PAGENAV' => $pageturns,
			'START' => "$start",
			'END' => "$end",
			'LISTCOUNT' => "$TotalRowCount", 
			'LISTINGS' => $listings,
			'LISTINGTYPE' => $list_type,
			'HTML' => $template
		);
		return $content;
		
	}

	public function viewEvents ($eventID, $lookup, $calID, $viewfull) {		
		$wpdb = $this->wpdb;
		$wp_pageID = $wpdb->get_var($wpdb->prepare("Select cal_wp_pageID from " . $this->themetable . " where cal_id = %d", $calID));
		$formID = 0;
		$calendarURL = get_permalink($wp_pageID);
		$eventList = "";
		if ($eventID != "" && is_numeric($eventID)) {
			$sql = "Select * from " . $this->table . " where id = %d";
			$results = $wpdb->get_results($wpdb->prepare($sql, $eventID));
		} else if ($lookup != "") {
			$sql = "Select * from " . $this->table . " where dt = %s and status = 1 and calendar_id = %d order by DT";
			$results = $wpdb->get_results($wpdb->prepare($sql, $lookup, $calID));
		} else {
			$currdate = date("Y-m-d");
			$futuredate =  date("Y-m-d", strtotime("+12 months"));
			$sql = "Select * from " . $this->table . " where dt between %s and %s and status = 1 and calendar_id = %d order by dt";
			$results = $wpdb->get_results($wpdb->prepare($sql, $currdate, $futuredate, $calID));
		}
		$eventID != "" ? $showfull = true : $showfull = false;
		
		
		if(count($results) != 0) {
			$bgcolor = "#f3f5f7";
			foreach($results as $row) {
				//======== Remove images from content ========//
				$event_description = strip_tags(stripslashes($row->description), '<br><b></b><strong></strong><a></a>');
				$event_description = substr($event_description, 0, 200) . "...";
				$description = str_replace("&nbsp;", "", $row->description);
				$description = apply_filters('the_content', stripslashes($description));
				$description = str_replace("<p>", "", $description);
				$description = str_replace("</p>", "<br><br>", $description);
				if ($viewfull) {
					$description = stripslashes($description);
				} else {
					$description = strip_tags(stripslashes($description), '<br><b></b><strong></strong><a></a>');	
				}
				$_SERVER['SERVER_PORT'] == 443 ? $showdescription = str_replace("http", "https", $showdescription) : '';
				$eventID = $row->id;
				$formID = $row->rsvp_form;
				$event_postID = $row->wp_postID;
				$eventDate = $this->date_to_str($row->dt);
				$event_title = str_replace("'", "",stripslashes($row->title));
				$event_title = str_replace("&", "and",$event_title);
				$link_title = str_replace(" ","+",$event_title);
				$address = str_replace(" ","+",stripslashes($row->event_address));
				$city = str_replace(" ","+",stripslashes($row->event_city));
				$state = str_replace(" ","+",stripslashes($row->event_state));
				$zip = str_replace(" ","+",$row->event_zip);
				$jdstartDate = $this->date_to_jd($row->event_start);
				$jdendDate = $this->date_to_jd($row->event_end);	
				$event_rsvp = $row->event_rsvp;
				$rsvpLink = $row->rsvp_link;
				$featuredImage = $row->featured_image;
				$row->event_address != "" ? $event_address = $row->event_address .  "<br>" : $event_address = "";
				$row->event_city != "" && $row->event_state != "" ? $event_address .=  $row->event_city . ", " . $row->event_state : '';


				//Get Event Start Information
				$startHour = $row->start_hour;
				$startMin = $row->start_minute;
				$startPart = $row->start_day;
				$startyear = $row->year;
				$startmonth = $row->month;
				$startday = $row->day;
				$url = get_permalink($event_postID);
				$row->featured_txt != "" && !$showfull ? $showdescription = apply_filters('the_content', $row->featured_txt)  : $showdescription = apply_filters('the_content', $row->description);
				
				strlen($startHour) < 2 ? $startHour = "0" . $startHour : '';
				strlen($startMin) < 2 ? $startMin = "0" . $startMin : '';
				strlen($startmonth) < 2 ? $startmonth = "0" . $startmonth : '';
				strlen($startday) < 2 ? $startday = "0" . $startday : '';

				$eventstart = $startmonth . "/" . $startday . "/" . $startyear . " " . $startHour . ":" .$startMin . " " . $startPart; 

				//Get Event End Information
				if ($row->recurring) {
					$recurStartDate = $row->recurring_startdate;
					$sqlRecur = "Select * from " . $this->table . " where recurring_startdate = %s order by dt desc limit 1"; 
					$resultRecur = $wpdb->get_results($wpdb->prepare($sqlRecur,$recurStartDate));
					if (count($resultRecur) != 0) {
						foreach ($resultRecur as $rec) {
							$dt = $rec->dt;
							$endmonth = $rec->month;
							$end_day = $rec->day;
							$endyear = $rec->year;
							$endhour = $rec->end_hour;
							$endmin = $rec->end_minute;
							$endPart = $rec->end_day;
						}
						strlen($endmonth) < 2 ? $endmonth = "0" . $endmonth : '';
						strlen($end_day) < 2 ? $end_day = "0" . $end_day : '';
						strlen($endhour) < 2 ? $endhour = "0" . $endhour : '';
						strlen($endmin) < 2 ? $endmin = "0" . $endmin : '';
					}					
				} else {
					$endmonth = $startmonth;
					$end_day = $startday;
					$endyear = $startyear;
					$endhour = $row->end_hour;
					$endmin = $row->end_minute;
					$endPart = $row->end_day;

					strlen($endmonth) < 2 ? $endmonth = "0" . $endmonth : '';
					strlen($end_day) < 2 ? $end_day = "0" . $end_day : '';
					strlen($endhour) < 2 ? $endhour = "0" . $endhour : '';
					strlen($endmin) < 2 ? $endmin = "0" . $endmin : '';
				}

				$eventend = $endmonth . "/" . $end_day . "/" . $endyear . " " . $endhour . ":" .$endmin . " " . $endPart;
				$startDate = date('Ymd', strtotime($eventstart)) . "T" . date('His', strtotime($eventstart)) . "Z";
				$endDate = date('Ymd', strtotime($eventend)) . "T" . date('His', strtotime($eventend)) . "Z";

				$companyName = $this->company;
				$companyName = str_replace(" ", "+", $companyName);
				$companyEvent = "{$companyName}+Event:";

				
				$google_url = "http://www.google.com/calendar/event?action=TEMPLATE&dates=$startDate/$endDate&text=$companyEvent+$link_title&location=$address,$city,$state+$zip,United+States&details=$event_title";
				$facebook_url = "http://www.facebook.com/sharer.php?s=100&p[url]=$url&p[title]=$event_title&p[summary]=$eventstart - $address, $city, $state $zip";
				$twitter_url = "http://twitter.com/home?status=$event_title+$eventstart+-+$address,+$city,+$state+$zip";				
				$outlook_url = KYBCAL_URLPATH . "/model/outlook.php?eventID=$eventID&wpID=$wp_pageID";
				
				if ($lookup != "") {
					$postInfo = get_post($event_postID);
					$cat = get_the_category($postInfo->ID);
					if (count($cat) != 0) {
						foreach ($cat as $c) {
							$catName = $c->name;
						}
					}
					$catInitial = strtoupper(substr($catName, 0, 1));
					$authorID = $postInfo->post_author;
					$authorFName = get_the_author_meta('first_name', $authorID ); 
					$authorLName = get_the_author_meta('last_name', $authorID ); 
					$authorSlug = get_the_author_meta('user_nicename', $authorID ); 
					//$authorImage = get_wp_user_avatar($authorID, 'thumbnail');
					//$authorLink = $cmcls->get_the_author_posts_link_by_id($authorID, '');
					//$post_short_summary = simple_fields_fieldgroup('post_short_summary', $event_postID);
					//$post_short_summary == "" ? $post_short_summary = $this->get_excerpt_by_id($event_postID, 52) : '';
					//$sharethis = do_shortcode('[sharethis]');
					$eventlist_template = KYBCAL_ABSPATH . "/view/html/cal_eventrow.html";
					$eventcontent = array(
						'THEMEURL' => $this->templateURL,
						'SITEURL' => site_url(),
						'POSTID' => "$event_postID",
						'AUTHORIMAGE' => $authorImage,
						'AUTHORNAME' => $authorFName . " " . $authorLName,
						'AUTHORFIRST' => $authorFName,
						'AUTHORLAST' => $authorLName,
						'AUTHORSLUG' => $authorSlug,
						'AUTHORLINK' => $authorLink,
						'CATEGORYINITIAL' => $catInitial,
						'EVENTTITLE' => $event_title,
						'EVENTURL' => $url,
						'EVENTSUMMARY' => $post_short_summary,
						'EVENTDESCRIPTION' => $showdescription,
						'SHARETHIS' => $sharethis,

					);
					$eventInfo = $this->showPage ($eventlist_template, $eventcontent, true);
					$eventInfo = "<div class='event_titlearea'><h4 class='calh4' >$event_title</h4>{$eventDate}</div><div class='event_desc'>$description<br></div><hr/>";
					$listtemplate = KYBCAL_ABSPATH . "/view/html/calendar_eventlist.html";
				} else {
					$listtemplate = KYBCAL_ABSPATH . "/view/html/calendar_eventbox.html";
				}
				if ($event_rsvp)  { //This needs to be taken out of the main plugin
					if ($formID != 0) {
						$formWP = $wpdb->get_var($wpdb->prepare("Select form_wp_page from forms where form_id = %d", $formID));
						$formPayment = $wpdb->get_var($wpdb->prepare("Select form_payment from forms where form_id = %d", $formID));
						$rsvpLink = get_permalink($formWP) . "$formID/";
					} 

					$formPayment == 1 ? $_SERVER['SERVER_PORT'] != 443 ? $rsvpLink = str_replace("http", "https", $rsvpLink): '' : '';
					$rsvpLink = "<div align='center'><div class=\"btn_rsvp\" onclick=\"javascript:window.open('$rsvpLink')\">Click Here to RSVP</div></div>";
				}
				if ($featuredImage != "" && !$showfull) {
					$descstyle = "style='float:left;width:50%;'";
					$descimgstyle = "style='float:left;width:50%;'";
				} else {
					$descstyle = "style='float:none;width:100%;'";
					$descimgstyle = "style='display:none;'";
				}
				
				
				$listcontent = array(
					'TEMPLATEURL' => $this->templateURL,
					'PLUGINURL' => KYBCAL_URLPATH ,
					'EVENTURL' => $url,
					'EVENTTITLE' => $event_title,
					'EVENTDATE' => $eventDate,
					'EVENTDESCRIPTION' => $showdescription,
					'ADDTHISEVENTDESCRIPTION' => $event_title,
					'GOOGLELINK' => $google_url,
					'FACEBOOKSHARELINK' => $facebook_url,
					'TWITTERLINK' => $twitter_url,
					'BGCOLOR' => $bgcolor,
					'EVENTLIST' => $eventInfo,
					'RSVPLINK' => $rsvpLink,
					'DESCSTYLE' => $descstyle,
					'DESCIMAGESTYLE' => $descimgstyle,
					'FEATUREIMAGE' => $featuredImage,
					'EVENTADDRESS' => $event_address,
					'COMPANY' => $companyName,
					'OUTLOOKURL' => $outlook_url,
					'CALURL' => $calendarURL,
				);			
				$eventList .= $this->showPage ($listtemplate, $listcontent, true);
				switch ($bgcolor) {
					case "#f7f7fb":
						$bgcolor = "#ffffff";
					break;
					case "#ffffff":
						$bgcolor = "#f7f7fb";
					break;
				}
				
			}
		} else {
			$eventList = "<p>There are currently no events to list for this day.  Please check back with us at a later date.";
		}
		
		$template = "calendar_eventlist.html";
		$content = array (
			'EVENTLIST' => $eventList,
			'HTML' => $template
		);
		return $content;
	}	

	public function cal_settings($calID){
		$wpdb = $this->wpdb;
		$imageFolder = KYBCAL_URLPATH . "/images";
		$pluginUrl = KYBCAL_URLPATH;
		$adminURL = $this->adminURL;
		$row = $this->get_calInfo($calID);
       $process = $this->checkRequest('process');		
		$ajaxURL = KYBCAL_AJAXPATH . "/model/calendar_ajax.php";
	
		if ($calID != "" && $process != 'deletetheme') {
			$this->fillCal($wp_categoryID,$calID);
			$configInfo = $wpdb->get_row("Select * from " . $this->configtable);			
			$fontChoices = json_decode($configInfo->fonts, true);
			$head_days = json_decode($configInfo->week_days, true);
			$fontSizes = explode(",", $configInfo->font_sizes);
			$fonts = array();
			$fontsizes = array();
			$wkdays = array();
			foreach ($fontChoices as $key => $value) {
				foreach ($value as $k => $v) {
					$fonts[$k] = $k;
				}
			}
			foreach ($fontSizes as $key => $value) {
				$fontsizes[$value] = $value;
			}
			foreach ($head_days as $key => $value) {
				foreach ($value as $k => $v) {
					$wkdays[$v] = $k;
				}
			}
			
			
			$styles = array('bold' => 'Bold', 'italic' => 'Italic', 'bolditalic' => 'Bold/Italic');
			$arrow_fontoptions = $this->buildOptions($fonts, $row->arrows_font, '', '', 0, 0, true, '', '');
			$monyr_fontoptions = $this->buildOptions($fonts, $row->monyr_font, '', '', 0, 0, true, '', '');
			$wkname_fontoptions = $this->buildOptions($fonts, $row->wkname_font, '', '', 0, 0, true, '', '');
			$wkday_fontoptions = $this->buildOptions($fonts, $row->wkday_font, '', '', 0, 0, true, '', '');
			$selwkday_fontoptions = $this->buildOptions($fonts, $row->selwkday_font, '', '', 0, 0, true, '', '');
			$event_link_fontoptions = $this->buildOptions($fonts, $row->event_link_font, '', '', 0, 0, true, '', '');
			$apply_fontoptions = $this->buildOptions($fonts, $row->apply_font, '', '', 0, 0, true, '', '');
			$direct_fontoptions = $this->buildOptions($fonts, $row->direct_font, '', '', 0, 0, true, '', '');
			$view_fontoptions = $this->buildOptions($fonts, $row->view_font, '', '', 0, 0, true, '', '');
			$current_fontoptions = $this->buildOptions($fonts, $row->current_font, '', '', 0, 0, true, '', '');

			$arrow_fontsizeoptions = $this->buildOptions($fontsizes, $row->arrows_fontsize, '', '', 0, 0, true, '', '');
			$monyr_fontsizeoptions = $this->buildOptions($fontsizes, $row->monyr_fontsize, '', '', 0, 0, true, '', '');
			$wkname_fontsizeoptions = $this->buildOptions($fontsizes, $row->wkname_fontsize, '', '', 0, 0, true, '', '');
			$wkday_fontsizeoptions = $this->buildOptions($fontsizes, $row->wkday_fontsize, '', '', 0, 0, true, '', '');
			$selwkday_fontsizeoptions = $this->buildOptions($fontsizes, $row->selwkday_fontsize, '', '', 0, 0, true, '', '');
			$event_link_fontsizeoptions = $this->buildOptions($fontsizes, $row->event_link_fontsize, '', '', 0, 0, true, '', '');
			$apply_fontsizeoptions = $this->buildOptions($fontsizes, $row->apply_fontsize, '', '', 0, 0, true, '', '');
			$direct_fontsizeoptions = $this->buildOptions($fontsizes, $row->direct_fontsize, '', '', 0, 0, true, '', '');
			$view_fontsizeoptions = $this->buildOptions($fontsizes, $row->view_fontsize, '', '', 0, 0, true, '', '');
			$current_fontsizeoptions = $this->buildOptions($fontsizes, $row->current_fontsize, '', '', 0, 0, true, '', '');

			$arrow_styleoptions = $this->buildOptions($styles, $row->arrows_style, '', '', 0, 0, true, '', '');
			$monyr_styleoptions = $this->buildOptions($styles, $row->monyr_style, '', '', 0, 0, true, '', '');
			$wkname_styleoptions = $this->buildOptions($styles, $row->wkname_style, '', '', 0, 0, true, '', '');
			$wkday_styleoptions = $this->buildOptions($styles, $row->wkday_style, '', '', 0, 0, true, '', '');
			$selwkday_styleoptions = $this->buildOptions($styles, $row->selwkday_style, '', '', 0, 0, true, '', '');
			$event_link_styleoptions = $this->buildOptions($styles, $row->event_link_style, '', '', 0, 0, true, '', '');
			$apply_styleoptions = $this->buildOptions($styles, $row->apply_style, '', '', 0, 0, true, '', '');
			$direct_styleoptions = $this->buildOptions($styles, $row->direct_style, '', '', 0, 0, true, '', '');
			$view_styleoptions = $this->buildOptions($styles, $row->view_style, '', '', 0, 0, true, '', '');
			$current_styleoptions = $this->buildOptions($styles, $row->current_style, '', '', 0, 0, true, '', '');

			$show_settings_options = $this->buildOptions($this->bitOptions, $row->show_list, '', '', 0, 0, true, '', '');
			$show_nextprev_options = $this->buildOptions($this->bitOptions, $row->show_nextprev, '', '', 0, 0, true, '', '');
			$show_views_options = $this->buildOptions($this->bitOptions, $row->show_views, '', '', 0, 0, true, '', '');
			$show_myapply_options = $this->buildOptions($this->bitOptions, $row->show_myapply, '', '', 0, 0, true, '', '');
			$cal_daystart_options = $this->buildOptions($wkdays, $row->start_wday, '', '', 0, 0, true, '', '');
			
			$cal_status_options = $this->buildOptions($this->activateOptions, $row->calStatus, '', '', 0, 0, true, '', '');
			$calendarcontent = $this->viewCalendar($calID, false);
			$calTemplate = KYBCAL_ABSPATH . "/view/html/" . $calendarcontent['HTML'];
			$calendarview = $this->showPage ($calTemplate, $calendarcontent, true);		
			
			$template = "calendar_settings.html";
		} else {
			$sql = "Select * from " . $this->themetable;
			$results = $wpdb->get_results($sql);
			if (count($results) != 0) {
				foreach ($results as $row) {
					$calID = $row->cal_id;
					$calTitle = stripslashes($row->cal_title);
					$calHook = $row->cal_hook;
					$calManage = $this->_doIcon("", "javascript:window.location='$adminURL&process=manage&calID=$calID'", "icon-cog icon_gray fs5 hover", '');
					$calEdit = $this->_doIcon("", "javascript:window.location='$adminURL&process=settings&calID=$calID'", "icon-pencil icon_gray fs5 hover", '');
					$calDelete = $this->_doIcon("", "javascript:calendar.view.checkDelete('$adminURL&process=deletetheme&calID=$calID')", "icon-trash icon_gray fs5 hover", '');
					$row->cal_status == 1 ? $calStatus = "Activated" : $calStatus = "Deactivated"; 
					$alignset = array('left', 'left', 'left', 'center', 'center','center');
					$calcontent = array(
						$calTitle, $calHook, $calStatus, $calManage, $calEdit, $calDelete
					);
					$calthemeList .= $this->buildRow2($calcontent, '', '', count($calcontent), $alignset, '', '', '', '', '','top');
				}
			} else {
				$calcontent = array(
					'There are currently no calendar themes available.'
				);
				$calthemeList .= $this->buildRow2($calcontent, '', '', count($calcontent), '', '', '5', '', '', '','top');
			}
			$template="calendar_theme_list.html";
		}
		
		$cal_wp_page = $this->wp_page_dropdown($row->wp_pageID, 'form[cal_wp_pageID][i]', 'span7', 'cal_wp_pageID');
		$cal_wp_category = $this->wp_categories_dropdown($row->wp_categoryID, 'form[cal_wp_categoryID][i]', 'span7', 'cal_wp_categoryID', '-- Add New Category --');
		$settingblocks = "";
		$num = 2;
		$titles = array(
			'Blocklist' => array(
					'title' => 'Calendar Directional Arrows',
					'font' => array('name' => 'form[arrows_font][s]', 'id' => 'arrows_font', 'value' => $arrow_fontoptions),
					'fontsize' => array('name' => 'form[arrows_fontsize][s]', 'id' => 'arrows_fontsize', 'value' => $arrow_fontsizeoptions),
					'style' => array('name' => 'form[arrows_style][s]', 'id' => 'arrows_style', 'value' => $arrow_styleoptions),
					'color' => array('name' => 'form[arrow_color][s]', 'id' => 'arrow_color', 'value' => $row->arrow_color, 'defaultcolor' => '#ffffff', 'classname' => 'arrow-color-field'),
					'bg' => array('name' => 'form[arrow_bgcolor][s]', 'id' => 'arrow_bgcolor', 'value' => $row->arrow_bgcolor, 'defaultcolor' => '#5e899a', 'classname' => 'arrow-bgcolor-color-field'),
					'border' => array('name' => 'form[arrow_border][s]', 'id' => 'arrow_border', 'value' => $row->arrow_border, 'defaultcolor' => '#ffffff', 'classname' => 'arrow-border-color-field'),
					'image' => false
				),
				array(
					'title' => 'Calendar Month/Year',
					'font' => array('name' => 'form[monyr_font][s]', 'id' => 'monyr_font', 'value' => $monyr_fontoptions),
					'fontsize' => array('name' => 'form[monyr_fontsize][s]', 'id' => 'monyr_fontsize', 'value' => $monyr_fontsizeoptions),
					'style' => array('name' => 'form[monyr_style][s]', 'id' => 'monyr_style', 'value' => $monyr_styleoptions),
					'color' => array('name' => 'form[monyr_color][s]', 'id' => 'monyr_color', 'value' => $row->monyr_color, 'defaultcolor' => '#ffffff', 'classname' => 'monyr-color-field'),
					'bg' => array('name' => 'form[monyr_bgcolor][s]', 'id' => 'monyr_bgcolor', 'value' => $row->monyr_bgcolor, 'defaultcolor' => '#ffffff', 'classname' => 'monyr-bgcolor-color-field'),
					'border' => array('name' => 'form[monyr_border][s]', 'id' => 'monyr_border', 'value' => $row->monyr_border, 'defaultcolor' => '#ffffff', 'classname' => 'monyr-border-color-field'),
					'image' => false
				),
				array(
					'title' => 'Calendar Names Of The Week',
					'font' => array('name' => 'form[wkname_font][s]', 'id' => 'wkname_font', 'value' => $wkname_fontoptions),
					'fontsize' => array('name' => 'form[wkname_fontsize][s]', 'id' => 'wkname_fontsize', 'value' => $wkname_fontsizeoptions),
					'style' => array('name' => 'form[wkname_style][s]', 'id' => 'wkname_style', 'value' => $wkname_styleoptions),
					'color' => array('name' => 'form[wkname_color][s]', 'id' => 'wkname_color', 'value' => $row->wkname_color, 'defaultcolor' => '#ffffff', 'classname' => 'wkname-color-field'),
					'bg' => array('name' => 'form[wkname_bgcolor][s]', 'id' => 'wkname_bgcolor', 'value' => $row->wkname_bgcolor, 'defaultcolor' => '#e77d07', 'classname' => 'wkname-bgcolor-color-field'),
					'border' => array('name' => 'form[wkname_border][s]', 'id' => 'wkname_border', 'value' => $row->wkname_border, 'defaultcolor' => '#ffffff', 'classname' => 'wkname-border-color-field'),
					'image' => false
				),
				
			);
		$titles = json_decode(json_encode($titles), FALSE);
		$subtitles1 = array(
			'Headers' => array(
					'title' => 'Calendar Apply Button',
					'font' => array('name' => 'form[apply_font][s]', 'id' => 'apply_font', 'value' => $apply_fontoptions),
					'fontsize' => array('name' => 'form[apply_fontsize][s]', 'id' => 'apply_fontsize', 'value' => $apply_fontsizeoptions),
					'style' => array('name' => 'form[apply_style][s]', 'id' => 'apply_style', 'value' => $apply_styleoptions),
					'color' => array('name' => 'form[apply_color][s]', 'id' => 'apply_color', 'value' => $row->apply_color, 'defaultcolor' => '#ffffff', 'classname' => 'apply-color-field'),
					'bg' => array('name' => 'form[apply_bgcolor][s]', 'id' => 'apply_bgcolor', 'value' => $row->apply_bgcolor, 'defaultcolor' => '#5c5c5c', 'classname' => 'apply-bgcolor-color-field'),
					'border' => array('name' => 'form[apply_border][s]', 'id' => 'apply_border', 'value' => $row->apply_border, 'defaultcolor' => '#ffffff', 'classname' => 'apply-border-color-field'),
					'image' => false
				),
				array(
					'title' => 'Calendar Directional Buttons',
					'font' => array('name' => 'form[direct_font][s]', 'id' => 'direct_font', 'value' => $direct_fontoptions),
					'fontsize' => array('name' => 'form[direct_fontsize][s]', 'id' => 'direct_fontsize', 'value' => $direct_fontsizeoptions),
					'style' => array('name' => 'form[direct_style][s]', 'id' => 'direct_style', 'value' => $direct_styleoptions),
					'color' => array('name' => 'form[direct_color][s]', 'id' => 'direct_color', 'value' => $row->direct_color, 'defaultcolor' => '#ffffff', 'classname' => 'direct-color-field'),
					'bg' => array('name' => 'form[direct_bgcolor][s]', 'id' => 'direct_bgcolor', 'value' => $row->direct_bgcolor, 'defaultcolor' => '#0079c0', 'classname' => 'direct-bgcolor-color-field'),
					'border' => array('name' => 'form[direct_border][s]', 'id' => 'direct_border', 'value' => $row->direct_border, 'defaultcolor' => '#ffffff', 'classname' => 'direct-border-color-field'),
					'image' => false
				),
				array(
					'title' => 'Calendar View Buttons',
					'font' => array('name' => 'form[view_font][s]', 'id' => 'view_font', 'value' => $view_fontoptions),
					'fontsize' => array('name' => 'form[view_fontsize][s]', 'id' => 'view_fontsize', 'value' => $view_fontsizeoptions),
					'style' => array('name' => 'form[view_style][s]', 'id' => 'view_style', 'value' => $view_styleoptions),
					'color' => array('name' => 'form[view_color][s]', 'id' => 'view_color', 'value' => $row->view_color, 'defaultcolor' => '#ffffff', 'classname' => 'view-color-field'),
					'bg' => array('name' => 'form[view_bgcolor][s]', 'id' => 'view_bgcolor', 'value' => $row->view_bgcolor, 'defaultcolor' => '#2ac545', 'classname' => 'view-bgcolor-color-field'),
					'border' => array('name' => 'form[view_border][s]', 'id' => 'view_border', 'value' => $row->view_border, 'defaultcolor' => '#ffffff', 'classname' => 'view-border-color-field'),
					'image' => false
				),
			);
		$subtitles1 = json_decode(json_encode($subtitles1), FALSE);

		$subtitles2 = array(
			'WeekDays' => array(
					'title' => 'Calendar Week Days',
					'font' => array('name' => 'form[wkday_font][s]', 'id' => 'wkday_font', 'value' => $wkday_fontoptions),
					'fontsize' => array('name' => 'form[wkday_fontsize][s]', 'id' => 'wkday_fontsize', 'value' => $wkday_fontsizeoptions),
					'style' => array('name' => 'form[wkday_style][s]', 'id' => 'wkday_style', 'value' => $wkday_styleoptions),
					'color' => array('name' => 'form[wkday_color][s]', 'id' => 'wkday_color', 'value' => $row->wkday_color, 'defaultcolor' => '#464646', 'classname' => 'wkday-color-field'),
					'bg' => array('name' => 'form[wkday_bgcolor][s]', 'id' => 'wkday_bgcolor', 'value' => $row->wkday_bgcolor, 'defaultcolor' => '#ffffff', 'classname' => 'wkday-bgcolor-color-field'),
					'border' => array('name' => 'form[wkday_border][s]', 'id' => 'wkday_border', 'value' => $row->wkday_border, 'defaultcolor' => '#ffffff', 'classname' => 'wkday-border-color-field'),
					'image' => array('name' => 'form[wkday_image][s]', 'id' => 'wkday_image', 'value' => $row->wkday_image, 'srcname' => 'wkday_image_view', 'imgbtn' => 'wkday_image_button')
				),
				array(
					'title' => 'Calendar Selected Week Days',
					'font' => array('name' => 'form[selwkday_font][s]', 'id' => 'selwkday_font', 'value' => $selwkday_fontoptions),
					'fontsize' => array('name' => 'form[selwkday_fontsize][s]', 'id' => 'selwkday_fontsize', 'value' => $selwkday_fontsizeoptions),
					'style' => array('name' => 'form[selwkday_style][s]', 'id' => 'selwkday_style', 'value' => $selwkday_styleoptions),
					'color' => array('name' => 'form[selwkday_color][s]', 'id' => 'selwkday_color', 'value' => $row->selwkday_color, 'defaultcolor' => '#e77d07', 'classname' => 'selwkday-color-field'),
					'bg' => array('name' => 'form[selwkday_bgcolor][s]', 'id' => 'selwkday_bgcolor', 'value' => $row->selwkday_bgcolor, 'defaultcolor' => '#eeeeee', 'classname' => 'selwkday-bgcolor-color-field'),
					'border' => array('name' => 'form[selwkday_border][s]', 'id' => 'selwkday_border', 'value' => $row->selwkday_border, 'defaultcolor' => '#ffffff', 'classname' => 'selwkday-border-color-field'),
					'image' => array('name' => 'form[selwkday_image][s]', 'id' => 'selwkday_image', 'value' => $row->selwkday_image, 'srcname' => 'selwkday_image_view', 'imgbtn' => 'selwkday_image_button')
				),
				array(
					'title' => 'Calendar Week Day Links',
					'font' => array('name' => 'form[event_link_font][s]', 'id' => 'event_link_font', 'value' => $event_link_fontoptions),
					'fontsize' => array('name' => 'form[event_link_fontsize][s]', 'id' => 'event_link_fontsize', 'value' => $event_link_fontsizeoptions),
					'style' => array('name' => 'form[event_link_style][s]', 'id' => 'event_link_style', 'value' => $event_link_styleoptions),
					'color' => array('name' => 'form[event_link_color][s]', 'id' => 'event_link_color', 'value' => $row->event_link_color, 'defaultcolor' => '#b04040', 'classname' => 'event-link-color-field'),
					'bg' => array('name' => 'form[event_link_bgcolor][s]', 'id' => 'event_link_bgcolor', 'value' => $row->event_link_bgcolor, 'defaultcolor' => '#ffffff', 'classname' => 'event-link-bgcolor-color-field'),
					'border' => array('name' => 'form[eventlink_border][s]', 'id' => 'eventlink_border', 'value' => $row->event_link_border, 'defaultcolor' => '#ffffff', 'classname' => 'event-link-border-color-field'),
					'image' => false
				),
				array(
					'title' => 'Calendar Current Day',
					'font' => array('name' => 'form[current_font][s]', 'id' => 'current_font', 'value' => $current_fontoptions),
					'fontsize' => array('name' => 'form[current_fontsize][s]', 'id' => 'current_fontsize', 'value' => $current_fontsizeoptions),
					'style' => array('name' => 'form[current_style][s]', 'id' => 'current_style', 'value' => $current_styleoptions),
					'color' => array('name' => 'form[current_color][s]', 'id' => 'current_color', 'value' => $row->current_color, 'defaultcolor' => '#464646', 'classname' => 'current-color-field'),
					'bg' => array('name' => 'form[current_bgcolor][s]', 'id' => 'current_bgcolor', 'value' => $row->current_bgcolor, 'defaultcolor' => '#dddddd', 'classname' => 'current-bgcolor-color-field'),
					'border' => array('name' => 'form[current_border][s]', 'id' => 'current_border', 'value' => $row->current_border, 'defaultcolor' => '#dddddd', 'classname' => 'current-border-color-field'),
					'image' => false
				),
			);
		$subtitles2 = json_decode(json_encode($subtitles2), FALSE);

		$settingblocks = $this->get_tab_blocks($titles, 'body', 'h4', false, 3, true);
		$sub_blocks1 = $this->get_tab_blocks($subtitles1, 'subbody', 'h4', true, 1, false);
		$sub_blocks2 = $this->get_tab_blocks($subtitles2, 'subbody', 'h4', true, 4, false);
		
		$content = array (
			'ADMINURL' => $this->adminURL,
			'PLUGINURL' => $pluginUrl,
			'AJAXURL' => "$ajaxURL",
			'CALID' => "$calID",
			'CALHOOK' => $row->calHook,
			'CALTITLE' => stripslashes($row->calTitle),
			'CALDESC' => stripslashes($row->calDesc),
			'CALTHEMELIST' => $calthemeList,
			'CALURL' => KYBCAL_URLPATH,
			'SHOWSETTINGSOPTIONS' => $show_settings_options,
			'STARTWKDAYSOPTIONS' => $cal_daystart_options,
			'NEXTPREVOPTIONS' => $show_nextprev_options,
			'SHOWVIEWPREVOPTIONS' => $show_views_options,
			'MTHYROPTIONS' => $show_myapply_options,
			'CALENDARSTATUSOPTIONS' => $cal_status_options,
			'CALVIEW' => $calendarview,
			'WP_PAGE' => $cal_wp_page,
			'WP_CATEGORY' => $cal_wp_category,
			'SETTINGBLOCKS' => $settingblocks,
			'SUBBLOCKS1' => $sub_blocks1,
			'SUBBLOCKS2' => $sub_blocks2,
			'HTML' => $template
		);
		return $content;
	}

	public function get_tab_blocks($titles, $idname, $htag, $openfirst, $num, $widtitle) {
		$openfirst ? $class = 'open' : $class = 'closed';
		$widtitle ? $class .= " widget" : '';
		foreach ($titles as $t) {	
			$blocktemplate = KYBCAL_ABSPATH . "/view/html/calendar_settingblock.html";
			$blockcontent = array(
				'blocknum' => $idname . $num,
				'class' => $class,
				'heading' => "<$htag class=\"heading\">{$t->title}</$htag>",
				'headingtitle' => $t->title,
				'fontfieldname' => $t->font->name,
				'fontfieldid' => $t->font->id,
				'fontfieldvalue' => $t->font->value,
				'fontsizefieldname' => $t->fontsize->name,
				'fontsizefieldid' => $t->fontsize->id,
				'fontsizefieldvalue' => $t->fontsize->value,
				'stylefieldname' => $t->style->name,
				'stylefieldid' => $t->style->id,
				'stylefieldvalue' => $t->style->value,
				'colorfieldname' => $t->color->name,
				'colorfieldid' => $t->color->id,
				'colorfieldvalue' => $t->color->value,
				'colordefault' => $t->color->defaultcolor,
				'colorclass' => $t->color->classname,
				'bgfieldname' => $t->bg->name,
				'bgfieldid' => $t->bg->id,
				'bgfieldvalue' => $t->bg->value,
				'bgdefault' => $t->bg->defaultcolor,
				'bgclass' => $t->bg->classname,
				'borderfieldname' => $t->border->name,
				'borderfieldid' => $t->border->id,
				'borderfieldvalue' => $t->border->value,
				'borderdefault' => $t->border->defaultcolor,
				'borderclass' => $t->border->classname,
			);
			if ($t->image) {
				$blockcontent['imgclass'] = "";
				$blockcontent['imgname'] = $t->image->name;
				$blockcontent['imgid'] = $t->image->id;
				$blockcontent['imgval'] = $t->image->value;
				$blockcontent['imgbtn'] = $t->image->imgbtn;
				$blockcontent['imgsrcname'] = $t->image->srcname;
				$t->image->value != "" ? $blockcontent['viewclass'] = "" : $blockcontent['viewclass'] = "closed";
			} else {
				$blockcontent['imgclass'] = "closed";
			}
			$settingblocks .= $this->showPage ($blocktemplate, $blockcontent, true);	
			$num++;
			$widtitle ? $class .= "closed widget" : $class="closed";
		}
		return $settingblocks;
	}

	public function get_calInfo($calID) {
		$wpdb = $this->wpdb;
		$calInfo = array();
		/*echo "<pre>";
		print_r($_POST);
		echo "</pre>";*/
		//Set defaults
		$this->checkRequest('cal_title') != "" ? $calInfo['calTitle'] = $this->checkRequest('cal_title') : $calInfo['calTitle'] = '';
		$this->checkRequest('cal_description') != "" ? $calInfo['calDesc'] = $this->checkRequest('cal_description') : $calInfo['calDesc'] = '';
		$calInfo['calHook'] = '';
		$this->checkRequest('cal_status') != "" ? $calInfo['calStatus'] = $this->checkRequest('cal_status') : $calInfo['calStatus'] = 0;
		$preview = $this->checkRequest('preview');
		
		//======== Fonts ========//
		$this->checkRequest('arrows_font') != '' ? $calInfo['arrows_font'] = $this->checkRequest('arrows_font') : $calInfo['arrows_font'] = 'Arial';
		$this->checkRequest('monyr_font') != '' ? $calInfo['monyr_font'] = $this->checkRequest('monyr_font') : $calInfo['monyr_font'] = 'Arial';
		$this->checkRequest('wkname_font') != '' ? $calInfo['wkname_font'] = $this->checkRequest('wkname_font') : $calInfo['wkname_font'] = 'Arial';
		$this->checkRequest('wkday_font') != '' ? $calInfo['wkday_font'] = $this->checkRequest('wkday_font') : $calInfo['wkday_font'] = 'Arial';
		$this->checkRequest('selwkday_font') != '' ? $calInfo['selwkday_font'] = $this->checkRequest('selwkday_font') : $calInfo['selwkday_font'] = 'Arial';
		$this->checkRequest('selwkday_font') != '' ? $calInfo['selwkday_font'] = $this->checkRequest('selwkday_font') : $calInfo['selwkday_font'] = 'Arial';
		$this->checkRequest('event_link_font') != '' ? $calInfo['event_link_font'] = $this->checkRequest('event_link_font') : $calInfo['event_link_font'] = 'Arial';
		$this->checkRequest('apply_font') != '' ? $calInfo['apply_font'] = $this->checkRequest('apply_font') : $calInfo['apply_font'] = 'Arial';
		$this->checkRequest('direct_font') != '' ? $calInfo['direct_font'] = $this->checkRequest('direct_font') : $calInfo['direct_font'] = 'Arial';
		$this->checkRequest('view_font') != '' ? $calInfo['view_font'] = $this->checkRequest('view_font') : $calInfo['view_font'] = 'Arial';
		$this->checkRequest('current_font') != '' ? $calInfo['current_font'] = $this->checkRequest('current_font') : $calInfo['current_font'] = 'Arial';
		
		//======== Font Sizes ========//
		$this->checkRequest('arrows_fontsize') != '' ? $calInfo['arrows_fontsize'] = $this->checkRequest('arrows_fontsize') : $calInfo['arrows_fontsize'] = '';
		$this->checkRequest('monyr_fontsize') != '' ? $calInfo['monyr_fontsize'] = $this->checkRequest('monyr_fontsize') : $calInfo['monyr_fontsize'] = '';
		$this->checkRequest('wkname_fontsize') != '' ? $calInfo['wkname_fontsize'] = $this->checkRequest('wkname_fontsize') : $calInfo['wkname_fontsize'] = '';
		$this->checkRequest('wkday_fontsize') != '' ? $calInfo['wkday_fontsize'] = $this->checkRequest('wkday_fontsize') : $calInfo['wkday_fontsize'] = '';
		$this->checkRequest('selwkday_fontsize') != '' ? $calInfo['selwkday_fontsize'] = $this->checkRequest('selwkday_fontsize') : $calInfo['selwkday_fontsize'] = '';
		$this->checkRequest('event_link_fontsize') != '' ? $calInfo['event_link_fontsize'] = $this->checkRequest('event_link_fontsize') : $calInfo['event_link_fontsize'] = '';
		$this->checkRequest('apply_fontsize') != '' ? $calInfo['apply_fontsize'] = $this->checkRequest('apply_fontsize') : $calInfo['apply_fontsize'] = '';
		$this->checkRequest('direct_fontsize') != '' ? $calInfo['direct_fontsize'] = $this->checkRequest('direct_fontsize') : $calInfo['direct_fontsize'] = '';
		$this->checkRequest('view_fontsize') != '' ? $calInfo['view_fontsize'] = $this->checkRequest('view_fontsize') : $calInfo['view_fontsize'] = '';
		$this->checkRequest('current_fontsize') != '' ? $calInfo['current_fontsize'] = $this->checkRequest('current_fontsize') : $calInfo['current_fontsize'] = '';

		//======== Colors ========//		
		$this->checkRequest('arrow_color') != '' ? $calInfo['arrow_color'] = "#" . $this->checkRequest('arrow_color') : $calInfo['arrow_color'] = '#ffffff';
		$this->checkRequest('monyr_color') != '' ? $calInfo['monyr_color'] = "#" . $this->checkRequest('monyr_color') : $calInfo['monyr_color'] = '#464646';
		$this->checkRequest('wkname_color') != '' ? $calInfo['wkname_color'] = "#" . $this->checkRequest('wkname_color') : $calInfo['wkname_color'] = '#ffffff';
		$this->checkRequest('wkday_color') != '' ? $calInfo['wkday_color'] = "#" . $this->checkRequest('wkday_color') : $calInfo['wkday_color'] = '#464646';
		$this->checkRequest('selwkday_color') != '' ? $calInfo['selwkday_color'] = "#" . $this->checkRequest('selwkday_color') : $calInfo['selwkday_color'] = '#e77d07';
		$this->checkRequest('event_link_color') != '' ? $calInfo['event_link_color'] = "#" . $this->checkRequest('event_link_color') : $calInfo['event_link_color'] = '#e77d07';
		$this->checkRequest('apply_color') != '' ? $calInfo['apply_color'] = "#" . $this->checkRequest('apply_color') : $calInfo['apply_color'] = '#e77d07';
		$this->checkRequest('direct_color') != '' ? $calInfo['direct_color'] = "#" . $this->checkRequest('direct_color') : $calInfo['direct_color'] = '#e77d07';
		$this->checkRequest('view_color') != '' ? $calInfo['view_color'] = "#" . $this->checkRequest('view_color') : $calInfo['view_color'] = '#e77d07';
		$this->checkRequest('current_color') != '' ? $calInfo['current_color'] = "#" . $this->checkRequest('current_color') : $calInfo['current_color'] = '#e77d07';		

		//======== Styles ========//
		$this->checkRequest('arrows_style') != '' ? $calInfo['arrows_style'] = $this->checkRequest('arrows_style') : $calInfo['arrows_style'] = '';
		$this->checkRequest('monyr_style') != '' ? $calInfo['monyr_style'] = $this->checkRequest('monyr_style') : $calInfo['monyr_style'] = '';
		$this->checkRequest('wkname_style') != '' ? $calInfo['wkname_style'] = $this->checkRequest('wkname_style') : $calInfo['wkname_style'] =  'bold';
		$this->checkRequest('selwkday_style') != '' ? $calInfo['selwkday_style'] = $this->checkRequest('selwkday_style') : $calInfo['selwkday_style'] = '';
		$this->checkRequest('wkday_style') != '' ? $calInfo['wkday_style'] = $this->checkRequest('wkday_style') : $calInfo['wkday_style'] = '';
		$this->checkRequest('event_link_style') != '' ? $calInfo['event_link_style'] = $this->checkRequest('event_link_style') : $calInfo['event_link_style'] = '';
		$this->checkRequest('apply_style') != '' ? $calInfo['apply_style'] = $this->checkRequest('apply_style') : $calInfo['apply_style'] = '';
		$this->checkRequest('direct_style') != '' ? $calInfo['direct_style'] = $this->checkRequest('direct_style') : $calInfo['direct_style'] = '';
		$this->checkRequest('view_style') != '' ? $calInfo['view_style'] = $this->checkRequest('view_style') : $calInfo['view_style'] = '';
		$this->checkRequest('current_style') != '' ? $calInfo['current_style'] = $this->checkRequest('current_style') : $calInfo['current_style'] = '';

		//======== BG Colors ========//
		$this->checkRequest('arrow_bgcolor') != '' ? $calInfo['arrow_bgcolor'] = "#" . $this->checkRequest('arrow_bgcolor') : $calInfo['arrow_bgcolor'] = '#5e899a';
		$this->checkRequest('monyr_bgcolor') != '' ? $calInfo['monyr_bgcolor'] = "#" . $this->checkRequest('monyr_bgcolor') : $calInfo['monyr_bgcolor'] = '#ffffff';
		$this->checkRequest('wkname_bgcolor') != '' ? $calInfo['wkname_bgcolor'] = "#" . $this->checkRequest('wkname_bgcolor') : $calInfo['wkname_bgcolor'] = '#e77d07';
		$this->checkRequest('selwkday_bgcolor') != '' ? $calInfo['selwkday_bgcolor'] = "#" . $this->checkRequest('selwkday_bgcolor') : $calInfo['selwkday_bgcolor'] = '#eeeeee';
		$this->checkRequest('wkday_bgcolor') != '' ? $calInfo['wkday_bgcolor'] = "#" . $this->checkRequest('wkday_bgcolor') : $calInfo['wkday_bgcolor'] = '#ffffff';
		$this->checkRequest('event_link_bgcolor') != '' ? $calInfo['event_link_bgcolor'] = "#" . $this->checkRequest('event_link_bgcolor') : $calInfo['event_link_bgcolor'] = '';
		$this->checkRequest('apply_bgcolor') != '' ? $calInfo['apply_bgcolor'] = "#" . $this->checkRequest('apply_bgcolor') : $calInfo['apply_bgcolor'] = '#5c5c5c';
		$this->checkRequest('direct_bgcolor') != '' ? $calInfo['direct_bgcolor'] = "#" . $this->checkRequest('direct_bgcolor') : $calInfo['direct_bgcolor'] = '';
		$this->checkRequest('view_bgcolor') != '' ? $calInfo['view_bgcolor'] = "#" . $this->checkRequest('view_bgcolor') : $calInfo['view_bgcolor'] = '';
		$this->checkRequest('current_bgcolor') != '' ? $calInfo['current_bgcolor'] = "#" . $this->checkRequest('current_bgcolor') : $calInfo['current_bgcolor'] = '';

		//======== Images ========//
		$this->checkRequest('arrow_image') != '' ? $calInfo['arrow_image'] =  $this->checkRequest('arrow_image') : $calInfo['arrow_image'] = '';
		$this->checkRequest('monyr_image') != '' ? $calInfo['monyr_image'] =  $this->checkRequest('monyr_image') : $calInfo['monyr_image'] = '';
		$this->checkRequest('wkname_image') != '' ? $calInfo['wkname_image'] =  $this->checkRequest('wkname_image') : $calInfo['wkname_image'] = '';
		$this->checkRequest('wkday_image') != '' ? $calInfo['wkday_image'] =  $this->checkRequest('wkday_image') : $calInfo['wkday_image'] = '';
		$this->checkRequest('selwkday_image') != '' ? $calInfo['selwkday_image'] =  $this->checkRequest('selwkday_image') : $calInfo['selwkday_image'] = '';
		$this->checkRequest('event_link_image') != '' ? $calInfo['event_link_image'] = $this->checkRequest('event_link_image') : $calInfo['event_link_image'] = '';

		//======== Borders ========//
		$this->checkRequest('arrow_border') != '' ? $calInfo['arrow_border'] = "#" . $this->checkRequest('arrow_border') : $calInfo['arrow_border'] = '';
		$this->checkRequest('monyr_border') != '' ? $calInfo['monyr_border'] = "#" . $this->checkRequest('monyr_border') : $calInfo['monyr_border'] = '';
		$this->checkRequest('wkname_border') != '' ? $calInfo['wkname_border'] = "#" . $this->checkRequest('wkname_border') : $calInfo['wkname_border'] = '';
		$this->checkRequest('wkday_border') != '' ? $calInfo['wkday_border'] = "#" . $this->checkRequest('wkday_border') : $calInfo['wkday_border'] = '';
		$this->checkRequest('selwkday_border') != '' ? $calInfo['selwkday_border'] = "#" . $this->checkRequest('selwkday_border') : $calInfo['selwkday_border'] = '';
		$this->checkRequest('eventlink_border') != '' ? $calInfo['eventlink_border'] = "#" . $this->checkRequest('eventlink_border') : $calInfo['eventlink_border'] = '';
		$this->checkRequest('apply_border') != '' ? $calInfo['apply_border'] = "#" . $this->checkRequest('apply_border') : $calInfo['apply_border'] = '';
		$this->checkRequest('direct_border') != '' ? $calInfo['direct_border'] = "#" . $this->checkRequest('direct_border') : $calInfo['direct_border'] = '';
		$this->checkRequest('view_border') != '' ? $calInfo['view_border'] = "#" . $this->checkRequest('view_border') : $calInfo['view_border'] = '';
		$this->checkRequest('current_border') != '' ? $calInfo['current_border'] = "#" . $this->checkRequest('current_border') : $calInfo['current_border'] = '';


		$this->checkRequest('show_list') != '' ? $calInfo['show_list'] = $this->checkRequest('show_list') : $calInfo['show_list'] = 0;
		$this->checkRequest('show_nextprev') != '' ? $calInfo['show_nextprev'] = $this->checkRequest('show_nextprev') : $calInfo['show_nextprev'] = 0;
		$this->checkRequest('show_views') != '' ? $calInfo['show_views'] = $this->checkRequest('show_views') : $calInfo['show_views'] = 0;
		$this->checkRequest('show_myapply') != '' ? $calInfo['show_myapply'] = $this->checkRequest('show_myapply') : $calInfo['show_myapply'] = 0;
		$this->checkRequest('start_wday') != '' ? $calInfo['start_wday'] = $this->checkRequest('start_wday') : $calInfo['start_wday'] = 0;
		
		
		$sql = "Select * from " . $this->themetable . " where cal_id = %d";
		$results = $wpdb->get_results($wpdb->prepare($sql, $calID));
		if (count($results) != 0 && $preview != "1") {
			$calInfo = array(); //reset array
			foreach ($results as $row) {
				$calInfo['calTitle'] = $row->cal_title;
				$calInfo['calDesc'] = $row->cal_description;
				$calInfo['calHook'] = $row->cal_hook;
				$calInfo['calStatus'] = $row->cal_status;

				//======== Fonts ========//
				$calInfo['arrows_font'] = $row->arrows_font;
				$calInfo['monyr_font'] = $row->monyr_font;
				$calInfo['wkname_font'] = $row->wkname_font;
				$calInfo['wkday_font'] = $row->wkday_font;
				$calInfo['selwkday_font'] = $row->selwkday_font;
				$calInfo['event_link_font'] = $row->event_link_font;
				$calInfo['apply_font'] = $row->apply_font;
				$calInfo['direct_font'] = $row->direct_font;
				$calInfo['view_font'] = $row->view_font;
				$calInfo['current_font'] = $row->current_font;

				//======== Font Sizes ========//
				$calInfo['arrows_fontsize'] = $row->arrows_fontsize;
				$calInfo['monyr_fontsize'] = $row->monyr_fontsize;
				$calInfo['wkname_fontsize'] = $row->wkname_fontsize;
				$calInfo['wkday_fontsize'] = $row->wkday_fontsize;
				$calInfo['selwkday_fontsize'] = $row->selwkday_fontsize;
				$calInfo['event_link_fontsize'] = $row->event_link_fontsize;
				$calInfo['apply_fontsize'] = $row->apply_fontsize;
				$calInfo['direct_fontsize'] = $row->direct_fontsize;
				$calInfo['view_fontsize'] = $row->view_fontsize;
				$calInfo['current_fontsize'] = $row->current_fontsize;

				//======== Colors ========//
				$calInfo['arrow_color'] = $row->arrow_color;
				$calInfo['monyr_color'] = $row->monyr_color;
				$calInfo['wkname_color'] = $row->wkname_color;
				$calInfo['wkday_color'] = $row->wkday_color;
				$calInfo['selwkday_color'] = $row->selwkday_color;
				$calInfo['event_link_color'] = $row->event_link_color;
				$calInfo['apply_color'] = $row->apply_color;
				$calInfo['direct_color'] = $row->direct_color;
				$calInfo['view_color'] = $row->view_color;
				$calInfo['current_color'] = $row->current_color;

				//======== Styles ========//
				$calInfo['arrows_style'] = $row->arrows_style;
				$calInfo['monyr_style'] = $row->monyr_style;
				$calInfo['wkname_style'] = $row->wkname_style;
				$calInfo['wkday_style'] = $row->wkday_style;
				$calInfo['selwkday_style'] = $row->selwkday_style;
				$calInfo['event_link_style'] = $row->event_link_style;
				$calInfo['apply_style'] = $row->apply_style;
				$calInfo['direct_style'] = $row->direct_style;
				$calInfo['view_style'] = $row->view_style;
				$calInfo['current_style'] = $row->current_style;

				//======== BG Colors ========//
				$calInfo['arrow_bgcolor'] = $row->arrow_bgcolor;
				$calInfo['monyr_bgcolor'] = $row->monyr_bgcolor;
				$calInfo['wkname_bgcolor'] = $row->wkname_bgcolor;
				$calInfo['wkday_bgcolor'] = $row->wkday_bgcolor;
				$calInfo['selwkday_bgcolor'] = $row->selwkday_bgcolor;
				$calInfo['event_link_bgcolor'] = $row->event_link_bgcolor;
				$calInfo['apply_bgcolor'] = $row->apply_bgcolor;
				$calInfo['direct_bgcolor'] = $row->direct_bgcolor;
				$calInfo['view_bgcolor'] = $row->view_bgcolor;
				$calInfo['current_bgcolor'] = $row->current_bgcolor;

				//======== Images========//
				$calInfo['arrow_image'] = $row->arrow_image;
				$calInfo['monyr_image'] = $row->monyr_image;
				$calInfo['wkname_image'] = $row->wkname_image;
				$calInfo['wkday_image'] = $row->wkday_image;
				$calInfo['selwkday_image'] = $row->selwkday_image;
				$calInfo['event_link_image'] = $row->event_link_image;

				//======== Borders ========//
				$calInfo['arrow_border'] = $row->arrow_border;
				$calInfo['monyr_border'] = $row->monyr_border;
				$calInfo['wkname_border'] = $row->wkname_border;
				$calInfo['wkday_border'] = $row->wkday_border;
				$calInfo['selwkday_border'] = $row->selwkday_border;
				$calInfo['eventlink_border'] = $row->eventlink_border;
				$calInfo['apply_border'] = $row->apply_border;
				$calInfo['direct_border'] = $row->direct_border;
				$calInfo['view_border'] = $row->view_border;
				$calInfo['current_border'] = $row->current_border;

				$calInfo['show_list'] = $row->show_list;
				$calInfo['show_nextprev'] = $row->show_nextprev;
				$calInfo['show_views'] = $row->show_views;
				$calInfo['show_myapply'] = $row->show_myapply;
				$calInfo['wp_pageID'] = $row->cal_wp_pageID;
				$calInfo['wp_categoryID'] = $row->cal_wp_categoryID;
				$calInfo['start_wday'] = $row->start_wday;
			}
		}
		
		return json_decode(json_encode($calInfo), FALSE);
	}

	public function cal_category($cal_title, $wp_cat_id, $cal_description) {
		global $wpdb;
		global $table_prefix;
		$type_slug = $this->generate_user_permalink($cal_title);
		//Add Event Calendar Category to post
		$wp_cat = array('cat_name' => $cal_title, 'category_nicename' => $type_slug, 'category_parent' => 0);
		
		$taxonomy_table = $table_prefix . "term_taxonomy";
		$terms_table = $table_prefix . "terms";
		if ($wp_cat_id > 0) {
			$categoryInfo = get_category_by_slug($type_slug);			
			$termID = $categoryInfo->term_id;
			$sqlUpdate = "Update $taxonomy_table set description = %s where term_id = %d";
			$wpdb->query($wpdb->prepare($sqlUpdate, $cal_description, $termID));
			$sqlUpdate = "Update $terms_table set name = %s, slug = %s where term_id = %d";
			$wpdb->query($wpdb->prepare($sqlUpdate, $cal_title, $type_slug, $termID));
		} else {
			$wp_cat_id = wp_insert_category($wp_cat);
		}
		return $wp_cat_id;
	}

	public function cal_process() {
		global $wpdb;
       $subprocess = $this->checkRequest('process');
		$calID = $this->checkRequest('calID');
       if ($subprocess == 'deletetheme') {
          //Delete all calendar entries in this theme
          $wpdb->query($wpdb->prepare("delete from " . $this->table . " where calendar_id = %d", $calID));
           //Delete calendar theme
          $wpdb->query($wpdb->prepare("delete from " . $this->themetable . " where cal_id = %d", $calID));
           echo "<p class='dbmessage'>The Calendar theme has been deleted successfully</p>";
       } else {
		$calID != "0" ? $process = "update" : $process = "add";
		$calID != "0" ? $processout = "updated" : $processout = "added";
		$wp_cat_id = $this->cal_category($_POST['form']['cal_title']['s'], $_POST['form']['cal_wp_categoryID']['i'], $_POST['form']['cal_description']['s']);
		if ($_POST['form']['cal_wp_categoryID']['i'] <= 0 ) {
			$_POST['form']['cal_wp_categoryID']['i'] = $wp_cat_id;
		} 
		$outcome = "The Calendar settings have been $processout successfully";
		$this->process_post($process, $_POST['form'], $this->themetable, $calID, $outcome, 'cal_id');
		$process == "add" ? $calID = $this->lastID($this->themetable, 'cal_id') : '';
		$calHook = "[kybcalendar cal_id=$calID]";
		$wpdb->query($wpdb->prepare("Update " . $this->themetable . " set cal_hook = %s where cal_id = %d", $calHook, $calID));
}

		
	}

	public function get_featured_cal() {
		global $wpdb;
		$featuredCal = "";
		$webdir = get_template_directory();
		$today = date('Y');
		$sql = "Select * from " . $this->table . " where featured = 1 and status = 1 and dt > %s limit 1";
		$row = $wpdb->get_row($wpdb->prepare($sql, $today));
		if (count($row) != 0) {
			$id = $row->id;
			$month = strtoupper($this->month_name($row->month));
			$day = $row->day;
			$lookup = $row->year . "-" . $row->month . "-" . $row->day;
			$row->calendar_id != 0 ? $calID = $row->calendar_id : $calID = 1;
			$calWP = $wpdb->get_var($wpdb->prepare("Select cal_wp_pageID from " . $this->themetable . " where cal_id = %d", $calID));
			$calURL = get_permalink($calWP);
			$eventLink = "$calURL/?viewevent=$id";
			$eventListLink = "$calURL/?process=viewdaylist&lookup=$lookup";
			$content = array(
				'EVENTTITLE' => $row->title,
				'EVENTDESC' => $row->featured_txt,
				'EVENTLINK' => $eventLink,
				'EVENTMONTH' => $month,
				'EVENTLISTLINK' => $eventListLink,
				'EVENTDAY' => "$day"
			);
			$template = $webdir . "/view/html/event_block.html";
			$featuredCal = $this->showPage ($template, $content, true);		
		}
		return $featuredCal;
	}

	public function fillCal($wp_catID, $calID) {
		$args = array(
			'posts_per_page' => 50,
			'category'         => $wp_catID,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'post',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'post_status'      => 'publish',
			'suppress_filters' => true 
		);
		$currevents = get_posts( $args );
		foreach ($currevents as $row) {
			$post_id = $row->ID;
			$post_date = $row->post_date;
			$post_title = $row->post_title;
			$post_content = $this->cleanHTML($row->post_content);
			$dt = $this->ReformatShortDateToDB($post_date);
			$month = date("m",strtotime($post_date));
			$day = date("d",strtotime($post_date));
			$year = date("Y",strtotime($post_date));

			$content = array(
				'calendar_id' => $calID,
				'wp_postID' => $post_id,
				'calendar_themes' => "$calID",
				'dt' => $dt,
				'event_start' => $post_date,
				'event_end' => $post_date,
				'event_timezone' => 'America/Chicago',
				'title' => $post_title,
				'description' => $post_content,
				'month' => $month,
				'day' => $day,
				'year' => $year,
				'status' => 1
			);
			$outcome = "The Calendar has been filled with event posts";
			//$this->processForm('add', $content, $this->table, $calID, '', 0);
		}
		
	}
}
?>