<?php
//This is the most important coding.
header("Content-Type: text/Calendar");
header("Content-Disposition: inline; filename=event_calendar.ics");

//======== Get required files ========//	
$basedir = $_SERVER['DOCUMENT_ROOT']; //change when put on client url
require_once("$basedir/wp-config.php");
$dir = WP_PLUGIN_DIR;
require_once("$dir/kybcalendar/model/shared.php"); // shared classes
require_once("$dir/kybcalendar/model/smarty.php"); // smarty engine
$timezone = date('e');
date_default_timezone_set('America/Chicago');

//======== Initiate class ========//
$cls = new calendarsharedProcesses ();

$eventID = $cls->checkRequest('eventID');
$wp_id = $cls->checkRequest('wpID');
$sql = "Select * from kyb_calendar where id = %d";
$result = $wpdb->get_results($wpdb->prepare($sql, $eventID));
	if(count($result) != 0) {
		foreach($result as $row) {
			$event_description = strip_tags($row->description, '<br><b></b><strong></strong><a></a>');
			$description = strip_tags($row->description);
			$eventID = $row->id;
			$eventDate = $cls->date_to_str($row->dt);
			$event_title = $row->title;
			$address = strip_tags($row->event_address);
			$city = strip_tags($row->event_city);
			$state = strip_tags($row->event_state);
			$zip = strip_tags($row->event_zip);
			$jdstartDate = $cls->date_to_jd($row->event_start);
			$jdendDate = $cls->date_to_jd($row->event_end);
			
			//Get Event Start Information
			$startHour = $row->start_hour;
			$startMin = $row->start_minute;
			$startPart = $row->start_day;
			$startyear = $row->year;
			$startmonth = $row->month;
			$startday = $row->day;
			$timezone = $row->event_timezone;
			$tz_offset = $wpdb->get_var($wpdb->prepare("Select tz_offset from kyb_calendar_timezones where tz_id = %s", $timezone));
			$tz_offset_neg = bcsub($tz_offset,1);
			$offset1 = "-0{$tz_offset}00";
			$offset2 = "-0{$tz_offset_neg}00";
			
			strlen($startHour) < 2 ? $startHour = "0" . $startHour : '';
			strlen($startMin) < 2 ? $startMin = "0" . $startMin : '';
			strlen($startmonth) < 2 ? $startmonth = "0" . $startmonth : '';
			strlen($startday) < 2 ? $startday = "0" . $startday : '';

			$eventstart = $startmonth . "/" . $startday . "/" . $startyear . " " . $startHour . ":" .$startMin . " " . $startPart; 

			
			//Get Event End Information
			if ($row->recurring) {
				$recurStartDate = $row->recurring_startdate;
				$sqlRecur = "Select * from kyb_calendar where recurring_startdate = %s order by dt desc limit 1"; 
				$resultRecur = $wpdb->get_results($wpdb->prepare($sqlRecur,$recurStartDate));
				if (count($resultRecur) != 0) {
					foreach ($resultRecur as $rec) {
						$dt = $rec->dt;
						$endmonth = $rec->month;
						$end_day = $rec->day;
						$endyear = $rec->year;
						$endhour = $rec->end_hour;
						$endmin = $rec->end_minute;
						$endPart = $rec->end_day;
					}
					strlen($endmonth) < 2 ? $endmonth = "0" . $endmonth : '';
					strlen($end_day) < 2 ? $end_day = "0" . $end_day : '';
					strlen($endhour) < 2 ? $endhour = "0" . $endhour : '';
					strlen($endmin) < 2 ? $endmin = "0" . $endmin : '';
				}
				
			} else {
				$endmonth = $startmonth;
				$end_day = $startday;
				$endyear = $startyear;
				$endhour = $row->end_hour;
				$endmin = $row->end_minute;
				$endPart = $row->end_day;

				strlen($endmonth) < 2 ? $endmonth = "0" . $endmonth : '';
				strlen($end_day) < 2 ? $end_day = "0" . $end_day : '';
				strlen($endhour) < 2 ? $endhour = "0" . $endhour : '';
				strlen($endmin) < 2 ? $endmin = "0" . $endmin : '';
			}

			$eventend = $endmonth . "/" . $end_day . "/" . $endyear . " " . $endhour . ":" .$endmin . " " . $endPart;
			
			$startDate = date('Ymd', strtotime($eventstart)) . "T" . date('His', strtotime($eventstart)) . "Z";
			$endDate = date('Ymd', strtotime($eventend)) . "T" . date('His', strtotime($eventend)) . "Z";
			$companyName = $cls->company;
			$company = $cls->company;
			//$companyEmail = $cls->admin_email;
			$companyName = str_replace(" ", "+", $companyName);
			$companyEvent = "{$companyName}+Event:";
			//Get Post Information
			$postInfo = get_post($wp_id);
			$authorID = $postInfo->post_author;
			$companyEmail = get_the_author_meta('user_email', $authorID );
			$url = get_permalink($wp_id);
		}
	}

echo "BEGIN:VCALENDAR\n";
echo "VERSION:2.0\n";
echo "PRODID:-//$company//$company Events v1.0//EN\n";
echo "CALSCALE:GREGORIAN\n";
echo "METHOD:PUBLISH\n";
echo "X-WR-CALNAME:$company Events\n";
echo "X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n";
echo "BEGIN:VTIMEZONE\n";
echo "TZID:$timezone\n";
echo "X-LIC-LOCATION:$timezone\n";
echo "BEGIN:DAYLIGHT\n";
echo "TZOFFSETFROM:$offset1\n";
echo "TZOFFSETTO:$offset2\n";
echo "TZNAME:EDT\n";
echo "DTSTART:$startDate\n";
echo "RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU\n";
echo "END:DAYLIGHT\n";
echo "BEGIN:STANDARD\n";
echo "TZOFFSETFROM:$offset2\n";
echo "TZOFFSETTO:$offset1\n";
echo "TZNAME:EDT\n";
echo "DTSTART:$startDate\n";
echo "RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU\n";
echo "END:STANDARD\n";
echo "END:VTIMEZONE\n";
echo "BEGIN:VEVENT\n";
echo "DTSTAMP:$startDate\n";
echo "DTSTART;TZID=$timezone:$startDate\n";
echo "DTEND;TZID=$timezone:$endDate\n";
echo "SUMMARY:$event_title\n";
echo "DESCRIPTION:$event_title\\n$address\\n$city, $state $zip\\nEvent Page: $url\n";
echo "ORGANIZER;CN=$company:MAILTO:$companyEmail\n";
echo "CLASS:PUBLIC\n";
echo "CREATED:$startDate\n";
echo "LOCATION:$address $city $state $zip\n";
echo "URL:$url\n";
echo "SEQUENCE:1\n";
echo "LAST-MODIFIED:$startDate\n";
echo "UID:$companyEmail\n";
echo "END:VEVENT\n";
echo "END:VCALENDAR\n";


?>
