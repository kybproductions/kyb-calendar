<?php
ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('log_errors',TRUE);
ini_set('display_errors',FALSE);
ini_set("error_log", WP_PLUGIN_DIR . "/kybcalendar/error_log.log");
class PDF extends FPDF
{

	var $widths;
	var $aligns;

//Page header
	function Header()
	{
		$basedir = $_SERVER['DOCUMENT_ROOT'];
		$startDate = date("Y-m-d", strtotime($_REQUEST['startdate']));
		$endDate = date("Y-m-d", strtotime($_REQUEST['enddate']));
		$startDate != $endDate ? $runDates = $this->ReformatDatedb($startDate) . " - " . $this->ReformatDatedb($endDate) : $runDates = $this->ReformatDatedb($startDate);

		//Logo
		/*$this->Image("$basedir/wp-content/themes/trinityriver/images/logo.jpg",123,8,50);
		$this->SetFont('Arial','IB',12);
		$this->Cell(0,20,'',0,1,'C');
		$this->Cell(0,5,'River Trip Schedule',0,1,'C');
		$this->SetFont('Arial','B',12);
		$this->Cell(0,5,$runDates,0,1,'C');
		$this->Ln(10);*/
		//Arial bold 15
		//$this->SetFont('Arial','B',15);
		//Move to the right
		//$this->Cell(80);
		//Title
		//$this->Cell(30,10,'Title',1,0,'C');
		//Line break
		//$this->Ln(20);
	}

	//========= Date and Time functions ========//
	function ReformatDatedb($currDate){
		if (strlen($currDate) > 10) {
			$currDate = substr($currDate, 0, strlen($currDate) - 9);
		}
		$createdate = explode('-', $currDate);
		$newDate = $createdate[1] . "/" . $createdate[2] . "/" . $createdate[0];
		$newDate == "//" ? $newDate = '' : '';
		return $newDate;
	}


//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Page number
		$this->Cell(0,10,'� TRINITY RIVER KAYAK CO.'.$this->PageNo().'/{nb}',0,0,'C');
	}


	function LineItems($title, $alpha, $measure) {

		$textWrap= $title;
		$textNoWrap = "there will be no wrapping here thank you";
	
		$data = array();
		foreach ($title as $key=>$value) {		
			$data[] = array($value, 1, 50, 50, 0, 50);
		}
					
		/* Layout */
		
		$this->SetDataFont();
		//$this->AddPage();

		// Headers and widths
		
		$w = array(140, 45, 5, 20, 20, 20);

		/*for($i = 0; $i < count($header); $i++) {
			$this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
		}*/

		$this->Ln();

		// Mark start coords

		$x = $this->GetX();
		$y = $this->GetY();
		$i = 0;
		
		foreach($data as $row)
		{	
			$y1 = $this->GetY();
			$this->Cell($w[2], 6, $alpha[$i] . ".", 0, 'L');
			$this->MultiCell($w[0], 6, $row[0], 0,'L');	
			$y2 = $this->GetY();
			$yH = $y2 - $y1;
						
			$this->SetXY($x + $w[0], $this->GetY() - $yH);			
			$this->MultiCell($w[1], $yH, $measure[$i], 0, 'L');
			//$this->Ln();
			
			$i++;
		}
		
		//$this->Ln(10);

		$this->setTextFont();
		//$this->Cell($w[0] + $w[1] + $w[3], 6, 'Total', 'TB', 0, 'L');
		$this->setDataFont(true);
		//$this->Cell($w[2], 6, number_format(2, 2), 'TB', 0, 'R');
		//$this->Cell($w[2], 6, number_format(2, 2), 'TB', 0, 'R');
		//$this->Cell($w[2], 6, number_format(2, 2), 'TB', 0, 'R');
		//$this->Ln();

		$this->setTextFont();

		//$this->Write(10, "Notes: " . "Thank you for your business.");
		//$this->Ln(10);	
	}

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$this->MultiCell($w, 5, $data[$i], 0, $a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w, $txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r", '', $txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}

	private function setTextFont($isBold = false) {
		$this->SetFont('Arial', $isBold ? 'B' : '', 9);
	}
	
	private function setDataFont($isBold = false) {
		$this->SetFont('Times', $isBold ? 'B' : '', 11);
	}

	private function getAddressLength($address) {
		return count(explode("\n", $address));
	}
		
	private function writeAddress($address) {
		$lines = explode("\n", $address);
		foreach ($lines as $line) {
			$this->Cell($this->PG_W, 5, $line, 0, 0, 'C');
			$this->Ln(4);
		}
	}	
}




function GenerateWord()
{
    //Get a random word
    $nb=rand(3, 10);
    $w='';
    for($i=1;$i<=$nb;$i++)
        $w.=chr(rand(ord('a'), ord('z')));
    return $w;
}

function GenerateSentence()
{
    //Get a random sentence
    $nb=rand(1, 10);
    $s='';
    for($i=1;$i<=$nb;$i++)
        $s.=GenerateWord().' ';
    return substr($s, 0, -1);
}
?>