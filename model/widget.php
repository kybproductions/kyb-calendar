<?php
//KYB Calendar Side Bar Widget Class
class kybcalendar_widget extends WP_Widget {

	function __construct() {
		parent::__construct('kybcalendar_widget', __('KYB Calendar Widget', 'wpb_widget_domain'), array( 'description' => __( 'KYB Calendar Side Bar Widget', 'wpb_widget_domain' ), ) 
		);
	}

	public function widget( $args, $instance ) {
		require_once(KYBCAL_ABSPATH . "/model/shared.php"); // shared class
		require_once(KYBCAL_ABSPATH . "/model/smarty.php"); // smarty engine
		include_once(KYBCAL_ABSPATH . "/controller/controller.php"); 
		$controller = new CalendarController();
		$content = $controller->viewSideCal();
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output		
		echo $content;
		echo $args['after_widget'];
	}
		
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
	<?php 
	}
	
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} 
?>