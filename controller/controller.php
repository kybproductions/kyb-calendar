<?php
/*******************************************************************************
* CALENDAR CONTROLLER														   *
*                                                                              *
* Version: 1.0                                                                 *
* Date:    09-03-2012                                                          *
* Author:  Kimla Y. Beasley													   *
* Copyright 2012 KYB PRODUCTIONS											   *
*******************************************************************************/


interface calendarfunctions {	
	public function update_calendar($process);
	public function processToBlog($calID, $ID);
    public function process_repeat($post_array, $ID);
    public function process_customdates($post_array, $ID);
    public function process_mode($post_array, $ID, $modThis, $process);
	public function viewCalendar($calID, $index);
	public function getCalView($calID, $yr, $viewtype, $url, $currview, $arrow_bgcolor, $wp_pageID);	
	public function getListings($m, $y, $no_of_days, $calID);
	public function calForm($calID, $action, $area);
	public function monthoptions($r, $m, $yn);
	public function calDays($no_of_days, $r, $yn, $j, $calID);
	public function calSidebarDays($no_of_days, $r, $yn, $j);
	public function calViewDays($no_of_days, $r, $yn, $j, $wp_pageID, $calID, $showadj, $index);
	public function viewSidebarCal($showcal, $calcls);
	public function calScript($no_of_days, $r, $yn);
	public function viewcalList($datelookup);
	public function viewannouncements($datelookup);
	public function viewEvents ($eventID, $lookup, $calID, $viewfull);
	public function cal_settings($calID);
	public function get_calInfo($calID);
	public function cal_process();
	public function get_featured_cal();
	public function fillCal($wp_catID, $calID);
}

class CalendarController { 
	public $pluginDir;
	public $mod;
	var $eventsUrl;
	var $currURL;
	
	 public function __construct()  
     { 
		$this->pluginDir =  KYBCAL_ABSPATH;
		require_once($this->pluginDir . '/model/model.php');
		$this->mod = new calendarclass;
		$this->eventsUrl = get_permalink(11);
		$this->currURL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$urlInfo = parse_url($this->currURL, PHP_URL_PATH);
		$this->urlInfo = explode("/", $urlInfo);
		date_default_timezone_set(date_default_timezone_get());
	 }
         
     public function invoke($action)  
     { 
		$pagename = $this->checkRequest('page');
		$content = "";
		$management = "admin";
		$mgmt_title = "Calendar Management";
		$showheader = true;
		$action == "" ? $action = $this->checkRequest('process') : '';
		$content = $this->calendar_processes($action);
		is_array($content) ? $html = $content['HTML'] : $html = '';
		include_once($this->pluginDir . "/view/output.php");
     }  

	public function calendar_processes($action) {
		global $wpdb;
       $defaultCalID = $wpdb->get_var("Select cal_id from " . KYBCAL_TABLE . "kyb_calendar_themes where cal_status = 1 limit 1");
		switch ($action) {
			case "update":
				$calID = $this->checkRequest('ID');
				$content = $this->mod->calForm($calID, $action, 'admin');
			break;
			case "updatesubmit";			
				$this->checkRequest('calID') == "" ? $calID = $defaultCalID : $calID = $this->checkRequest('calID');
				$this->mod->update_calendar('update');				$content = $this->mod->viewCalendar($calID, true);
			break;
			case "addsubmit";
				$this->checkRequest('calID') == "" ? $calID = $defaultCalID : $calID = $this->checkRequest('calID');
				$this->mod->update_calendar('add');
				$content = $this->mod->viewCalendar($calID, true);
			break;
			case "announcements":
				$content = $this->mod->viewannouncements('');
			break;
			case "viewday":
				$lookup = $this->mod->checkRequest('lookup');
				$content = $this->mod->viewannouncements($lookup);
			break;
            case "viewdaylist":
				$lookup = $this->urlInfo[3];	
				$content = $this->mod->viewEvents ('', $lookup);
			break;
            case "viewevent":				
				$lookup = "";	
				$eventID = $this->urlInfo[3];
				$content = $this->mod->viewEvents ($eventID, $lookup);
			break;
			case "addnew":				
				$calID = $this->checkRequest('ID');
				if ($calID == "") { $calID = 0; }
				$content = $this->mod->calForm($calID, $action, 'admin');
			break;
			case "delete" :
		       $ID = $this->checkRequest('ID');
		       $modThis = $this->checkRequest('mod');
				$this->mod->process_mode(array(), $ID, $modThis, 'delete');

				$this->checkRequest('calID') == "" ? $calID = $defaultCalID : $calID = $this->checkRequest('calID');
				$content = $this->mod->viewCalendar($calID, true);
			break;
            case "eventlist":			
				$eventID = "";
				$lookup = "";
				$calID = $this->checkRequest('calID');
				if (count($this->urlInfo) == 6) {
					$eventID = $this->urlInfo[4];
				}
				if ($eventID != "" && $eventID != "viewdaylist") {
					$content = $this->mod->viewEvents ($eventID, $lookup);
				} else {
					$content = $this->mod->viewCalendar($calID, true);
				}
			break;
			case"settings":			
				$calID = $this->checkRequest('calID');
				$this->checkRequest('process') == 'calsubmit' ? $this->mod->cal_process() : '';
				$content = $this->mod->cal_settings($calID);
			break;
			case "manage":
				$calID = $this->checkRequest('calID');
				$content = $this->mod->viewCalendar($calID, true);
			break;
			
			default:
				$calID = $this->checkRequest('calID');
				$this->checkRequest('process') == 'calsubmit' || $this->checkRequest('process') == 'deletetheme' ? $this->mod->cal_process() : '';
				$content = $this->mod->cal_settings($calID);
				/*$this->checkRequest('calID') == "" ? $calID = $defaultCalID : $calID = $this->checkRequest('calID');
				$content = $this->mod->viewCalendar($calID, true);*/
			break;
		}
		return $content;
	}

	public function calajax($action) {
		switch ($action) {
			case "changecal":
				$calID = $this->checkRequest('calID');
				$calendarcontent = $this->mod->viewCalendar($calID, false);
				$calTemplate = KYBCAL_ABSPATH . "/view/html/" . $calendarcontent['HTML'];
				$calendarview = $this->mod->showPage ($calTemplate, $calendarcontent, true);
				echo $calendarview;
			break;
			case "viewcal":				
				$prm = $this->mod->checkRequest('prm'); 
echo $prm;
				$calID = $this->mod->checkRequest('calID');
				$row = $this->mod->get_calInfo($calID);	
				if(isset($prm) && $prm > 0){
					$m= $prm;
					$r=(int)date('m',mktime(0,0,0,$m,1,$y));
				} else {
					$m= date("m");
					$r=date("m");
				}
				$this->mod->checkRequest('y') != "" ? $y = $this->mod->checkRequest('y') : $y= date("Y");     // Finds today's or selected year
				$no_of_days = date('t',mktime(0,0,0,$m,1,$y)); // This is to calculate number of days in a month
				$mn=date('M',mktime(0,0,0,$m,1,$y)); // Month is calculated to display at the top of the calendar
				$yn=date('Y',mktime(0,0,0,$m,1,$y)); // Year is calculated to display at the top of the calendar
				$j= date('w',mktime(0,0,0,$m,1,$y)); // This will calculate the week day of the first day of the month
				$calDays = $this->mod->BeginTable('100%', "0", "0", "0","400");
				$calDays .= $this->mod->calViewDays($no_of_days, $r, $yn, $j, $row->wp_pageID, $calID);
				$calDays .= $this->mod->EndTable();

				echo $calDays;
			break;
		}
	}

	public function cleanString($string) {
		$string = preg_replace("@<script[^>]*>.+</script[^>]*>@i", "", $string); 
		$cleaned = trim(strip_tags(stripslashes($string)));
		$cleaned = str_replace("'","''",$string);
		$cleaned = str_replace("''","&quot;",$string);
		return $cleaned;
	}

	public function checkRequest($var) {
		$value = "";
		 if (isset($_POST[$var]) || isset($_GET[$var])) {
			$_POST[$var] != "" ? $value = $this->cleanString($_POST[$var]) : $value = $this->cleanString($_GET[$var]);
		 }
		 $value = $this->cleanString($value);
		 return $value;		 
	}

	public function viewEvents($process, $cal_id, $eventID) {
		$output = "";
		$lookup = "";
		$viewlist = false;
		$viewall = false;
		/*if ($process == "viewevent") {
			$stripped = str_replace($this->eventsUrl,"", $this->currURL);
			$stripURL = explode("/", $stripped);
			$eventID = $stripURL[1];
		}*/
		if (isset($_REQUEST['viewevent'])) {
			$process = "viewevent";
			$eventID = $this->checkRequest('viewevent');
		}

		if (isset($_REQUEST['viewdaylist'])) {
			$process = "viewevent";
			$lookup = $this->checkRequest('lookup');
		}
		
		switch ($process) {
			case "viewdaylist":
				$stripped = str_replace($this->eventsUrl,"", $this->currURL);
				$stripURL = explode("/", $stripped);
				$this->checkRequest('lookup') != "" ? $lookup = $this->checkRequest('lookup') : $lookup = $stripURL[1];
			break;

			case "viewlistings":
				$viewlist = true;
				$prm = $this->checkRequest('prm'); 
				$chm = $this->checkRequest('chm'); 
				if(isset($prm) && $prm > 0){
					$m= bcadd($prm,$chm);
					$r=(int)date('m',mktime(0,0,0,$m,1,$y));
				} else {
					$m= date("m");
					$r=date("m");
				}
				$d= date("d");     // Finds today's date
				$this->checkRequest('y') != "" ? $y = $this->checkRequest('y') : $y= date("Y");     // Finds today's or selected year
				$no_of_days = date('t',mktime(0,0,0,$m,1,$y)); // This is to calculate number of days in a month
			break;
			case "viewall" :
				$viewall = true;
			break;
		}
		
		
		if ($eventID != "" && $eventID != "viewdaylist") {
			$content = $this->mod->viewEvents ($eventID, $lookup, $cal_id, true);
		} else if ($lookup != "") {
			$content = $this->mod->viewEvents ($eventID, $lookup, $cal_id, true);
		} else if ($viewlist) {
			$content = $this->mod->getListings($m, $y, $no_of_days);
		} else if ($viewall) {
			$content = $this->mod->viewEvents ($eventID, $lookup, $cal_id, true);
		} else {
		
			$content = $this->mod->viewCalendar($cal_id, false);
		}		
		is_array($content) ? $html = $content['HTML'] : $html = '';
		
		if ($html != "") {
			$template =  KYBCAL_ABSPATH . "/view/html/$html";	
			$page = new CalendarSmarty ($template);
			$page->replace_tags($content);
			$output = $page->viewoutput();
		}
		return $output;
	}

	public function viewSideCal() {
		$content = $this->mod->viewSidebarCal(true, '');
		return $content;
	}

	public function viewFeaturedCal() {
		$content = $this->mod->get_featured_cal();
		return $content;
	}
}
?>