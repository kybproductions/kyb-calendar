-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: 10.6.175.44
-- Generation Time: Feb 28, 2014 at 09:14 AM
-- Server version: 5.0.96
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `kybcalendar`
--

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `kybcalendar_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fonts` text NOT NULL,
  `font_sizes` text NOT NULL,
  `week_days` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='holds calendar configurations' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kybcalendar_config`
--

INSERT INTO `kybcalendar_config` (`id`, `fonts`, `font_sizes`, `week_days`) VALUES
(1, '{"fonts":{"Arial":{"google":""},"Verdana":{"google":""},"Tahoma":{"google":""},"Times New Roman":{"google":""},"Open Sans":{"google":"https://fonts.googleapis.com/css?family=Open+Sans"},"Lato":{"google":"https://fonts.googleapis.com/css?family=Lato"}}}', '10px,11px,12px,13px,14px,16px,18px,20px,24px,28px,30px', '{"days":{"Monday":"0","Tuesday":"6","Wednesday":"5","Thursday":"4","Friday":"3","Saturday":"2", "Sunday": "1"}}');


--
-- Table structure for table `kyb_calendar`
--

CREATE TABLE IF NOT EXISTS `kyb_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calendar_id` int(11) NOT NULL DEFAULT '0',
  `wp_postID` int(11) NOT NULL DEFAULT '0',
  `calendar_themes` varchar(200) DEFAULT NULL,
  `dt` date NOT NULL DEFAULT '0000-00-00',
  `start_time` varchar(50) NOT NULL DEFAULT '',
  `start_hour` varchar(20) NOT NULL DEFAULT '',
  `start_minute` varchar(20) NOT NULL DEFAULT '',
  `start_day` varchar(20) NOT NULL DEFAULT '',
  `end_hour` varchar(20) NOT NULL DEFAULT '',
  `end_minute` varchar(20) NOT NULL DEFAULT '',
  `end_day` varchar(20) NOT NULL DEFAULT '',
  `end_time` varchar(50) NOT NULL DEFAULT '',
  `event_timezone` varchar(100) DEFAULT NULL,
  `event_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `event_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `event_rsvp` tinyint(4) NOT NULL DEFAULT '0',
  `rsvp_form` int(11) NOT NULL DEFAULT '0',
  `rsvp_link` varchar(200) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `tooltip` varchar(50) DEFAULT NULL,
  `description` text,
  `cal_link` varchar(255) DEFAULT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `featured_txt` text,
  `featured_image` text,
  `target` tinyint(4) DEFAULT NULL,
  `month` int(11) NOT NULL DEFAULT '0',
  `day` tinyint(4) NOT NULL DEFAULT '0',
  `year` int(11) NOT NULL DEFAULT '0',
  `recurring` tinyint(4) NOT NULL DEFAULT '0',
  `recurring_type` varchar(50) DEFAULT NULL,
  `recurring_period` int(11) NOT NULL DEFAULT '0',
  `recurring_startdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `event_address` varchar(200) DEFAULT NULL,
  `event_city` varchar(100) DEFAULT NULL,
  `event_state` varchar(20) DEFAULT NULL,
  `event_zip` varchar(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `calendartime` time DEFAULT NULL,
  `calendarendtime` time DEFAULT NULL,
  `order_num` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `signature_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `calendar_id` (`calendar_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kyb_calendar`
--


-- --------------------------------------------------------

--
-- Table structure for table `kyb_calendar_themes`
--

CREATE TABLE IF NOT EXISTS `kyb_calendar_themes` (
  `cal_id` int(11) NOT NULL AUTO_INCREMENT,
  `cal_title` varchar(255) NOT NULL,
  `cal_description` text,
  `cal_hook` varchar(100) NOT NULL,
  `arrows_font` varchar(10) DEFAULT NULL,
  `monyr_font` varchar(10) DEFAULT NULL,
  `wkname_font` varchar(10) DEFAULT NULL,
  `wkday_font` varchar(10) DEFAULT NULL,
  `selwkday_font` varchar(10) DEFAULT NULL,
  `event_link_font` varchar(10) DEFAULT NULL,
  `apply_font` varchar(10) DEFAULT NULL,
  `direct_font` varchar(10) DEFAULT NULL,
  `view_font` varchar(10) DEFAULT NULL,
  `current_font` varchar(10) DEFAULT NULL,
  `arrows_fontsize` varchar(10) DEFAULT NULL,
  `monyr_fontsize` varchar(10) DEFAULT NULL,
  `wkname_fontsize` varchar(10) DEFAULT NULL,
  `wkday_fontsize` varchar(10) DEFAULT NULL,
  `selwkday_fontsize` varchar(10) DEFAULT NULL,
  `event_link_fontsize` varchar(10) DEFAULT NULL,
  `apply_fontsize` varchar(10) DEFAULT NULL,
  `direct_fontsize` varchar(10) DEFAULT NULL,
  `view_fontsize` varchar(10) DEFAULT NULL,
  `current_fontsize` varchar(10) DEFAULT NULL,
  `arrow_color` varchar(10) DEFAULT NULL,
  `monyr_color` varchar(10) DEFAULT NULL,
  `wkname_color` varchar(10) DEFAULT NULL,
  `wkday_color` varchar(10) DEFAULT NULL,
  `selwkday_color` varchar(10) DEFAULT NULL,
  `event_link_color` varchar(10) DEFAULT NULL,
  `apply_color` varchar(10) DEFAULT NULL,
  `direct_color` varchar(10) DEFAULT NULL,
  `view_color` varchar(10) DEFAULT NULL,
  `current_color` varchar(10) DEFAULT NULL,
  `arrows_style` varchar(10) DEFAULT NULL,
  `monyr_style` varchar(10) DEFAULT NULL,
  `wkname_style` varchar(10) DEFAULT NULL,
  `wkday_style` varchar(10) DEFAULT NULL,
  `selwkday_style` varchar(10) DEFAULT NULL,
  `event_link_style` varchar(10) DEFAULT NULL,
  `apply_style` varchar(10) DEFAULT NULL,
  `direct_style` varchar(10) DEFAULT NULL,
  `view_style` varchar(10) DEFAULT NULL,
  `current_style` varchar(10) DEFAULT NULL,
  `arrow_bgcolor` varchar(10) DEFAULT NULL,
  `monyr_bgcolor` varchar(10) DEFAULT NULL,
  `wkname_bgcolor` varchar(10) DEFAULT NULL,
  `wkday_bgcolor` varchar(10) DEFAULT NULL,
  `selwkday_bgcolor` varchar(10) DEFAULT NULL,
  `event_link_bgcolor` varchar(10) DEFAULT NULL,
  `apply_bgcolor` varchar(10) DEFAULT NULL,
  `direct_bgcolor` varchar(10) DEFAULT NULL,
  `view_bgcolor` varchar(10) DEFAULT NULL,
  `current_bgcolor` varchar(10) DEFAULT NULL,
  `arrow_image` varchar(150) DEFAULT NULL,
  `monyr_image` varchar(150) DEFAULT NULL,
  `wkname_image` varchar(150) DEFAULT NULL,
  `wkday_image` varchar(150) DEFAULT NULL,
  `selwkday_image` varchar(150) DEFAULT NULL,
  `event_link_image` varchar(150) DEFAULT NULL,
  `apply_image` varchar(255) DEFAULT NULL,
  `direct_image` varchar(255) DEFAULT NULL,
  `view_image` varchar(255) DEFAULT NULL,
  `current_image` varchar(255) DEFAULT NULL,
  `arrow_border` varchar(20) DEFAULT NULL,
  `monyr_border` varchar(20) DEFAULT NULL,
  `wkname_border` varchar(20) DEFAULT NULL,
  `wkday_border` varchar(20) DEFAULT NULL,
  `selwkday_border` varchar(20) DEFAULT NULL,
  `eventlink_border` varchar(20) DEFAULT NULL,
  `apply_border` varchar(10) DEFAULT NULL,
  `direct_border` varchar(10) DEFAULT NULL,
  `view_border` varchar(10) DEFAULT NULL,
  `current_border` varchar(10) DEFAULT NULL,
  `show_list` tinyint(4) NOT NULL DEFAULT '0',
  `show_nextprev` tinyint(4) NOT NULL DEFAULT '1',
  `show_views` tinyint(4) NOT NULL DEFAULT '1',
  `show_myapply` tinyint(4) NOT NULL DEFAULT '1',
  `cal_wp_pageID` int(11) NOT NULL DEFAULT '0',
  `cal_wp_categoryID` int(11) NOT NULL DEFAULT '0',
  `start_wday` int(11) NOT NULL DEFAULT '1',
  `cal_status` tinyint(4) NOT NULL DEFAULT '1',
  `theme_tzone` varchar(100) NOT NULL,
  PRIMARY KEY (`cal_id`),
  KEY `cal_id` (`cal_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Holds information for calendar themes' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kyb_calendar_themes`
--

INSERT INTO `kyb_calendar_themes` (`cal_id`, `cal_title`, `cal_description`, `cal_hook`, `arrows_font`, `monyr_font`, `wkname_font`, `wkday_font`, `selwkday_font`, `event_link_font`, `apply_font`, `direct_font`, `view_font`, `current_font`, `arrows_fontsize`, `monyr_fontsize`, `wkname_fontsize`, `wkday_fontsize`, `selwkday_fontsize`, `event_link_fontsize`, `apply_fontsize`, `direct_fontsize`, `view_fontsize`, `current_fontsize`, `arrow_color`, `monyr_color`, `wkname_color`, `wkday_color`, `selwkday_color`, `event_link_color`, `apply_color`, `direct_color`, `view_color`, `current_color`, `arrows_style`, `monyr_style`, `wkname_style`, `wkday_style`, `selwkday_style`, `event_link_style`, `apply_style`, `direct_style`, `view_style`, `current_style`, `arrow_bgcolor`, `monyr_bgcolor`, `wkname_bgcolor`, `wkday_bgcolor`, `selwkday_bgcolor`, `event_link_bgcolor`, `apply_bgcolor`, `direct_bgcolor`, `view_bgcolor`, `current_bgcolor`, `arrow_image`, `monyr_image`, `wkname_image`, `wkday_image`, `selwkday_image`, `event_link_image`, `apply_image`, `direct_image`, `view_image`, `current_image`, `arrow_border`, `monyr_border`, `wkname_border`, `wkday_border`, `selwkday_border`, `eventlink_border`, `apply_border`, `direct_border`, `view_border`, `current_border`, `show_list`, `show_nextprev`, `show_views`, `show_myapply`, `cal_wp_pageID`, `cal_wp_categoryID`, `start_wday`, `cal_status`, `theme_tzone`) VALUES
(1, 'Event Calendar', '', '[kybcalendar cal_id=1]', 'Arial', 'Arial', 'Arial', 'Arial', 'Arial', 'Arial', 'Arial', 'Arial', 'Arial', 'Arial', '24px', '20px', '14px', '12px', '12px', '12px', '12px', '13px', '13px', '14px', '#ffffff', '#ffffff', '#ffffff', '#464646', '#3f3f3f', '#1e73be', '#ffffff', '#ffffff', '#ffffff', '#464646', 'bold', 'bold', 'bold', '', 'bold', 'bold', 'bold', '', '', '', '#0079c1', '#0079c1', '#f9a833', '#fcfcfc', '#ffffff', '', '#5c5c5c', '#0079c0', '#2ac545', '#dddddd', '', '', '', '', '', '', NULL, NULL, NULL, NULL, '', '', '', '#cbcbcb', '#cbcbcb', '', '#ffffff', '#ffffff', '#ffffff', '#dddddd', 0, 1, 1, 1, 0, -1, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `kyb_calendar_timezones`
--

CREATE TABLE IF NOT EXISTS `kyb_calendar_timezones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tz_id` varchar(200) NOT NULL,
  `tz_name` varchar(255) NOT NULL,
  `tz_offset` int(11) NOT NULL,
  `tz_status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Hold information for Time Zones' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kyb_calendar_timezones`
--

INSERT INTO `kyb_calendar_timezones` (`id`, `tz_id`, `tz_name`, `tz_offset`, `tz_status`) VALUES
(1, 'America/New_York', 'Eastern Standard Time', 5, 1),
(2, 'America/Chicago', 'Central Standard Time', 6, 1),
(3, 'America/Denver', 'Mountain Standard Time', 7, 1),
(4, 'America/Los_Angeles', 'Pacific Standard Time', 8, 1);
