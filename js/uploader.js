jQuery(document).ready(function() {
 
var original_send_to_editor = window.send_to_editor;
jQuery('#image_upload_button').click(function() {
 formfield = jQuery('#image_upload').attr('name');
 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
 window.send_to_editor = function(html) {
  imgurl = jQuery('img',html).attr('src');
  jQuery('#image_upload').val(imgurl);
  jQuery('#image_view').attr('src', imgurl);
  tb_remove();
  window.send_to_editor = window.restore_send_to_editor;
 }
 return false;
});

jQuery('#upload_image_button').click(function() {
 formfield = jQuery('#upload_image').attr('name');
 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
 window.send_to_editor = function(html) {
  imgurl = jQuery('img',html).attr('src');
  jQuery('#upload_image').val(imgurl);
  jQuery('#image_view').attr('src', imgurl);
  tb_remove();
  window.send_to_editor = window.restore_send_to_editor;
 }
 return false;
});

jQuery('#upload_video_button').click(function() {
 formfield = jQuery('#upload_video').attr('name');
 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
 window.send_to_editor = function(html) {
  imgurl = jQuery(html).attr('href'); 
  jQuery('#upload_video').val(imgurl);
  if (!!$('#upload_video_view'))
  {
	  var url = $("#datajax").val();
	  var obj = $("#upload_video");
	  previewVideo(obj, url, 'upload_video_view');
  }
  //jQuery('#image_view').attr('src', imgurl);
  tb_remove();
  window.send_to_editor = window.restore_send_to_editor;
 }
 return false;
});

jQuery('#upload_file_button').click(function() {
 formfield = jQuery('#upload_file').attr('name');
 tb_show('', 'media-upload.php?type=file&amp;TB_iframe=true');
 window.send_to_editor = function(html) {
  imgurl = jQuery(html).attr('href');   
  jQuery('#upload_file').val(imgurl);
  tb_remove();
  window.send_to_editor = window.restore_send_to_editor;
 }
 return false;
});


   
});

function upload_multi() {
	// Set all variables to be used in scope
	var frame,
      metaBox = $('#meta-box-id.postbox'), // Your meta box id here
      addImgLink = metaBox.find('.upload-custom-img'),
      delImgLink = metaBox.find( '.delete-custom-img'),
      imgContainer = metaBox.find( '.custom-img-container'),
      imgIdInput = metaBox.find( '.custom-img-id' );

	event.preventDefault();
	if ( frame ) {
      frame.open();
      return;
    }
    // Create a new media frame
    frame = wp.media({
      title: 'Select or Upload Media',
      button: {
        text: 'Use this media'
      },
      multiple: true  // Set to true to allow multiple files to be selected
    });

	// When an image is selected in the media frame...
    frame.on( 'select', function() {
		var selection = frame.state().get('selection');
		var imghtml = [];
		var imgCnt = $("#imagecnt").val();
		var urls = "";
		var attachment_urls = selection.map( function( attachment ) {
			attachment = attachment.toJSON();
			imghtml.push(attachment.url);
			var spanview = "<span id=\"image_" + imgCnt + "\" class=\"span3\" style='padding-bottom:10px;'><img src='" + attachment.url + "' style='width:100%;height:100px;'><div align='right'><span class='icon-remove f14 cursor icon-gray' onclick=\"imageDelete('/wp-content/plugins/kybdata/model/data_ajax.php?process=deleteimages&img=" + attachment.url + "','image_" + imgCnt + "', '" + attachment.url + "')\"></span><span class='icon-search f14 cursor icon-gray' onclick=\"jQuery.facebox('<img src=\\'" + attachment.url + "\\' >')\"></span></div></span>";
			$( "#sortable" ).append( spanview );
			urls = urls + attachment.url + ";";
			imgCnt++;
			return attachment.url;
		}).join();
		

	  $("#imagecnt").val(imgCnt);
	  var currentImg = $('#imageviews').val();

	  $('#imageviews').html(currentImg + urls);
	  
     /*
	  // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      // Send the attachment URL to our custom image input field.
      imgContainer.append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );

      // Send the attachment id to our hidden input
      imgIdInput.val( attachment.id );

      // Hide the add image link
      addImgLink.addClass( 'hidden' );

      // Unhide the remove image link
      delImgLink.removeClass( 'hidden' );*/
    });

	// DELETE IMAGE LINK
  /*delImgLink.on( 'click', function( event ){

    event.preventDefault();

    // Clear out the preview image
    imgContainer.html( '' );

    // Un-hide the add image link
    addImgLink.removeClass( 'hidden' );

    // Hide the delete image link
    delImgLink.addClass( 'hidden' );

    // Delete the image id from the hidden input
    imgIdInput.val( '' );

  });*/

	// Finally, open the modal on click
    frame.open();
	
});