if (ajaxurl == undefined) {
	var ajaxurl = "/wp-admin/admin-ajax.php";
}
var calendar = {
    model: {
        fieldItems: [],
        imgSrc: '/wp-content/plugins/kybcalendar/images/loading.gif'
    },
    view: { 
		addRecurDate: function () {
			input = $('<input/>');
			input.attr('type', 'text');
			input.attr('name', 'customdates[]');
			input.attr('class', 'span6 recurdate');
			input.attr('placeholder', 'click to enter date');

			icon = $('<span>');
			icon.attr('class', 'icon-trash icon_gray fs5 hover');
			icon.attr('onclick', 'calendar.view.deleteRecur(this)');			
			
			span = $('<div/>');
			span.attr('class', 'row-fluid');
			span.append(input);
			span.append(icon);
			$('#customrecur').append(span);
			$('.recurdate').datepicker({
				dateFormat : 'mm/dd/yy'
			});
			$('.override').removeAttr('disabled');
		},
		applyCal: function (currmonth, currview, url, calID) {
			chm = "0";
			prm = $('#month').val();
			yr = $('#year').val();
			currmo = parseInt(currmonth);
			prm = parseInt(prm);
			calendar.view.showProcessing('calendarset');
			params = {
				action: 'cal_action',
                process: 'changecal',
                prm: prm,
                y: yr,
				view: currview,
                calID: calID
            }
			$.post(ajaxurl, params, function(response) {
				$('#calendarset').html(response);
			});
		},
		calNav: function (url, currmonth, chm, curryear, currview, currday, calID, ontoday) {
			prm = parseInt(currmonth);
			yr = parseInt(curryear);
			currentTime = new Date()
			month = currentTime.getMonth() + 1;
			day = currentTime.getDate();
			year = currentTime.getFullYear();
       

			params = {};

          
          if (parseInt(currmonth) == 12 && chm == "1") {
               yr = parseInt(yr) + 1;
               month = 01;
               prm = '0';
          } else if (parseInt(currmonth) == 1 && chm == "-1") {
               yr = parseInt(yr) - 1;
               month = 13;
               prm = '13';
          } else if (parseInt(currmonth) > 12 && chm == "-1") {
               yr = parseInt(yr) - 1;
               month = 13;
               prm = '13';
          }
          
          
			if (currview == "Year" && ontoday) {
				currview = "Month";
			}
			params = {
				action: 'cal_action',
                process: 'changecal',
                prm: prm,
				chm: chm,
                y: yr,
				view: currview,
              cday: currday,
                calID: calID,
            }

			if (currview == "Week")	{
				var currweek = $('#currweek').val();
				if (chm == "0" && ontoday){
					params['wk'] = yr + "-" + month + "-" + day;
					params['nav'] = 'today';
				} else {
					params['wk'] = currweek;
				}
			}

			if (currview == "Day"){
             //alert(currday);
				cday = parseInt(currday);
             lastDayOfMonth = new Date(curryear, currmonth, 0);
             lastday = lastDayOfMonth.getDate();
				if (chm == "1") {
                 if (cday == lastday) {
                    cday = 1;
                    currmonth == 12 ? currmonth = 1 : currmonth++;
                    params.prm = currmonth;
                 } else {
					   cday = cday + 1;
                 }

					if (cday > 0){
						currday = cday;
						if (cday.length < 1){
							currday = "0" + cday;
						}	
					}
					params['dy'] = currday;
				}

				if (chm == "-1") {
					cday = cday - 1;
                 
					if (cday > 0){
						currday = cday;
						if (cday.length < 1){
							currday = "0" + cday;
						}
					} 
                    
                 
					
					params['dy'] = currday;
				}	
				
				if (chm == "0"){
					params['dy'] = day;
				}

             postday = parseInt(params['dy']);
              if (postday != lastday && chm != '-1' && chm != '0') {
                  
                  params.y = curryear;
                  params.prm = currmonth;
              } 
              //alert(postday);
              
         
          
			}
			calendar.view.showProcessing('calendarset');
			$.post(ajaxurl, params, function(response) {
				$('#calendarset').html(response);
			});
		},
		changeCalView: function (view,currmonth, url, currday, calID) {
			chm = "0";
			prm = $('#month').val();
			yr = $('#year').val();
			currmo = parseInt(currmonth);
			prm = parseInt(prm);
				
			params = {
				action: 'cal_action',
                process: 'changecal',
                prm: prm,
				chm: chm,
                y: yr,
				view: view,
                calID: calID
            }
			view =="Day" ? params['dy'] = currday : '';
			calendar.view.showProcessing('calendarset');
			$.post(ajaxurl, params, function(response) {
				$('#calendarset').html(response);
			});
		},
        changePeriodText: function (obj) {
            choice = obj.value;
            period = "";
            switch (choice) {
                case "BusDay":
                    period = "business days";
                    break;
                case "Day":
                    period = "days";
                    break;
                case "Week":
                    period = "weeks";
                    break;
                case "Month":
                    period = "months";
                    break;
                case "Year":
                    period = "years";
                    break;
            }
            $('#monthId').html(period);
			$('.override').removeAttr('disabled');
        },
        checkOK: function (var1) {
            if (confirm("Are you sure you want to delete this item?")) {
                collection = document.getElementsByName("form[modThis]");
                for (var x = 0; x < collection.length; x++) {
                    if (collection[x].checked) {
                        var1 += "&mod=" + collection[x].value;
                        window.location = var1
                    }
                }
            }
        },
        checkDelete: function (var1) {
            if (confirm("Are you sure you want to delete this item?")) {
                window.location = var1
            }
        },
		deleteRecur: function (obj) {
			divrow = $(obj).closest('div.row-fluid');
			divrow.remove();
			if ($('#customrecur').html() == "") {
				$('.override').attr('disabled', true);
			}
		},
        directCal: function (obj, url) {
            items = $(obj).val();
            calendar.model.fieldItems = items.split("-");
            var prm = fieldItems[0];
            url = url + "?prm=" + prm + "&chm=1";
            window.location = url;
        },
        gotoLink: function (var1, obj) {
            query = '';
            if (obj != undefined) {
               query = $(obj).val();
            }
            window.location = var1 + query;
        },
        loadkybCal: function (url, loadarea, elem, pluginurl) {
            processcallback = function (r, elem) {
                $(elem).html(r);
            }
            imgSrc = pluginurl + '/images/loading.gif';
            $(elem).html("<p align='center'>Loading please wait...<br/><img src='" + imgSrc + "' width='32' height='32'/></p>");
            calendar.controller.processCall(url, '', processcallback, elem);
			
        },
        updateCal: function (direction, chm) {
			prm = $('#monthsel').val();
			yr = $('#yearsel').val();
            if (direction != undefined) {
				newmonth = parseInt(prm) + parseInt(chm);
				if (newmonth > 12) {
					newmonth = 1;
					yr = parseInt(yr) + 1;
				} else if (newmonth <=0) {
					yr = parseInt(yr) -1;
					newmonth = 12;
				}
				$('#monthsel').val(newmonth);
				$('#yearsel').val(yr);				
				prm = newmonth;
			}
			chm == undefined ? chm = '' : '';
            
            calID = $('#calID').val();
            $('#calindexevents').html("Processing please wait...");
			
            params = {
				action: 'cal_action',
                process: 'viewcal',
                prm: prm,				
                y: yr,
                calID: calID
            }
			
			
			$.post(ajaxurl, params, function(response) {
				$('#calindexevents').html(response);
			});
        },
        showSearch: function (adminurl) {
            keywords = $('#post-search-input').val();
            datelookup = $('#datepicker').val();
            keywords == "Search by keyword" ? keywords = "" : '';
            datelookup == "Search by date" ? datelookup = "" : '';
            window.location = adminurl + "&process=viewday&filtertype=search&keywords=" + keywords + "&lookup=" + datelookup;
        },
        switchCalView: function (pluginurl, url, process, addparams) {				
            calendar.view.showProcessing('calendarset');
            params = calendar.controller.getFormFields(process, addparams);
        	$.post(ajaxurl, params, function(response) {
				$('#calendarset').html(response);
			});
        },
		showProcessing: function (elem) {		
            $('#' + elem).html("<p align='center'>Processing please wait...<br/><img src='" + calendar.model.imgSrc + "' width='32' height='32'/></p>");
		},
        toggletiny: function (obj) {
        cnt = $(obj).val();
       
  $(obj).children('option').each(function () {

 tab_body = "body" + $(this).val();
       
             $("#" + tab_body).hide();
});
 tab_body = "body" + cnt;
       
             $("#" + tab_body).show()
},
        toggletabs: function (tabnum, elem, cnt, id) {
            listparent = $(elem).parent().parent();
            listlen = $(listparent).children('li').length;
            $(listparent).children('li').each(function () {
                tab_body = id + cnt;
                if (cnt == tabnum) {
                    $("#" + tab_body).show();
                    $(this).addClass('active');
                } else {
                    $("#" + tab_body).hide();
                    $(this).removeClass('active');
                }
                cnt++;
            });
        }
    },
    controller: {
        formCheck: function () {
            alertMsg = "";
            $('form input, form select, form textarea').each(function () {
                if ($(this).attr('required') == 'required' && $(this).val() == '') {
                    id = $(this).attr('id');
                    if (id) {
                        alertMsg += " - " + id + "\n";
                    }
                }
            });
            if (alertMsg != "") {
                alert('Please complete the following fields:\n' + alertMsg);
                return false;
            }
            return true;
        },
        getFormFields: function (process, addparams) {
            data = {};
			process != "" ? data['process'] = process : '';

			$('form input, form select, form textarea').each(function () {
				inputName = $(this).attr('name');
				if (inputName && inputName.indexOf('form') > -1) {
					data[$(this).attr('id')] = $(this).val().replace("#", "");
				}
			});
			if (addparams) {
				addparams = addparams.split(",");
				for (i=0; i<addparams.length;i++){
					v = addparams[i].split("=>");
					data[v[0]] = v[1];
				}
			}
            
            return data;
        },
        processCall: function (url, params, callback, args) {
            $.ajax({
                url: url,
                data: params,
                type: 'POST',
                success: function (resp) {
                    e = eval(callback);
                    if (e) {
                        if (args) {
                            e(resp, args);
                        } else {
                            e(resp);
                        }
                    }
                },
                error: function (e) {
                    alert('Error: ' + e);
                }
            });
        }
    }
}


