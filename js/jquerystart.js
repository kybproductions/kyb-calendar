jQuery(document).ready(function() {
    jQuery('#datepicker').datepicker({
        dateFormat : 'mm/dd/yy'
    });
	jQuery('#datepicker1').datepicker({
        dateFormat : 'mm/dd/yy'
    });
	jQuery('#datepicker2').datepicker({
        dateFormat : 'mm/dd/yy'
    });
	jQuery('#datepicker3').datepicker({
        dateFormat : 'mm/dd/yy'
    });
	jQuery('#datepicker4').datepicker({
        dateFormat : 'mm/dd/yy'
    });
	jQuery('#datepicker5').datepicker({
        dateFormat : 'mm/dd/yy'
    });
});


jQuery(document).ready(function() {
	jQuery('.uploadbtn').click(function() {
		upload_multi(this, false);
		return false;
	});
});


function upload_multi(elem, multiple) {
	// Set all variables to be used in scope
	var frame,
      metaBox = $('#meta-box-id.postbox'), // Your meta box id here
      addImgLink = metaBox.find('.upload-custom-img'),
      delImgLink = metaBox.find( '.delete-custom-img'),
      imgContainer = metaBox.find( '.custom-img-container'),
      imgIdInput = metaBox.find( '.custom-img-id' );

	event.preventDefault();
	if ( frame ) {
      frame.open();
      return;
    }
    // Create a new media frame
    frame = wp.media({
      title: 'Select or Upload Media',
      button: {
        text: 'Use this media'
      },
      multiple: multiple  // Set to true to allow multiple files to be selected
    });

	// When an image is selected in the media frame...
    frame.on( 'select', function() {
		var selection = frame.state().get('selection');
		var img_input = $(elem).parent().find('input.uploadimg');
		var img_div = $(elem).parent().find('div.labelimage');
		var urls = "";
		
		var attachment_urls = selection.map( function( attachment ) {
				attachment = attachment.toJSON();
				if (multiple) {
					urls = urls + attachment.url + ";";
				} else {
					img_input.val(attachment.url);
					img_div.html('<img src="' + attachment.url + '" width="150"/>');
				}
				
				return attachment.url;
			}).join();
    });

	
    frame.open();
	
};
