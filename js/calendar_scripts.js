function NewWindow2(mypage, myname, w, h, scroll, pos) {
	var LeftPosition = "";
	var TopPosition = "";
	if (pos == "random") {
		LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
		TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
	}

	if (pos == "center") {
		LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
		TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
	} else if ((pos != "center" && pos != "random") || pos == null) {
		LeftPosition = 0;
		TopPosition = 20
	}
	var settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';

	window.open(mypage, myname, settings);
}

function changeCAL(obj, url) {
	items = obj.value;
	var fieldItems = new Array();
	fieldItems = items.split("-");
	var prm = fieldItems[0];
	url = url + "?prm=" + prm + "&chm=1";
	window.location = url;
}

function applyCal(currmonth, currview, url) {
	var chm = "0";
	var prm = document.getElementById('month').value;
	var yr = document.getElementById('year').value;
	currmo = parseInt(currmonth);
	prm = parseInt(prm);
	url = url + "?prm=" + prm + "&chm=" + chm + "&y=" + yr + "&view=" + currview;
	window.location = url;
}

function changeCalView(obj,currmonth, url, currday) {
	var chm = "0";
	var prm = document.getElementById('month').value;
	var yr = document.getElementById('year').value;
	var view = obj.value;
	currmo = parseInt(currmonth);
	prm = parseInt(prm);
	url = url + "?prm=" + prm + "&chm=" + chm + "&y=" + yr + "&view=" + view;
	if (view =="Day")
	{
		url = url + "&dy=" + currday;
	}
	window.location = url;
}

function calNav(url, currmonth, chm, curryear, currview) {
	prm = parseInt(currmonth);
	yr = parseInt(curryear);
	if (chm == "1" && currview == "Year")
	{
		yr = yr + 1;
	} else if (chm == "-1" && currview == "Year")
	{
		yr = yr - 1;
	}
	url = url + "?prm=" + prm + "&chm=" + chm + "&y=" + yr + "&view=" + currview;
	if (currview == "Week")
	{
		var currweek = document.getElementById('currweek').value;
		url = url + "&wk=" + currweek;
	}

	if (currview == "Day")
	{
		var currday = document.getElementById('currday').value;
		cday = parseInt(currday);
		if (chm == "1") {
			cday = cday + 1;
			if (cday > 0)
			{
				currday = cday;
				if (cday.length < 1)
				{
					currday = "0" + cday;
				}
				
			}
			url = url + "&dy=" + currday;
		}

		if (chm == "-1") {
			cday = cday - 1;
			if (cday > 0)
			{
				currday = cday;
				if (cday.length < 1)
				{
					currday = "0" + cday;
				}
				
			}
			url = url + "&dy=" + currday;
		}
		
	}
	window.location = url;
}