<?php  
/*
Plugin Name:       KYB Productions Calendar Manager
Plugin URI:        http://www.kybproductions.net/products/kyb-event-calendar-wp-plugin/
Description:	   KYB Calendar is a customized calendar management system.
Version:           1.3
Author:            KYB Productions, LLC
License:           GNU General Public License v2
License URI:       http://www.gnu.org/licenses/gpl-2.0.html
Domain Path:       /languages
Text Domain:       kybcalendar
Bitbucket Plugin URI: https://bitbucket.org/kybproductions/kyb-calendar
Bitbucket Branch:     master
Requires WP: 3.8
Requires PHP: 5.3.0

Copyright (c) 2015, KYB Productions, LLC.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/ 

if( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('log_errors',TRUE);
ini_set("error_log", WP_PLUGIN_DIR . "/kybcalendar/error_log.log");
ini_set('display_errors',FALSE);
if ( defined( 'KYBCAL_VERSION' ) ) {
	// don't allow two copies of KYB Calendar to run simultaneously
	if ( is_admin()) {
		$message = sprintf( __( 'Another copy of KYB Calendar is already activated (version %1$s in "%2$s")', 'rv' ), KYBCAL_VERSION, KYBCAL_FOLDER );
		die($message);
	}
	return;
}


//Define constants
$siteURL = site_url();
$pluginURL = "$siteURL/wp-content/plugins";
define ('KYBCAL_VERSION', '1.2');
define ('KYBCAL_FOLDER', dirname(plugin_basename(__FILE__)));
define ('KYBCAL_BASENAME', plugin_basename(__FILE__) );
!defined('WP_CONTENT_URL') ? define('WP_CONTENT_URL', site_url('wp-content', $scheme)) : '';
!defined('WP_CONTENT_DIR') ? define( 'WP_CONTENT_DIR', str_replace('\\', '/', ABSPATH) . 'wp-content' ) : '';
define ('KYBCAL_ABSPATH', WP_CONTENT_DIR . '/plugins/' . KYBCAL_FOLDER);
define ('KYBCAL_URLPATH', $pluginURL . "/" . KYBCAL_FOLDER);
define('KYBCAL_ADMINURL', admin_url() . "admin.php?page=kybcalendar-calendar");
define('KYBCAL_AJAXPATH', "/" . str_replace(ABSPATH, "", WP_PLUGIN_DIR) . "/" . KYBCAL_FOLDER);
define('KYBCAL_TABLE', $wpdb->prefix);
date_default_timezone_set('America/Chicago');

class Event_Calendar {

	static $_instance = null;
	
	static function instance() {
		if(!self::$_instance) {
			self::$_instance = new Event_Calendar();
		}
		
		return self::$_instance;
	}

	function __construct() {
		add_action('admin_menu', array($this,'tools_calendar_actions'));
		add_shortcode('kybcalendar', array($this,'eventlist_view'));
		add_shortcode('getfeatcal', array($this,'get_featured_calendar'));
		add_shortcode('getminical', array($this,'get_mini_calendar'));
		register_activation_hook(__FILE__,array($this,'calendar_install'));
		register_deactivation_hook(__FILE__, array($this,'calendar_uninstall'));
		if(strpos($_SERVER['QUERY_STRING'], KYBCAL_FOLDER) !== FALSE) {
			add_action('admin_init',array($this,'calendar_admin_head'));
			add_action('admin_init', array($this,'miceeditor_admin_init'));
			add_action('admin_head', array($this,'miceeditor_admin_head'));
			
		}
		
		/* Adds a box to the main column on the Post and Page edit screens */
		add_action( 'add_meta_boxes', array($this,'calendar_add_custom_box'));
		add_action( 'save_post', array($this, 'calendar_save_postdata'));
		add_action( 'widgets_init', array($this, 'load_kybcalendar_widget'));
		add_action( 'wp_ajax_cal_action', array($this, 'cal_action_callback'));
		add_action( 'wp_ajax_nopriv_cal_action', array($this, 'cal_action_callback'));
		
	}

	function cal_includes () {
		require_once(KYBCAL_ABSPATH . "/model/shared.php"); // shared processes
		require_once(KYBCAL_ABSPATH . "/model/smarty.php"); // smarty engine
	}
	function tools_calendar_actions() {
		$userLevel = 1;
		$calman = __("Calendar Mgmt.", "kybcalendar_plugin");
		$addcal = __("Add Event", "index");
		$calsettings = __("Calendar Themes", "index");
		add_menu_page("Calendar &rsaquo; $calman", "Calendar Mgmt.", $userLevel,"kybcalendar-calendar", array($this,"calendar_controller"), 'dashicons-calendar-alt');
		add_submenu_page(__FILE__, "Calendar Themes &rsaquo; $calsettings", $calsettings, $userLevel, "kybcalendar-calendar-themes", array($this,"calendar_controller"));
		add_submenu_page(__FILE__, "Add Event &rsaquo; $addcal", $addcal, $userLevel, "kybcalendar-calendar-add", array($this,"calendar_controller"));
	}
	

	function cal_action_callback() {		
		$this->cal_includes();
		$action = $this->checkRequest('process');
		strpos($_SERVER['QUERY_STRING'], 'kybcalendar-calendar-add') !== FALSE ? $action = "addnew" : '';	
		strpos($_SERVER['QUERY_STRING'], 'kybcalendar-calendar-themes') !== FALSE ? $action = "settings" : '';	
		include_once("controller/controller.php");  	
		$controller = new CalendarController();  
		$controller->calajax($action); 
		wp_die(); // this is required to terminate immediately and return a proper response
	}
	
	static function calendar_install() {
		global $wpdb;
		global $table_prefix;
		global $current_user;
		$dbtables = KYBCAL_ABSPATH . "/admin/sql/calendar_db_tables.sql";
		$backupfile = KYBCAL_ABSPATH . "/admin/sql/calendar_db_backup.sql";
		if (is_file($backupfile)) {
			$dbtables = $backupfile;
		} 
		$file_content = file($dbtables);
		$query = "";
		$tables = array("`kyb_calendar`","`kyb_calendar_themes`", "`kyb_calendar_timezones`", "`kybcalendar_config`");
		foreach($file_content as $sql_line){
			if(trim($sql_line) != "" && strpos($sql_line, "--") === false){
				$query .= $sql_line;
				//Need to add in to make sure the table name that this is renaming to does not exists so will not overwrite.
				if (substr(rtrim($query), -1) == ';'){
					if (is_file($backupfile)) {
						//Load last back up of database tables
						//Check to see if table has current table prefix
						$table = trim($table_prefix . str_replace("`","",$tables[$key]));
						if (strpos($query, $table) !== false) {
							$wpdb->query($query);
						} else {
							$query = str_replace($tables[$key],"`" . trim($table_prefix . str_replace("`","",$tables[$key])) . "`", $query);
							$wpdb->query($query);
						}
					} else {
						foreach ($tables as $key => $value) {
							if (strpos($query, $tables[$key]) !== false) {
								$table = trim($table_prefix . str_replace("`","",$tables[$key]));
								//if (!$this->table_exist($table)) {
									//ALTER TABLE  `wp_harmony_forms` ADD  `form_payment_policy` TEXT NULL AFTER  `form_list_intro` ;
									//This still would cause errors on insert if table exists so will need to include a deactivation
									$query = str_replace($tables[$key],"`" . trim($table_prefix . str_replace("`","",$tables[$key])) . "`", $query);
									$wpdb->query($query);
								//}
							}
						}
					}
					$query = "";
				}
			}
		}

		//Add Event Calendar Category
		$wp_cat = array('cat_name' => 'Event Calendar', 'category_nicename' => 'event-calendar', 'category_parent' => 0);
		$wp_cat_id = wp_insert_category($wp_cat);
		$wpdb->query($wpdb->prepare("Update " . $table_prefix . "kyb_calendar_themes set cal_wp_categoryID = %d where cal_id = %d", $wp_cat_id, 1));
	}

	private function calendar_uninstall() {
		global $wpdb;
		global $table_prefix;
		//Back up any current form data for later reinstall if needed
		$tables = array("`kyb_calendar`","`kyb_calendar_themes`", "`kyb_calendar_timezones`", "`kybcalendar_config`");
		$this->backup_tables($tables);
		//Drop form tables
		foreach ($tables as $key => $value) {
			$table = trim($table_prefix . str_replace("`","",$tables[$key]));
			$chkTbl = $wpdb->get_results("SHOW TABLES LIKE '$table'");
			if (count($chkTbl)) {
				$sql = "DROP TABLE $table";
				$wpdb->query($sql);
			}
		}
	}

	function calendar_controller() {
		$this->cal_includes();
		$action = $this->checkRequest('process');
		strpos($_SERVER['QUERY_STRING'], 'kybcalendar-calendar-add') !== FALSE ? $action = "addnew" : '';	
		strpos($_SERVER['QUERY_STRING'], 'kybcalendar-calendar-themes') !== FALSE ? $action = "settings" : '';	
		include_once("controller/controller.php");  	
		$controller = new CalendarController();  
		$controller->invoke($action); 
	}

	function get_featured_calendar() {
		$this->cal_includes();
		include_once("controller/controller.php");  	
		$controller = new CalendarController();
		$content = $controller->viewFeaturedCal();
		return $content;
	}

	function get_mini_calendar() {
		$this->cal_includes();
		include_once("controller/controller.php");  	
		$controller = new CalendarController();
		$content = $controller->viewSideCal();
		return $content;
	}

	function load_kybcalendar_widget() {
		require_once(KYBCAL_ABSPATH . "/model/widget.php"); // shared class
		register_widget( 'kybcalendar_widget' );
	}

	function calendar_widget($args, $instance) {
		$title = apply_filters( 'widget_title', $instance['title'] );

	}

	function checkRequest($var) {
		$value = "";
		 if (isset($_POST[$var]) || isset($_GET[$var])) {
			$_POST[$var] != "" ? $value = $this->cleanString($_POST[$var]) : $value = $this->cleanString($_GET[$var]);
		  }
		  $value = $this->cleanString($value);
		  return $value;		 
	}

	function cleanString($string) {
		$cleaned = mysql_real_escape_string(stripslashes(strip_tags($string)));
		return $cleaned;
	}

	function calendar_admin_head()	{
		//Use correct jquery
		wp_deregister_script( 'jquery' );
		wp_enqueue_script( 'forms-jquery', KYBCAL_URLPATH . '/js/jquery-1.11.0.min.js', array(), '1.11.0', false );
		wp_enqueue_script('forms-jquerystart-migrate', KYBCAL_URLPATH . '/js/jquery-migrate-1.2.1.min.js');

		//Add KYB Calendar Styles
		wp_enqueue_style('cal-admin-stylesheet', KYBCAL_URLPATH . '/css/admin_stylesheet.css');
		wp_enqueue_style('calendar-styles-css', KYBCAL_URLPATH . '/css/calendar_styles.css');

		//Add Date Picker Styles and Scripts
		wp_enqueue_style('kyb-calendar_ui', KYBCAL_URLPATH . '/css/calendar_ui.css');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script('jquerystart-js', KYBCAL_URLPATH . '/js/jquerystart.js', array('jquery'));
		

		//Add KYB Calendar Scripts
		wp_enqueue_script('calendar-js-js', KYBCAL_URLPATH . '/js/calendar_js.js');
		
				
		//Color Pickers
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'colorpicker-handle', KYBCAL_URLPATH . '/js/colorpicker.js', array( 'wp-color-picker' ), false, true );
	
	}

	function miceeditor_admin_init() {
		wp_enqueue_script('word-count');
		wp_enqueue_script('post');
		wp_enqueue_script('editor');
		wp_enqueue_script('media-upload');
		wp_register_script('calmanager-uploader', KYBCAL_URLPATH . '/js/uploader.js', array('jquery','media-upload','thickbox'));
		wp_enqueue_script('thickbox');
		wp_enqueue_script('uploader');
		wp_enqueue_style('thickbox');
		wp_enqueue_media();
	}

	function miceeditor_admin_head() {
		wp_tiny_mce();
	}

	function eventlist_view($atts) {
		extract( shortcode_atts( array(
				'cal_id' => '0',
				'process' => '',
				'eventid' => '',
				'echo' => '0',
			), $atts ) );
		require_once(KYBCAL_ABSPATH . "/model/shared.php"); // shared class
		require_once(KYBCAL_ABSPATH . "/model/smarty.php"); // smarty engine
		include_once(KYBCAL_ABSPATH . "/controller/controller.php"); 
		$controller = new CalendarController(); 
		$process == "" && $this->checkRequest('process') != "" ? $process = $this->checkRequest('process') : '';
		if (isset($_POST['process']) && $_POST['process'] == "formsubmit") {
			$process = $_POST['process'];
		} else if ($process != "") {
			$process = $process;
		} else {
			$process = "eventlist";
		}
		$pluginURL = KYBCAL_URLPATH;
		echo "<style>";
		echo "@font-face {font-family: 'kybformfont';	src:url('$pluginURL/css/fonts/kybfonts.eot?-9rerhn');src:url('$pluginURL/css/fonts/kybfonts.eot?#iefix-9rerhn') format('embedded-opentype'),		url('$pluginURL/css/fonts/kybfonts.woff?-9rerhn') format('woff'),url('$pluginURL/css/fonts/kybfonts.ttf?-9rerhn') format('truetype'),url('$pluginURL/css/fonts/kybfonts.svg?-9rerhn#icomoon') format('svg');font-weight: normal;font-style: normal;}";
		echo "</style>";
		wp_register_style('kybcalendar-styles-css', KYBCAL_URLPATH . '/css/calendar_styles.css', array(), '1.2', 'screen');
		wp_enqueue_style('kybcalendar-fonts-css', KYBCAL_URLPATH . '/css/kybfonts.css');
		wp_enqueue_style('kybcalendar-styles-css', KYBCAL_URLPATH . '/css/calendar_styles.css');
		wp_enqueue_script('kybcalendar-js', KYBCAL_URLPATH . '/js/calendar_js.js');
		
		
		$content = $controller->viewEvents($process, $cal_id, $eventid);
		return $content;		
	}

	

	private function backup_tables($tables) {
		global $table_prefix;
		global $wpdb;
		$tables = is_array($tables) ? $tables : explode(',',$tables);
		$return = "";
		$valtypes = array();
		$fieldvals = "";
		$backupfile = KYBCAL_ABSPATH . "/admin/sql/calendar_db_backup.sql";
		
		foreach($tables as $table)
		{
			$table = trim($table_prefix . str_replace("`","",$table));
			$chkTbl = $wpdb->get_results("SHOW TABLES LIKE '$table'");
			if (count($chkTbl)) {
				$result = $wpdb->get_results('SELECT * FROM '.$table, ARRAY_A);
				$createtbl = $wpdb->get_row('SHOW CREATE TABLE '.$table);
				$return.= "\n\n".$createtbl->Create_Table.";\n\n";
				if (count($result)) {
					$return.= 'INSERT INTO '.$table.' (';
					$tblfields = $wpdb->get_results('SHOW COLUMNS FROM '.$table);
					foreach ( $wpdb->get_col( "DESC " . $table, 0 ) as $column_name ) {
						$return.= "`$column_name`, ";
						$colType = $wpdb->get_var($wpdb->prepare("SELECT COLUMN_TYPE FROM information_schema.COLUMNS WHERE TABLE_NAME = %s AND COLUMN_NAME = %s", $table, $column_name));
						$start = '\(';
						$end  = '\)';
						$colType = preg_replace('#('.$start.')(.*)('.$end.')#si', '', $colType);
						$valtypes[$column_name] = $colType;
					}
					$return = substr($return, 0, strlen($return) -2); 
					$return .= ") VALUES ";
					foreach ($result as $key => $row) {
						$j = 0;
						$return.= "(";
						foreach ($row as $k => $v) {
							$fielditem = addslashes($v);
							$fielditem = str_replace("\n","\\n",$fielditem);
							switch ($valtype[$k]) {
								case "tinyint":
									$return.= $fielditem;
								break;
								case "int":
									$return.= $fielditem;
								break;
								case "float":
									$return.= $fielditem;
								break;
								case "real":
									$return.= $fielditem;
								break;
								default:
									if (isset($fielditem)) { $return.= '"'.$fielditem.'"' ; } else { $return.= '""'; }
								break;
							}
							
							if ($j<(count($row)-1)) { $return.= ','; }
							$j++;
						}
						$return.= "),\n";
					}
					$return = substr($return, 0, strlen($return) -4); 
					$return.= ");\n";
					$valtypes = array();
				}
				$return.="\n\n\n";
			}
			
		}
		
		if ($return != "") {
			//save file
			$handle = fopen($backupfile,'w+');
			fwrite($handle,$return);
			fclose($handle);
		}
		
	}

	
	function calendar_add_custom_box() {
		$screens = array( 'post', 'page' );
		foreach ($screens as $screen) {
			add_meta_box(
				'kybcalendar_sectionid',
				__( 'Event Calendar',
				'kybcalendar_textdomain' ),
				array($this,'cal_inner_custom_box'),
				$screen,
				'normal',
				'high'
			);
		}
	}

	function cal_inner_custom_box() {
		global $wpdb;
		global $post;
		// Use nonce for verification
		wp_nonce_field( plugin_basename( __FILE__ ), 'kybcalendar_noncename' );
		wp_enqueue_style('calendar-styles-css', KYBCAL_URLPATH . '/css/calendar_styles.css');
		//Add Date Picker Styles and Scripts
		wp_enqueue_style('jquery-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script('jquerystart-js', KYBCAL_URLPATH . '/js/jquerystart.js');
		

		//Add KYB Calendar Scripts
		wp_enqueue_script('calendar-js-js', KYBCAL_URLPATH . '/js/calendar_js.js');
				
		require_once(KYBCAL_ABSPATH . "/model/shared.php"); // shared class
		require_once(KYBCAL_ABSPATH . "/model/smarty.php"); // smarty engine
		include_once(KYBCAL_ABSPATH . "/controller/controller.php"); 
		include_once(KYBCAL_ABSPATH . "/model/model.php"); 
		$cls = new calendarclass;
		$calID = $wpdb->get_var($wpdb->prepare("Select id from kyb_calendar where wp_postID = %d", $post->ID));
		$content = $cls->calForm($calID, '', 'post');
		is_array($content) ? $html = $content['HTML'] : $html = '';
		$template =  KYBCAL_ABSPATH . "/view/html/$html";
		$output = $cls->showPage ($template, $content, true);
		echo $output;
	}

	function calendar_save_postdata($post_id) {
		if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;
		 
		if (!wp_verify_nonce( $_POST['kybcalendar_noncename'], plugin_basename ( __FILE__ ) ) )
			return;
		 
		if ( 'page' == $_POST['post_type'] ){
			if (!current_user_can( 'edit_page', $post_id ) )
				return;
		} else {
			if (!current_user_can( 'edit_post', $post_id ) )
				return;
		}

		//Do save functions here
		if (isset($_POST['calID'])) {
			//Do calendar process to update or add event to calendar
			require_once(KYBCAL_ABSPATH . "/model/shared.php"); // shared class
			require_once(KYBCAL_ABSPATH . "/model/smarty.php"); // smarty engine
			include_once(KYBCAL_ABSPATH . "/controller/controller.php"); 
			include_once(KYBCAL_ABSPATH . "/model/model.php"); 
			$cls = new calendarclass;
			$cls->checkRequest('calID') != "" ? $calID = $cls->checkRequest('calID') : $calID = 0;
			$calID == 0 ? $process = 'add' : $process = 'update';
			$cls->update_calendar_form($process, $calID, $post_id);			
		}		
	}
}
// create shared instance
if ( is_admin() )
	Event_Calendar::instance();
else
    add_action( 'plugins_loaded', array( 'Event_Calendar', 'instance' ) );




?>